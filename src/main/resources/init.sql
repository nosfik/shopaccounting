CREATE DATABASE shopdb CHARACTER SET utf8 COLLATE utf8_general_ci;


/* CHANGING PRODUCT RELATION */
update product as p set supplier_id = (select supplier from supplier_products as sp where p.id = sp.products)
drop supplier_products;

/* ADDING ADMIN USER */
ALTER TABLE balance MODIFY COLUMN balance decimal(19,3)
INSERT INTO `users` (username,password,enabled) VALUES ('tru','203708', true);
INSERT INTO authority (username, authority) VALUES ('tru', 'ROLE_ADMIN');


update product set supplier_id = 34, is_deleted = 1 where id IN (446, 447, 448, 449, 450);
update product set supplier_id = 13, is_deleted = 1 where id IN (653, 654);
update product set supplier_id = 73, is_deleted = 1 where id = 609;
update product set supplier_id = 73, is_deleted = 1 where id = 1173;


select * from invoice_product where product IN (select id from product where supplier_id = 0)


/*DONE*/





select distinct invoice0_.id as id1_2_0_, invoicepro2_.id as id1_3_1_, supplier3_.id as id1_10_2_, products4_.id as id1_5_3_, productcat5_.id as id1_6_4_, invoice0_.created_date as created_2_2_0_, invoice0_.credit as credit3_2_0_, invoice0_.date as date4_2_0_, invoice0_.expenses as expenses5_2_0_, invoice0_.supplier as supplier7_2_0_, invoice0_.updated_date as updated_6_2_0_, invoicepro2_.margin_price as margin_p2_3_1_, invoicepro2_.name as name3_3_1_, invoicepro2_.price as price4_3_1_, invoicepro2_.product as product6_3_1_, invoicepro2_.quantity as quantity5_3_1_, products1_.invoice as invoice1_2_0__, products1_.products as products2_4_0__, supplier3_.name as name2_10_2_, supplier3_.product_category as product_3_10_2_, products4_.is_deleted as is_delet2_5_3_, products4_.margin_price as margin_p3_5_3_, products4_.name as name4_5_3_, products4_.price as price5_5_3_, products4_.supplier_id as supplier6_5_3_, products4_.supplier_id as supplier6_10_1__, products4_.id as id1_5_1__, productcat5_.name as name2_6_4_ 
from invoice invoice0_ 
inner join invoice_products products1_ on invoice0_.id=products1_.invoice 
inner join invoice_product invoicepro2_ on products1_.products=invoicepro2_.id 
inner join supplier supplier3_ on invoice0_.supplier=supplier3_.id 
left outer join product products4_ on supplier3_.id=products4_.supplier_id and ( products4_.is_deleted <> 1) 
left outer join product_category productcat5_ on supplier3_.product_category=productcat5_.id 

where invoice0_.date=?



/* -----------------------------------------------------------------------------------------------------------------------------*/
ALTER TABLE product MODIFY COLUMN price decimal(10,3)
ALTER TABLE invoice_product MODIFY COLUMN price decimal(10,3)
