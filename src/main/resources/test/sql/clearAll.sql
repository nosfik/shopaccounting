TRUNCATE TABLE invoice_products;
TRUNCATE TABLE invoice_product;
TRUNCATE TABLE invoice;
TRUNCATE TABLE revenue_expenses;
TRUNCATE TABLE balance;
TRUNCATE TABLE product;
TRUNCATE TABLE supplier;
TRUNCATE TABLE product_category;
