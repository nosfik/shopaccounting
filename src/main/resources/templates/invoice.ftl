<script id="productExciseTypeAndGroup" type="text/template">

<h2>Додані нові акцизні продукти</h2><br/>
<h3>Заповніть додаткові поля</h3><br/></br>

<table>
<tr>
	<th>Продукт</th>
	<th>Група</th>
	<th>Тип</th>
	<th>Код</th>
	<th>Об'єм</th>
</tr>


<% _.each(products, function(product) { %>  

<tr class="excise-product-item">
<input type="hidden" name="product_id" value="<%=product.id%>"/>
	<td><span><%-product.name%></span></td>
	<td>
		<select class='excise-group' name="excise_group">
		<option></option>
		<% _.each(groups, function(group) { %>
			<option value="<%=group.id%>"><%=group.name%></option>
		<% }); %>
		</select>

	</td>

	<td>
		<select class='excise-type' name="excise_type">
		<option></option>
		<% _.each(types, function(type) { %>
			<option value="<%=type.id%>"><%=type.name%></option>
		<% }); %>
		</select>

	</td>

	<td>
		<input type="text" class="excise-text excise-code" name="code" value=""/>
	</td>

	<td>
		<input type="text" class="excise-text excise-volume" name="volume" value=""/>
	</td>


</tr>

<% }); %>




</table>

</script>


<script id="cigaretteParsedList" type="text/template">
<form>

  <table>
    <tr>
      <td style="padding:10px"><div style="width:15px;height:15px;background-color:#71F163;float:left"></div>&nbsp;- Товар якого немає в базі</td>
	<td style="padding:10px"><div style="width:15px;height:15px;background-color:#FFD73E;float:left"></div>&nbsp;- Ціна на товар була змінена</td>
	<td style="padding:10px"><div style="width:15px;height:15px;background-color:red;float:left"></div>&nbsp;- Сигарети по різній ціні</td>

    
    </tr>
  
  </table>

<table id="cigarette-parsed-table">
<tr>
<th class="cigarette-item-header">№</th>
<th class="cigarette-item-header">Марка</th>
<th class="cigarette-item-header">Штрихкод</th>

<th class="cigarette-item-header cigarette-def-price<%= excise ?  ' hidden' : '' %>">Цiна</th>
<th class="cigarette-item-header cigarette-exc-price<%= excise ?  '' : ' hidden' %>">Цiна з<br/>акцизом</th>


<th class="cigarette-item-header">Нова ціна</th>
<th class="cigarette-item-header">Реком. ціна</th>
<th class="cigarette-item-header">Ціна<br/>продажу</th>
<th class="cigarette-item-header">Нова ціна<br/>продажу</th>
<th class="cigarette-item-header">Кількість</th>
<th class="cigarette-item-header">Код в к/а</th>
<th class="cigarette-item-header">Видалити</th>
</tr>

<% _.each(cig, function(p, t) { %>  
<tr>
<input type="hidden" value="<%=p.id%>" name="product_id">
<input type="hidden" value="<%=p.name%>" name="product_name">
<input type="hidden" value="<%=p.barcode%>" name="product_barcode">

<td class="cigarette-item"><%=(t+1)%></td>
<td class="cigarette-item cigarette-item-name"><%-p.name%></td>
<td class="cigarette-item barcode"><%-p.barcode%></td>



<td class="cigarette-item old-excise-price <%= excise ?  '' : ' hidden' %>"><%-p.oldExcisePrice%></td>

<td class="cigarette-item old-price <%= excise ?  ' hidden' : '' %>"><%-p.oldPrice%></td>


<td class="cigarette-item old-recomm"><input type="text" name="price" value="<%-p.price%>"/></td>
<td class="cigarette-item new-recomm"><%-p.recommendedPrice%></td>
<td class="cigarette-item old-margin-price"><%-p.oldMarginPrice%></td>
<td class="cigarette-item"><input type="text" name="marginPrice" value="<%-p.marginPrice%>"/></td>
<td class="cigarette-item"><input type="text" name="quantity" value="<%-p.quantity%>"/></td>
<td class="cigarette-item code"><input type="text" name="code" data-val="<%-p.code%>" value="<%-p.code%>"/></td>
<td><a href="#" onclick="$(this).closest('tr').remove()"><img width="20px" src="/img/error.png"></a></td>
</tr>
<% }); %>

</table>
</form>

</script>


<script id="invoiceRow" type="text/template">
	<tr>	

<% if (typeof(showDate) !== "undefined" && showDate === true) { %>
			<td class="number-column" style="width:12%"><%=date%></td>
<% } else { %>
			<td class="number-column"><%=number%></td>
<% } %>
			<td class="supplier-column supplier-table-name"><%-supplier.name%></td>
			<td class="margin-column"><%=averageMargin%></td>
			<td class="total-column"><%=Shop.util.getPriceView(total)%></td>
			<td class="income-column"><%=Shop.util.getPriceView(totalWithMargin)%></td>
			<td class="expenses-column"><%=expenses%></td>


	</tr>
</script>

<script id="revenueExpensesRow" type="text/template">
<tr>
			<td class="revenue-number"><%=number%></td>
			<td class="revenue-type"><%=type_name%></td>
			<td class="revenue-comment"><%-comment%></td>
			<td class="revenue-revenue"><%=Shop.util.getPriceView(revenue)%></td>
			<td class="revenue-expenses"><%=Shop.util.getPriceView(expenses)%></td>
</tr>
</script>

<script id="invoice-full" type="text/template">
<td colspan='6' class="no-padding">

<table>
<tr><td>

<table>
<tr>
<th>Товар</th>
<th class="product-def-price<%= excise ?  ' hidden' : '' %>">Цiна</th>
<th class="product-exc-price<%= excise ?  '' : ' hidden' %>">Цiна з акцизом</th>
<th>Нацiнка</th>
<th>Цiна продажу</th>
<th>Кiлькiсть</th>
<th>Сума накладної</th>
<th>Приход</th>

</tr>
<% _.each(products, function(p) { %>  
  <tr data-id="<%=p.productId%>">


<% if (typeof(p.code) !== "undefined" && p.code) { %>
		<td class="product-name"><%-p.name%><b> [<%=p.code%>]</b></td>
	<% } else {%>
		<td class="product-name"><%-p.name%></td>
	<% } %>


<td class="product-price product-def-price <%= excise ?  ' hidden' : '' %>"><%=Shop.util.getPriceView(p.price)%></td>
<td class="product-excprice product-exc-price<%= excise ?  '' : ' hidden' %>"><%=Shop.util.getPriceView(p.excisePrice)%></td>
<td class="product-margin"><%=p.margin%></td>
<td class="product-maring-price"><%=Shop.util.getPriceView(p.marginPrice)%></td>
<td class="product-quantity"><%=p.quantity%></td>
<td class="product-total"><%=Shop.util.getPriceView(p.total)%></td>
<td class="product-total-with-margin"><%=Shop.util.getPriceView(p.totalWithMargin)%></td></tr>
<% }); %>
</table>

</td></tr>

<% if(Shop.invoice.currentView !== "invoiceBlocked") { %>
<tr><td class="control-invoice">

<button type="button" id="edit-invoice">Редагувати</button>
<button type="button" id="delete-invoice">Видалити</button>
<% if (typeof(credit) !== "undefined" && credit === true ) { %>
<button type="button" id="fix-credit">Погасити борг</button>
<% } %>

</td></tr>
<% } %>
</table>

</td>

</script>

<script id="invoice-dialog-product" type="text/template">
<tr class='invoice-product-item'>


	
<input type="hidden" name="id" value="<%=id%>"/>
<input type="hidden" name="product_id" value="<%=productId%>"/>
<% if (typeof(code) !== "undefined") { %>
	<input type="hidden" name="code" value="<%=code%>"/>
<% } %>
<% if (typeof(barcode) !== "undefined") { %>
	<input type="hidden" name="barcode" value="<%=barcode%>"/>
<% } %>



<% if (typeof(uuid) !== "undefined") { %>
	
	<input type="hidden" name="uuid" value="<%=uuid%>"/>

<% } %>




<% if (typeof(newProduct) !== "undefined") { %>
	<td class="product-name"><a href="#" onclick="$(this).closest('tr').remove()"><img width="20px" src="/img/error.png"></a><input style="width:150px" type="text" name="product_name" value="<%-name%>"/></td>
<% } else {%>
	<input type="hidden" name="product_name" value="<%-name%>"/>

	<% if (typeof(code) !== "undefined") { %>
		<td class="product-name"><%-name%><b> [<%=code%>]</b></td>
	<% } else {%>
		<td class="product-name"><%-name%></td>
	<% } %>
	
<% } %>


<td class="product-price product-def-price <%= excise ?  'hidden' : '' %>"><input type="text" class="changeble" name="price" value="<%=Shop.util.getPriceView(price)%>" autocomplete="off" /></td>
<td class="product-excise-price product-exc-price <%= excise ?  '' : 'hidden' %>"><input type="text" class="changeble" name="excisePrice" value="<%=Shop.util.getPriceView(excisePrice)%>" autocomplete="off" /></td>
<td class="product-margin margin"><input type="text" class="changeble" name="margin" value="<%=margin%>" autocomplete="off"/></td>
<td class="product-margin-price"><input type="text" class="changeble" name="margin_price" value="<%=Shop.util.getPriceView(marginPrice)%>" autocomplete="off" /></td>

<td class="product-quantity"><input type="text" class="deletable" name="quantity" autocomplete="off"  value="<%=quantity%>"/></td>
</tr>
</script>

<script id="revenue-dialog" type="text/template">
<form id="revenue-dialog-form">

	<table>

  <tr>
<td>
<label for="revenue_type"/>Тип</label></td><td>
<select id="revenue_type" name="type" class="field">
<option value="0">Виберiть тип</option>
<% _.each(r_types, function(value, t) {  %>  
	<% if (typeof(type) !== "undefined" && type == value.id) { %>
		<option value="<%=value.id%>" selected><%=value.name%></option>
	<% } else {%>
		<option value="<%=value.id%>"><%=value.name%></option>
	<% } %>
<% }) %>

</select>
</td>
</tr>

<tr>

<td><label for="revenue-revenue">Приход</label></td><td> <input type="text" class="field" autocomplete="off" id="revenue-revenue" name="revenue" value="<%=Shop.util.getPriceView(revenue)%>"/> </td>

</tr><tr>
<td><label for="revenue-expenses"> Витрати </label></td><td><input autocomplete="off" class="field" type="text" id="revenue-expenses" name="expenses" value="<%=Shop.util.getPriceView(expenses)%>"/> </td>
</tr>
<tr>
	
<td >
<label for="revenue-comment">Коментар</label></td><td> <textarea id="revenue-comment" class="field" name="comment" value="<%-comment%>"><%=comment%></textarea>
</td>

</tr>
</table>

<% if(Shop.invoice.currentView !== "invoiceBlocked") { %>

<button type="button" id="save-revenue">Зберегти</button>

<% if (typeof(type) !== "undefined") { %>
	<button type="button" id="delete-revenue">Видалити</button>
<% } %>


<% } %>


</form>

</script>

<script id="search-dialog" type="text/template">

<form id="dialog-search-form">
<table>
    	<tr>
    			<td><label for="select-category">Категорiя</label></td>
    			<td> 
    				<select name="category" id='select-category'>
						<option class='default-value' value="-1">Виберіть категорію</option>
    					<option class='default-value' value="0">Всi</option>
							<% _.each(categories, function(value, t) {  %>  
								<% if (typeof(selected_category) !== "undefined" && selected_category == value.id) { %>
									<option value="<%=value.id%>" selected><%=value.name%></option>
								<% } else {%>
									<option value="<%=value.id%>"><%=value.name%></option>
								<% } %>
							<% }) %>
    				</select>
    			</td>
    	</tr>
    	
    	<tr>
    			<td style="vertical-align:top"><label for="select-supplier">Постачальник</label></td>
    			<td> 
    				<select name="supplier" id='select-supplier'>
    					<option class='default-value' value="0"></option>
						<% _.each(suppliers, function(value, t) {  %>  
								<% if (typeof(selected_supplier) !== "undefined" && selected_supplier == value.id) { %>
									<option value="<%=value.id%>" selected><%=value.name%></option>
								<% } else {%>
									<option value="<%=value.id%>"><%=value.name%></option>
								<% } %>
							<% }) %>
    				</select>
     			</td>
    	</tr>
    	<tr>
    			<td><label  for="select-product">Продукт</label></td>
    			<td> 
					<select name="product" id='select-product'>
    					<option class='default-value' value="0" selected></option>

							<% if (typeof(supplier) !== "undefined") { %>
								<% _.each(supplier.products, function(value, t) {  %>  
									<% if (typeof(selected_product) !== "undefined" && selected_product == value.id) { %>
										<option value="<%=value.id%>" selected><%=value.name%></option>
									<% } else {%>
										<option value="<%=value.id%>"><%=value.name%></option>
									<% } %>
								<% }) %>

							<% } %>
    				</select>
    			</td>
    	</tr>

		<tr>
			<td><label  for="expenses">Кількість</label></td>
			<td> 
					<select name="limit" id='select-supplier'>
						<option selected value="5">5</option>
						<option value="10">10</option>
						<option value="25">25</option>
						<option value="50">50</option>
					</select>
			</td>

		</tr>
</table>

</form>

<button type="button" id="search-inv-button" class="uibutton" disabled>Пошук</button>





</script>
<script id="invoice-dialog" type="text/template">
<form id="add-invoice-form" enctype="multipart/form-data">
    <fieldset>
<table style="width:100%"><tr>


<td>

 <table>
    	<tr>
    			<td><label for="name">Категорiя</label></td>
    			<td> 
    				<select name="category" id='select-category'>
						<option class='default-value' value="-1">Виберіть категорію</option>
    					<option class='default-value' value="0">Всi</option>
    				</select>
    			</td>
    	</tr>
    	
    	<tr>
    			<td style="vertical-align:top"><label for="name">Постачальник</label></td>
    			<td> 
    				<select name="supplier" id='select-supplier'>
    					<option class='default-value' value="0"></option>
    				</select>
					<div id="invoice-file" style="margin-top:15px;display:none">
						<span style="display:block;font-weight: 700;">Виберіть файл з інвойсом</span>
						<input type="file" name="file" id="invoice-document" accept="application/pdf"/>
					</div>
   					
     			</td>
    	</tr>
    	<tr>
    			<td><label  for="expenses">Повернення</label></td>
    			<td> 
					<input type="text" autocomplete="off" class="model-part" id="expenses" name="expenses" value="<%=expenses%>"/>
    			</td>
    	</tr>
</table>



</td>

<td style="vertical-align:top;width:105px;background-color: #C1E2FC;font-size:19px">
<table>
<tr>
    			<td><label class="pointer" for="credit">Борг</label></td>
    			<td> 
				<% if (credit == true) { %>
  			  		<input type="checkbox" autocomplete="off" class="model-part" name="credit" value="true" id="credit" checked>
				<% } else {%>
 					<input type="checkbox" autocomplete="off" class="model-part" name="credit" value="true" id="credit">
				<% } %>

			
    				 
    			</td>
    	</tr>
		<tr> <td><label class="pointer" for="excise">Акциз</label></td>
			 <td>
				<% if (excise == true) { %>
  			  		<input type="checkbox" autocomplete="off" class="model-part" name="excise" value="true" id="excise" checked>
				<% } else {%>
 					<input type="checkbox" autocomplete="off" class="model-part" name="excise" value="true" id="excise">
				<% } %>
		
			</td>
		</tr>
    	</table>

</td>

</tr></table>
   
		
    </fieldset>
    
<fieldset>
	<legend>Продукти</legend>
    <table id="products-step-1" style="display:none">
    	<tr>
    		<td class="first-colums">
    			<table></table>
    		</td>
			<td valign="top" class="second-column">
				<div id="select-all-product" class="uibutton">Вибрати всi</div>
				<div id="select-product" class="uibutton">Далi</div>

			</td>
    	</tr>
    </table>
	<table id="products-step-2" style="display:none">
    	<tr>
    		<td>
				
    			<table id="product-list">
				<tr>
					<th class="sortable" data-field="name">Продукт</th>
					<th class="product-def-price sortable number" data-field="price">Цiна</th>
					<th class="product-exc-price hidden sortable number" data-field="excisePrice">Цiна(Акциз)</th>
					<th class="sortable number" data-field="margin">Нацiнка(%)</th>
					<th class="sortable number" data-field="marginPrice">Цiна продажу</th>
					<th class="sortable" data-field="quantity">Кiлькiсть</th>
				</tr>
				</table>
				<div id="add-product" class="uibutton">Створити новий продукт</div> <div id="un-select-product" class="uibutton">Повернутись до вибору продуктiв</div>
    		</td>
    	</tr>
    </table>
</fieldset>

	
<button type="button" id="save-invoice" style="display:none">Зберегти</button>
  </form>

</script>

<div id="excise-types-product-view">
</div>

<div id="dialog-cigarette">
</div>

<div id="dialog-form">
<div id="invoice-dialog-view">
  
  </div>
</div>



<div id="search-form">
<div id="invoice-search-view">
  
  </div>
</div>

<div id="dialog-revenue">
<div id="revenue-dialog-view">
  
  </div>
</div>

<div id="invoice-wrapper" style="position:relative">

<div id="datepicker" style="z-index:1"></div>
	<div id="date-block">
		<input type="hidden" id="invoiceDate"/>
		<span></span>
	</div>
	
	
<div id=invoice-container-wrapper>
	<div id=invoice-container>
	
	<table id='invoice-layout'>
	<tr><td class="invoice-revenue-layout" valign="top">
	<div id="invoice-container">
		<div id="table-header" class="lockable-header">
			<span>Накладнi</span>	<button type="button" class="blockable" id="add-invoice">Додати накладну</button>
		</div>
		
		<div id="table-header-info" class="">
			<span></span>	
		</div>
		
		<table style="width:100%" id="invoice-table-header" class="table-header invoice-row">
				<tr>
				<th class="number-column">№</th>
				<th class="date-column">Дата</th>
				<th class="supplier-column">Постачальник</th>
				<th class="margin-column">Нацiнка (середня)</th>
				<th class="total-column">Сума накладної</th>
				<th class="income-column">Приход</th>
				<th class="expenses-column">Витрати</th>
				</tr>
			</table>
			
			<div id="invoice-table"></div>
		</div>
		<div id="expenses-container" class="lockable-header">
			<div id="table-header">
				<span>Iншi витрати</span>	<button type="button" class="blockable" id="add-revenue-expenses">Додати (витрати/доходи)</button>
			</div>
			
			<table style="width:100%" class="table-header">
				<tr>
					<th class="revenue-number">№</th>
					<th class="revenue-type">Тип</th>
					<th class="revenue-comment">Коментар</th>
					<th class="revenue-revenue">Приход</th>
					<th class="revenue-expenses">Витрати</th>
				</tr>
			</table>
			<div id="expenses-table"></div>
		</div>
	
	</td>
		<td class="balance-layout" valign="top">
		<div id="summary">
			<fieldset>
				<legend>Накладнi</legend>
				<table>
					<tr><td class="t-header">Приход</td><td><span id="summary-invoice-revenue"></span></td></tr>
					<tr><td class="t-header">Витрати</td><td><span id="summary-invoice-expenses"></span></td></tr>
					<tr><td class="t-header">Сума</td><td><span id="summary-invoice-summ"></span></td></tr>
					<tr><td class="t-header">Середня нацiнка</td><td><span id="summary-invoice-average-margin"></span></td></tr>
				</table>
			</fieldset>
			
			<fieldset>
				<legend>Приход / Витрати</legend>
				<table>
					<tr><td class="t-header">Приход</td><td><span id="summary-revenue-revenue"></span></td></tr>
					<tr><td class="t-header">Витрати</td><td><span id="summary-revenue-expenses"></span></td></tr>
				</table>
			</fieldset>
			
			<fieldset>
				<legend>Зміни продавців</legend>
				<table>
					<tr>
						<td class="t-header">І</td>
						<td> <span class="seller-title" id="first-seller"></span><select id="first-seller-select" class="summary-seller-select"></select></td>
						<td><span class="seller-title" id="second-seller"></span><select id="second-seller-select" class="summary-seller-select"></select></td>
					</tr>
					<tr>
						<td class="t-header">ІI</td>
						<td><span class="seller-title" id="third-seller"></span><select id="third-seller-select" class="summary-seller-select"></select></td>
						<td><span class="seller-title" id="fourth-seller"></span><select id="fourth-seller-select" class="summary-seller-select"></select></td>
					</tr>
				</table>
			</fieldset>
			
			<table>
				<tr><td class="t-header">Заг. Приход</td><td><span id="summary-total-revenue"></span></td></tr>
				<tr><td class="t-header">Заг. Витрати</td><td><span id="summary-total-expenses"></span></td></tr>
				<tr class="balance"><td>Залишок</td><td><span id="summary-balance"></span></td></tr>
			</table>
			
			<div id="balance-edit" style="display:none">Залишок на початок дня <br/> <input type="text" id="balance-real-value" name="balance_real"/><button type="button" id="save-balance">Зберегти</button></div>
			<div id="credit-invoices"><a href="#" id="credit-invoices-link"> Накладнi з боргом<span id="summary-credit"></span></a></div>
			<div id="credit-invoices"><a href="#" id="find-invoices-link"><span >Пошук накладних</span></a></div>
		</div>
		</td></tr>
	
	</table>
		
		
	</div>
</div>
</div>



<script type="text/javascript">

$('#save-balance').on('click', function(){
	$.ajax({
		url:"/rest/balance/" + $('#invoiceDate').val().replace(/-/g,''),
		method:"POST",
		data: {"balance":$('#balance-real-value').val()},
		success: function(ans) {
			if(ans.balancePk){
				alert("Збережено");
			}
		}
	})
});


$(function(){
	
	$('.seller-title').click(function(e){
		$('.seller-title').show();
		$('.summary-seller-select').hide();
		$(this).hide();
		$(this).closest('td').find('select').show();
	});
	
	
	$('.summary-seller-select').change(function(e){
		
		$(e.currentTarget).closest('td').find('span').html($(e.currentTarget).find("option:selected").text())
		
		var obj = {date: $('#invoiceDate').val()};
		
		if($('#first-seller-select').val()) {
			obj.firstSeller = {'id' : $('#first-seller-select').val()}
		}
		if($('#second-seller-select').val()) {
			obj.secondSeller = {'id' : $('#second-seller-select').val()}
		}
		if($('#third-seller-select').val()) {
			obj.thirdSeller ={'id' :  $('#third-seller-select').val()}
		}
		if($('#fourth-seller-select').val()) {
			obj.fourthSeller = {'id' : $('#fourth-seller-select').val()}
		}
			
		
		$.ajax({
			url:"/rest/schedule",
			method:"POST",
			dataType: 'json',
			contentType: "application/json;charset=utf-8",
			data:JSON.stringify(obj),
			success:function(){
				$('.seller-title').show();
				$('.summary-seller-select').hide();
			}
		})
	})
	
	
	$('body').on('loadSummary', function(){
		$.ajax({
			url:"/rest/balance/summary/" + $('#invoiceDate').val().replace(/-/g,''),
			method:"GET",
			success: function(response) {
					$('#first-seller,#second-seller,#third-seller,#fourth-seller').html(" - ");
					$('.summary-seller-select').html('');
					$('.summary-seller-select').data('id', '');
					if(response.sellerSchedule) {
						if(response.sellerSchedule.firstSeller) {
							$('#first-seller').html(response.sellerSchedule.firstSeller.name);
							$('#first-seller-select').data('id', response.sellerSchedule.firstSeller.id);
						} 
						if(response.sellerSchedule.secondSeller) {
							$('#second-seller').html(response.sellerSchedule.secondSeller.name);
							$('#second-seller-select').data('id', response.sellerSchedule.secondSeller.id);
						}
						if(response.sellerSchedule.thirdSeller) {
							$('#third-seller').html(response.sellerSchedule.thirdSeller.name);
							$('#third-seller-select').data('id', response.sellerSchedule.thirdSeller.id);
						}
						if(response.sellerSchedule.fourthSeller) {
							$('#fourth-seller').html(response.sellerSchedule.fourthSeller.name);
							$('#fourth-seller-select').data('id', response.sellerSchedule.fourthSeller.id);
						}
					}
					
					$.ajax({
						url:"/rest/schedule/seller",
						method:"GET",
						success: function(response) {
							$('.summary-seller-select').each(function(i,el) {
								$(el).append("<option value=''></option>")
								_.each(response, function(seller){
									if($(el).data('id') == seller.id) {
										$(el).append("<option value='"+seller.id+"' selected>"+seller.name+"</value>")
									} else {
										$(el).append("<option value='"+seller.id+"'>"+seller.name+"</value>")
									}
								})
							})
						}
					});
				
				$('#summary-invoice-revenue').html(response.invoiceRevenue);
				$('#summary-invoice-expenses').html(response.invoiceExpenses);
				$('#summary-invoice-summ').html(response.invoiceSumm);
				$('#summary-invoice-average-margin').html(response.invoiceAverageMargin);
				$('#summary-revenue-revenue').html(response.revenueRevenue);
				$('#summary-revenue-expenses').html(response.revenueExpenses);
				$('#summary-total-revenue').html(response.totalRevenue);
				$('#summary-total-expenses').html(response.totalExpenses);
				$('#summary-balance').html(response.balanceCalculated);
				$('#summary-credit').html( "(" + response.numberOfCreditInvoices + ")");
			}
		});
	});
	
	$('body').trigger('loadSummary');
	
})



var revenueTypes = ${revenueType};
var revenueTypesArr = ${revenueTypeArr};

var dialog, dialogRevenue, dialogSearch, suppliersList;





$(function(){
	var revenueView = new RevenueExpensesDialogView({model:$("#dialog-revenue" ).data("model")});
	var invoiceView = new InvoiceDialogView({model:$("#dialog-form" ).data("model")});
	
	
	dialog = $( "#dialog-form" ).dialog({
	    autoOpen: false,
	    height: 600,
	    width: 800,
	    modal: true,
	    open: function() {
	    	invoiceView.reinit({model:$("#dialog-form" ).data("model"), dialog:dialog})
	    },
	    close: function() {
	    }
	});
	
	dialogRevenue = $( "#dialog-revenue" ).dialog({
	    autoOpen: false,
	    height: 300,
	    width: 450,
	    modal: true,
	    open: function() {
	    	revenueView.reinit({model:$("#dialog-revenue" ).data("model"), dialog:dialogRevenue});
	    },
	    close: function() {

	    }
	});
	
	
	
	dialogSearch = $( "#search-form" ).dialog({
	    autoOpen: false,
	    title : "Пошук накладних",
	    height: 250,
	    width: 500,
	    modal: true,
	    open: function() {
	    	//invoiceView.reinit({model:$("#dialog-form" ).data("model"), dialog:dialog})
	    },
	    close: function() {
	    }
	});
	
	new InvoiceSearchView();
})




$('#find-invoices-link').click(function(e){
	e.preventDefault();
	dialogSearch.dialog("open");
})


$( "#add-invoice" ).button().on( "click", function() {
	$("#dialog-form" ).data("model", null);
	dialog.dialog( "option", "title",  "Нова накладна" );
    dialog.dialog("open");
    console.log(dialog)
  });
  
$( "#add-revenue-expenses" ).button().on( "click", function() {
	$( "#dialog-revenue" ).data("model", null);
	dialogRevenue.dialog("open");
  });
  

 var date = new Date();
 $('#date-block span').html(date.getDate() + ' ' + monthNames[date.getMonth()] + ' ' + date.getFullYear())
 setCurrentDate(date);
 
 $(function() {
    $( "#datepicker" ).datepicker({
    	dateFormat: "yymmdd",
    	onSelect: function(value, date) { 
             $('#datePicker').val(value);
             $('#date-block span').html(date.currentDay + ' ' + monthNames[date.currentMonth] + ' ' + date.currentYear);
             setCurrentDate(date, "change");
             $("#datepicker").hide(); 
          } 
    	
    });
    
    $('#summary .balance').on('dblclick',function(){
		$('#balance-edit').toggle();
	});
    
    
    $("#date-block span").click(function(){
        $("#datepicker").toggle();
    }); 
  });
</script>