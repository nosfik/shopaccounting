<script id="exciseTemplate" type="text/template">
	
	<% _.each(data, function(type) { %>  
	<h2 class='title-res'><%-type.name%></h2>
	<table>
		<tr>
			<th class="product">Продукт</th>
			<th>Кількість</th>
			<th>Об'єм</th>
			<th>Заг. об'єм</th>
			<th>Дата</th>
		</tr>
	<% _.each(type.products, function(product) { %>  
		<tr class="excise-product-item">
			<td><%-product.name%></td>
			<td><%=product.quantity%></td>
			<td><%=product.volume%></td>
			<td class="<%= ((product.volumeSumm == null) ? 'empty' :'')%>"><b><%=product.volumeSumm%></b></td>
			<td><%=product.date%></td>
		</tr>
	<% }); %>

	<tr class="reslut-row"><td colspan="5"><h2 class='result'>Всього : <%=type.sum%></h2></td></tr>
	</table>
	
	<br/><br/>	

	<% }); %>
	

</script>

<div id='statistic-wrapper'>
	<div id="statistic-block">
		
		
		<table style="width:100%">
				<tr>
					<td valign="top" style="width:320px">
					<div id="statistic-module">
						<h2>Акциз</h2>
						
						<table id="proceed-control" class="not-active">
						<tr><td><h4 style="padding-right:15px">Акцизні товари</h4></td><td style="text-align: right"><button type="button" style="margin-bottom: 15px;" class="uibutton" id="calculate">Показати</button></td></tr>
							<tr><td><span class="stat-table-title">З дати:</span></td><td><input type="text" class="datepicker" id="excise-stat-from" name="from"/></td></tr>
							<tr><td><span class="stat-table-title">По дату:</span></td><td><input type="text" class="datepicker" id="excise-stat-to" name="to"/></td></tr>
						</table>
					</div>
					</td>
					<td><div id="calculation"></div></td>
				</tr>
			</table>
	</div>
</div>

<script>
$( ".datepicker" ).datepicker({
	dateFormat: "yy-mm-dd"
});

$('#calculate').click(function(){
	if($('#excise-stat-from').val() ==  '' || $('#excise-stat-from').val() ==  '') {
		Shop.Popup.warn('Увага',"Не всі поля заповнені");
		return;
	}
	$.ajax({
		url : "/rest/statistic/excise/volume/" + $('#excise-stat-from').val().replace(/-/g,'') + "/" +  $('#excise-stat-to').val().replace(/-/g,''),
		success : function(response) {
			$('#calculation').empty();
			$('#calculation').append(_.template($('#exciseTemplate').html())(response))
		}
	});
	

});

</script>