<script id="revenuePrint" type="text/template">
	<tr>
		<td class="revenue-type"><%=revenueTypes[type]%></td>
		<td><%=revenue%></td>
		<td><%=expenses%></td>
	</tr>
</script>


<script id="invoicePrint" type="text/template">

<% if (expenses) { %>
			<tr class="iheader"><td colspan="4" class="supplier"><b><%-supplier.name%> (Витрати : <%=expenses%>)</b></td></tr>
<% } else { %>
			<tr class="iheader"><td colspan="4" class="supplier"><b><%-supplier.name%></b></td></tr>
<% } %>




		<% _.each(products, function(p) { %>  
 	 <tr>
         <% if (p.code) { %>
		<td class="product-name"><%-p.name%><b>[<%=p.code%>]</b></td>
         <% } else { %>
         <td class="product-name"><%-p.name%></td>
         <% } %>
		<td class="product-maring-price"><%=Shop.util.getPriceView(p.marginPrice)%></td>
		<td class="product-quantity"><%=p.quantity%></td>
		<td class="product-total-with-margin"><%=Shop.util.getPriceView(p.totalWithMargin)%></td>
    </tr>
	<% }); %>
<tr class="ifooter" style="background-color:lightgrey">
		<td class="product-name">Всього</td>
		<td class="product-maring-price"></td>
		<td class="product-quantity"></td>
		<td class="product-total-with-margin"><%=Shop.util.getPriceView(totalWithMargin)%></td>
</tr>
</script>

<script id="revenuePrint" type="text/template">

</script>


<script id="invoiceRow" type="text/template">
	<tr>	

<% if (typeof(print) !== "undefined" && print === true) { %>
			<tr class="checked">
<% } else { %>
			<tr>
<% } %>
			<td class="number-column"><%=number%></td>
			<td class="supplier-column supplier-table-name"><%-supplier.name%></td>
			<td class="margin-column"><%=averageMargin%></td>
			<td class="total-column"><%=Shop.util.getPriceView(total)%></td>
			<td class="income-column"><%=Shop.util.getPriceView(totalWithMargin)%></td>
			<td class="expenses-column"><%=expenses%></td>

			
<% if (typeof(print) !== "undefined" && print === true) { %>
			<td class="print-column"><input type="checkbox" name="print" checked="checked"/></td>
<% } else { %>
			<td class="print-column"><input type="checkbox" name="print"/></td>
<% } %>
			
	</tr>
</script>

<script id="revenueExpensesRow" type="text/template">
<tr>
			<td class="revenue-number"><%=number%></td>
			<td class="revenue-type"><%=type_name%></td>
			<td class="revenue-comment"><%-comment%></td>
			<td class="revenue-revenue"><%=Shop.util.getPriceView(revenue)%></td>
			<td class="revenue-expenses"><%=Shop.util.getPriceView(expenses)%></td>
			<td class="revenue-print"><input type="checkbox" name="print" style="width:100%"/></td>
</tr>
</script>

<script id="invoice-full" type="text/template">
<td colspan='7' class="no-padding">

<table>
<tr><td>

<table>
<tr>
<th>Товар</th>
<th>Цiна</th>
<th>Нацiнка</th>
<th>Цiна продажу</th>
<th>Кiлькiсть</th>
<th>Сума накладної</th>
<th>Приход</th>

</tr>
<% _.each(products, function(p) { %>  
  <tr>
<td class="product-name"><%-p.name%></td>
<td class="product-price"><%=Shop.util.getPriceView(p.price)%></td>
<td class="product-margin"><%=p.margin%></td>
<td class="product-maring-price"><%=Shop.util.getPriceView(p.marginPrice)%></td>
<td class="product-quantity"><%=p.quantity%></td>
<td class="product-total"><%=Shop.util.getPriceView(p.total)%></td>
<td class="product-total-with-margin"><%=Shop.util.getPriceView(p.totalWithMargin)%></td></tr>
<% }); %>
</table>

</td></tr>
</table>

</td>



</script>


<div id="invoice-wrapper" style="position:relative">

<div id="datepicker" style="z-index:1"></div>
	<div id="date-block">
		<input type="hidden" id="invoiceDate"/>
		<span></span>
	</div>
	
	
<div id=invoice-container-wrapper>
	<div id=invoice-print-container>
	
	<div id="invoice-container">
	
		<table style="width:100%">
		<tr><td><h2>Виберiть накладнi якi потрiбно роздрукувати</h2></td> <td style="text-align:right"><button type="button" class="uibutton" id="build-print-view">Перейти Далi</button></td></tr>
		</table>
		
		
		<div id="table-header">
			<span>Накладнi</span>
		</div>
		
	
			
			<div id="invoice-table">
				<div id="invoice-table-header">
					<table style="width:100%" id="invoice-table-header" class="table-header invoice-row">
						<tr>
							<th class="number-column">№</th>
							<th class="date-column">Дата</th>
							<th class="supplier-column sortable" data-field="supplier.name">Постачальник</th>
							<th class="margin-column">Нацiнка (середня)</th>
							<th class="total-column sortable" data-field="total">Сума накладної</th>
							<th class="income-column sortable" data-field="totalWithMargin">Приход</th>
							<th class="expenses-column">Витрати</th>
							<th class="print-column">Друк</th>
						</tr>
					</table>
				</div>
				<div id="invoice-table-body">
				
				</div>
			
			</div>
		</div>
		<div id="expenses-container" class="lockable-header">
			<div id="table-header">
				<span>Iншi витрати</span>
			</div>
			
			<table style="width:100%" class="table-header">
				<tr>
					<th class="revenue-number">№</th>
					<th class="revenue-type">Тип</th>
					<th class="revenue-comment">Коментар</th>
					<th class="revenue-revenue">Приход</th>
					<th class="revenue-expenses">Витрати</th>
					<th class="revenue-print">Друк</th>
				</tr>
			</table>
			<div id="expenses-table"></div>
		</div>
		
	</div>
	<div id="page-emulator"></div>
	<div id="print-area">
		<h2 id="invoice-title"></h2>
		<table id="total-print" class="print-table">
			<tr>
				<th>Приход накл.</th>
				<th>Витрати накл.</th>
				<th>Приход(iн.)</th>
				<th>Витрати(iн.)</th>
				<th>Приход</th>
				<th>Витрати</th>
				<th style="background-color: lightgrey">Залишок</th>
			</tr>
			<tr>
				<td class="invoice-revenue"></td>
				<td class="invoice-expenses"></td>
				<td class="revenue-revenue"></td>
				<td class="revenue-expenses"></td>
				<td class="revenue"></td>
				<td class="expenses"></td>
				<td style="background-color: lightgrey" class="balance"></td>
			</tr>
		</table>
		
		<table id="invoice-print-products" class="incoive-print-table print-table">
		<thead>
			<tr>
				<th>Продукт</th>
				<th>Цiна</th>
				<th>К</th>
				<th>Приход</th>
			</tr>
		</thead>
		<tbody></tbody>
		</table>
		
		<table id="revenue-print" class="revenue-print-table print-table no-break">
		<thead>
			<tr class="revenue-head">
				<th colspan="4"><h2>Iншi витрати</h2></th>
			</tr>
			<tr>
				<th>Тип</th>
				<th>Приход</th>
				<th>Витрати</th>
			</tr>
		</thead>
		<tbody></tbody>
		</table>
		
		
		
	</div>
	<div id="control-print">
		
		<table>
		
		<tr>
		<td><div style="margin-bottom:20px;">
			<h3>Шрифт</h3>
			<br/>
			<button type="button" class="font-button" onclick="setFont(7, this)">7</button>
			<button type="button" class="font-button" onclick="setFont(7.5, this)">7.5</button>
			<button type="button" class="font-button" onclick="setFont(8, this)">8</button>
			<button type="button" class="font-button" onclick="setFont(8.5, this)">8.5</button>
			<button type="button" class="font-button font-selected" onclick="setFont(9, this)">9</button>
			<button type="button" class="font-button" onclick="setFont(10, this)">10</button>
			<button type="button" class="font-button" onclick="setFont(11, this)">11</button>
			<button type="button" class="font-button" onclick="setFont(12, this)">12</button>
			<button type="button" class="font-button" onclick="setFont(13, this)">13</button>
			<button type="button" class="font-button" onclick="setFont(14, this)">14</button>
			<button type="button" class="font-button" onclick="setFont(15, this)">15</button>
			<button type="button" class="font-button" onclick="setFont(16, this)">16</button>
		</div></td>
		
		<td style="padding: 15px 0 0 100px"><button type="button" id="back-to-start-button" class="uibutton" onclick="$('#control-print').hide();$('#print-area').hide();$('#page-emulator').hide();$('#invoice-print-container').show();$('#build-print-view').show();">Повернутись назад</button>
	</td>
		
		
		</tr>
		
		<tr><td colspan=2>
		<button type="button" id="print-table-button" class="uibutton" onclick="printInv()">Друк</button>
		<button type="button" id="split-table" class="uibutton" onclick="splitInv('',true,0);$('.incoive-print-table').addClass('no-break');$('#reset-table').show();$(this).hide()">Розбити таблицю</button>
		<button type="button" id="reset-table" class="uibutton" style="display:none" onclick="$('#print-area .incoive-print-table, #print-area .revenue-print-table').remove();$('#print-area').append(Shop.invoice.invoiceTable.clone());$('#print-area').append(Shop.invoice.revenueTable.clone());$('#split-table').show();$(this).hide();">Назад до таблицi</button>
	
		</td></tr>
		
		</table>
		
	
	
			
		
	</div>
	
	
</div>
</div>



<script type="text/javascript">

var revenueTypes = ${revenueType};
  
function setCurrentDate(date, trigger) {
	
	var year, month, day;
	
	if(date.currentYear) {
		year = date.currentYear;
		month = date.currentMonth;
		day = date.currentDay;
	} else {
		year = date.getFullYear();
		month = date.getMonth();
		day = date.getDate();
	}
	
	month += 1;
	
	if(month <= 9) {
		month = "0" + month;
	}
	
	if(day <= 9) {
		day = "0" + day;
	}
	
	 $('#invoiceDate').val("" + year +'-'+ month +'-'+ day);
	 
	 if(trigger) {
		 $('#invoiceDate').trigger(trigger);
	 }
	
}

function setFont(font, el) {
	$('#print-area').css('font-size', font + 'pt');
	$('body').removeClass();
	
	var tmp = "font-" + font;
	
	$('body').addClass(tmp.replace(".","-"));
	$('.font-button').removeClass('font-selected')
	$(el).addClass('font-selected')
}

function printInv() {
	$('#back-to-start-button').hide();
    var printContents = document.getElementById("print-area").innerHTML;
    var originalContents = document.body.innerHTML;
    document.body.innerHTML = printContents;
    window.print();
    document.body.innerHTML = originalContents;
}



function splitInv(table, firstColumn, column) {
	var splitHeight = (column < 2) ? ($('#page-emulator').height() - $('#invoice-title').outerHeight() - $('#total-print').outerHeight()): $('#page-emulator').height();
	var height = 0;
	var header = $('#invoice-print-products thead');
	var lastFooterEl;
	var invTable = (table) ? table :  $('#invoice-print-products');
	var rows = invTable.find('tr');
	
	if(firstColumn) {
		invTable.addClass("first-column");
	} 
	
	rows.each(function(i, item) {
		height += $(item).height();
		if(height > splitHeight) {
			var otherTable = lastFooterEl.nextAll().clone();
			lastFooterEl.nextAll().remove();
			var string = "";
			$.each(otherTable, function(i,val) {
				string += val.outerHTML;
			});
			var newTable = $("<table class='incoive-print-table print-table'>" + header.html() + "<tbody> "+string+"</tbody></table>");
			lastFooterEl.closest('table').after(newTable);
			splitInv(newTable, firstColumn^true, ++column);
			return false;
		}
		if($(item).hasClass('ifooter')) {
			lastFooterEl = $(item);
		}
		
	});
}

var monthNames = [
"Січня", "Лютого", "Березня", "Квітня", "Травня", "Червня", 
"Липня", "Серпня", "Вересня", "Жовтня", "Листопада", "Грудня"
                ];
                var date = new Date();
                $('#date-block span').html(date.getDate() + ' ' + monthNames[date.getMonth()] + ' ' + date.getFullYear())
                setCurrentDate(date);
 $(function() {
	 
    $( "#datepicker" ).datepicker({
    	dateFormat: "yymmdd",
    	onSelect: function(value, date) { 
             $('#datePicker').val(value);
             $('#date-block span').html(date.currentDay + ' ' + monthNames[date.currentMonth] + ' ' + date.currentYear);
             setCurrentDate(date, "change");
             $("#datepicker").hide(); 
          } 
    });
    
    $('#summary .balance').on('dblclick',function() {
		$('#balance-edit').toggle();
	});
    
    
    $("#date-block span").click(function(){
        $("#datepicker").toggle();
    }); 
  });
</script>