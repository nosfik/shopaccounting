<div id='statistic-wrapper'>
<div style="width:100%; height:35px;background-color:white;background-color: #748CAF;padding:2px 0 5px 0">


<table style="margin:5px auto">
<tr>
<td><input type="text" class="datepicker" id="date-from" name="from" style="font-size: 20px; width: 130px; padding-left: 10px;    color: #615F5F;"/></td>
<td>&nbsp;-&nbsp;</td>
<td><input type="text" class="datepicker" id="date-to" name="to" style="font-size: 20px; width: 130px; padding-left: 10px;    color: #615F5F;"/></td>
</tr>


</table>

</div>
	<div id="statistic-block-wrapper" style="padding:20px">
	
	<table style="width:100%">
		<tr>
		<td style="width:70%"><div style="padding:10px" id="statistic-block-revenue"></div></td>
		<td style="width:30%;"><div style="padding:10px;" id="statistic-block-other"></div></td>
		</tr>
		<tr>
		<td colspan=2 style="width:100%"><div style="padding:10px" id="statistic-block-invoice-revenue"></div></td>
		</tr>
		<tr>
		<td colspan=2 style="width:100%"><div style="padding:10px" id="statistic-block-top-suppliers"></div></td>
		</tr>
		<tr>
		<td colspan=2 style="width:100%"><div style="padding:10px" id="statistic-block-supplier-invoice-revenue"></div></td>
		</tr>
		<tr>
		<td colspan=2 style="width:100%"><div style="padding:10px" id="statistic-block-seller-1"></div></td>
		</tr>
		<tr>
		<td colspan=2 style="width:100%"><div style="padding:10px" id="statistic-block-seller-2"></div></td>
		</tr>
	</table>
		
		
			
	</div>
</div>

<script src="/highchart/highcharts.js"></script>


<script>

var revenueTypes = ${revenueType};

$( ".datepicker" ).datepicker({
	dateFormat: "yy-mm-dd"
});




function init() {
	
	var date = new Date();
	setCurrentDate(date, null, $('#date-to'))
	date.setTime( date.getTime() - 30*1000*60*60*24*1);
	setCurrentDate(date, null, $('#date-from'));
	initGraphs();
	$('#date-to, #date-from').change(function(){
		initGraphs();
	})
}

init();
function initGraphs () {
	
	var from =  $('#date-from').val().replace(/-/g,''), to = $('#date-to').val().replace(/-/g,'');
	
	
	
	$.ajax({
		url : "/rest/statistic/general/proceed/" + from + "/" +  to,
		success : function(response) {
			var sum = 0.0;
			_.each(response.data2[0].data, function(proceed){
				sum += proceed; 
			})
			
			buildProceedGraph(response.data1, response.data2, Shop.util.formatPrice(sum));
		}
	})
	
	$.ajax({
		url : "/rest/statistic/general/other/" + from + "/" +  to,
		success : function(response) {
			buildOtherGraph(response.data1, response.data2);
		}
	})
	
	$.ajax({
		url : "/rest/statistic/general/invoice/revenue/" + from + "/" +  to,
		async: false,
		success : function(response) {
			buildInvoiceRevenueGraph(response.data1, response.data2);
		}
	})
	
	$.ajax({
		async: false,
		url : "/rest/statistic/general/invoice/supplier/" + from + "/" +  to,
		success : function(response) {
			buildTopSuppliersGraph(response.data1, response.data2);
		}
	})
	
	
	
	$.ajax({
		async: false,
		url : "/rest/statistic/general/seller/revenue/" + from + "/" +  to,
		success : function(response) {
			buildSellerGrpaph(response, "1");
			buildSellerGrpaph(response, "2");
		}
	})
	
}

var topSuppliersChart;

function buildTopSuppliersGraph(data1,data2){
	
	var config = {
        enabled: true,
        rotation: -75,
        color: 'black',
        crop:false,
      //verticalAlign:'bottom',
      //inside:true,
        style: {
            fontWeight: 'bold'
        
		},
        formatter: function() {
        	if(this.series.name.length > 15) {
        		return this.series.name.substr(0,13) + ".."
        	}
            return this.series.name;
        },
        y: 5, 
        style: {
            fontSize: '13px',
            fontFamily: 'Verdana, sans-serif'
        }
    };
	
	var event = {
            legendItemClick: function(event) {
            	console.log(this.visible)
               var visibility = this.visible ? 'visible' : 'hidden';
               var newSum = 0;
               for(var i = 0; i < topSuppliersChart.series.length;i++) {
            	   var serie = topSuppliersChart.series[i];
            	   if(serie.name == this.name) {
            		   if(!this.visible)
            		   	newSum += serie.yData[0];
            	   } else if (serie.visible){
            		   newSum += serie.yData[0];
            	   }
               }
               topSuppliersChart.setTitle({text: 'Топ 25 (Постачальники). Всього - <b>' + Shop.util.formatPrice(newSum) + '</b>'});
            }
        }
	
	var sum = 0;
	for(var i = 0; i < data2.length; i++) {
		sum+=data2[i].data[0].y;
		data2[i].dataLabels = config;
		data2[i].events = event;
	}
	
	
	topSuppliersChart = new Highcharts.Chart({
		 	chart: {
	            renderTo: 'statistic-block-top-suppliers',
	            type: 'column'
	         },
	        title: {
	            text: 'Топ 25 (Постачальники). Всього - <b>' + Shop.util.formatPrice(sum) + '</b>'
	        },
	        tooltip: {
	        	 headerFormat: '<span style="font-size:15px">{series.name}</span><br>',
	            pointFormat: '<b>{point.y:,.0f}</b>',
	          	style : { fontSize: "18px" }
	        },
	        xAxis: {
	        	categories:data1,
	        	 minPadding: 0,
	             maxPadding: 0.03
	        },
	        yAxis: {
	            title: {
	                text: 'гривень'
	            }
	        },
	       
	        
	        plotOptions: {
	        	
	            bar: {
	                dataLabels: {
	                    enabled: true
	                }
	            },
	            
	             series: {
	            	 cursor:'pointer',
	            	 events: {
	                     click: function (e) {
	                    	 console.log(this)
	                    	 if(this.data[0].options.id) {
	                    			var from =  $('#date-from').val().replace(/-/g,''), to = $('#date-to').val().replace(/-/g,'');
	                    		 $.ajax({
	                    				url : "/rest/statistic/general/invoice/supplier/" + this.data[0].options.id + "/" + from + "/" +  to,
	                    				success : function(response) {
	                    					buildSupplierInvoiceRevenueGraph(response.data1, response.data2, this.name, this.color );
	                    				}.bind(this)
	                    			})
	                    	 }
	                     }
	                 },
	                pointPadding: 0.05,
	                groupPadding: 0
	            } 
	        },
	        
	       
	      
	        series:data2
	    });
}


function buildSupplierInvoiceRevenueGraph(data1, data2, title, color) {
	
	$('#statistic-block-supplier-invoice-revenue').highcharts({
        chart: {
            type: 'area'
        },
      
        colors: [color],
        title: {
            text: 'Приход за період <b>'+ title + '</b>'
        },
        tooltip: {
            pointFormat: 'Приход <b>{point.y:,.0f}</b>'
        },
        xAxis: {
        	categories: data1
        },
        yAxis: {
            title: {
                text: 'Приход'
            },
            labels: {
                formatter: function () {
                    return this.value;
                }
            }
        },
      
        series: data2
    
    });
	
}

function buildInvoiceRevenueGraph(data1, data2){
	
	
	$('#statistic-block-invoice-revenue').highcharts({
        chart: {
            type: 'area'
        },
      
        colors: ['#93E796'],
        title: {
            text: 'Приход за період'
        },
        tooltip: {
            pointFormat: 'Приход <b>{point.y:,.0f}</b>'
        },
        xAxis: {
        	categories: data1
        },
        yAxis: {
            title: {
                text: 'Приход'
            },
            labels: {
                formatter: function () {
                    return this.value;
                }
            }
        },
      
        series: data2
    
    });
	
	
}

function buildSellerGrpaph(response, shift){
	
	
	
	var series = [];
	response = JSON.parse(response);
	var respFirst = response[shift];
	for(var i = 0; i < respFirst.length; i++) {
		var seller = {};
		seller.name = respFirst[i].name;
		seller.data = [];
		for(var j = 0; j < respFirst[i].data.length;j++) {
			if(respFirst[i].data[j].proceed) {
				var tmpArr = [];
				var date = respFirst[i].data[j]['date'].split('-')
				tmpArr.push(Date.UTC(date[0], (+date[1] - 1), (+date[2])));
				tmpArr.push(respFirst[i].data[j].proceed);
				seller.data.push(tmpArr);
			}
			
		}
		series.push(seller)
		
	}
	
	var el = '1' == shift ? $('#statistic-block-seller-1') : $('#statistic-block-seller-2');
	
	
	 el.highcharts({
	        chart: {
	            type: 'spline'
	        },
	        title: {
	            text: 'Статистика по продавцям ('+ shift +' зміна)'
	        },
	     
	        xAxis: {
	            type: 'datetime',
	            dateTimeLabelFormats: { // don't display the dummy year
	                month: '%e. %b',
	                year: '%b'
	            },
	            title: {
	                text: 'Date'
	            }
	        },
	        yAxis: {
	            title: {
	                text: 'Виручка (грн)'
	            },
	            min: 0
	        },
	        tooltip: {
	            headerFormat: '<b>{series.name}</b><br>',
	            pointFormat: '{point.x:%e. %b}: {point.y:.2f} грн'
	        },
	        
	        plotOptions: {
	            spline: {
	                marker: {
	                    enabled: true
	                }
	            }
	        },
	      
	        series: series
	    
	    });
	
}

function buildProceedGraph(data1, data2, sum){
    $('#statistic-block-revenue').highcharts({
        chart: {
            type: 'area'
        },
        title: {
            text: 'Виручка за період ' + '(' + sum + ' грн.)'
        },
        tooltip: {
            pointFormat: 'Виручка <b>{point.y:,.0f}</b>'
        },
        xAxis: {
        	categories: data1
        },
        yAxis: {
            title: {
                text: 'Виручка'
            },
            labels: {
                formatter: function () {
                    return this.value;
                }
            }
        },
      
        series: data2
    
    });
}

function buildOtherGraph(data1, data2) {
	
	 $('#statistic-block-other').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Інші витрати'
        },
        tooltip: {
            pointFormat: '<b>{point.y:,.0f}</b>',
          	style : { fontSize: "18px" }
        },
        xAxis: {
        	categories: data1,
        	labels : {
        		style : { fontSize: "18px" }
        	}
        },
        yAxis: {
            title: {
                text: 'гривень'
            },
            labels: {
           		 style : { fontSize: "18px" },
                formatter: function () {
                    return this.value;
                }
            }
        },
      
        series:data2
    });

}
</script>