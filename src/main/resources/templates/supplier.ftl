<script id="supplierTemplate" type="text/template">
	<div class="supplier-item"><%- name %></div> 
</script>

<script id="supplierTemplateFull" type="text/template">
<div class="supplier-item-full">

<% if (typeof(name) == "undefined" || name == '') { %>
<div class="supplier-title">
	<input type="text" class="supplier-input" name="name" value="" style="width:270px;padding:2px" placeholder="Введіть назву постачальника"/>
</div>
<% } else {%>
	<div class="supplier-title edit-block">
		<span class="edit-block-title" style="margin-right:10px"><%- name %></span>
		<input class="edit-block-input" type="text" name="name" value="<%- name %>"/>
		<img src="/img/edit2.png" id="edit-pic" height="20px"/>
	   <img src="/img/delete.png" id="supplier-delete" height="20px"/>
	</div>
		<div style="overflow:hidden">
	
<% if (typeof(productCategory) == "undefined" ||  productCategory == null) { %>
<div class="supplier-category" style="float:left">
	<select class="edit-block-select" id="supplier-category" name="productCategory" ></select>
</div>
<button id='add-product' class='uibutton-addp' style="float:right" >Додати продукт</button>
	<% } else {%>
			<table style="width:100%">
				<tr><td style="height:80px;width:205px;">
						<div class="supplier-category edit-block">
							<span class="edit-block-title supplier-category-title" ></span>
							<select class="edit-block-select" id="supplier-category" name="productCategory"></select>
							<img src="/img/edit2.png" id="edit-pic" height="20px"/>
						</div>
					</td>
					<td style="text-align: center">
						<div id="supplier-comment">
							<div style="color:#1f1f1f;font-size:19px"><%- comments %> </div>
							<textarea maxlength="255" name="comments" style="width:600px;height: 60px;display:none;font-size:19px"><%- comments %></textarea>
						</div>
					</td>
					<td style="text-align: right; width:200px">
						<button id='add-product' class='uibutton-addp' >Додати продукт</button>
					</td>
				</tr>
			</table>
<% } %>
</div>
<% } %>
<div id="supplier-products-container">

<table class="supplier-product-table" style="margin-top:20px;">
<thead>
				<tr>
					<th class="supplier-product-name" style="border-top-left-radius: 4px;text-align:center">Продукт</th>
					<th class="supplier-product-price">Цiна</th>
					<th  class="supplier-product-margin">Нацiнка(%)</th>
					<th  class="supplier-product-margin-price" >Цiна в магазинi</th>
					<th class="supplier-product-control" style="border-top-right-radius: 4px;"></th>
				</tr>
</thead>

<tbody></tbody>	
			</table>


</div>
</div> 
</script>


<script id="productFullTemplate" type="text/template">
<div id="dialog-product">

<table class="product-item">
<tr><td>Продукт</b></td><td><input type="text" style="width:300px" class='product-input' name="name" value="<%- name %>"/></td></tr>
<tr><td>Ціна</td><td><input type="text" class='product-input changeable' name="price" value="<%= Shop.util.getPriceView(price) %>"/></td></tr>
<tr><td>Націнка</td><td><input type="text" class='product-input changeable' name="margin" value="<%=margin %>"/></td></tr>
<tr><td>Ціна в магазині</td><td><input type="text" class='product-input changeable' name="marginPrice" value="<%= Shop.util.getPriceView(marginPrice) %>"/></td></tr>
</table>
<fieldset style="padding:5px">
 <legend><b>Акциз:</b></legend>
<table>
<tr><td>Ціна з акцизом</td><td><input type="text" class='product-input' name="excisePrice" value="<%= Shop.util.getPriceView(excisePrice) %>"/></td></tr>
<tr><td>Код</td><td><input type="text" class='product-input' name="code" value="<%=code %>"/></td></tr>
<tr><td>Об’єм(л/шт)</td><td><input type="text" class='product-input' name="volume" value="<%=volume %>"/></td></tr>

</table>
</fieldset>
<a href="#" id="product-move">Перенести продукт</a>

</div>

</script>


<script id="productMove" type="text/template">
<div id="dialog-product-move">
<h3>Перенести продукт</h3>
	<form id="move-product-form">
		<input type="hidden" name="product_id" value="<%=id%>"/>
        <table style="border-spacing: 10px; margin: 10px 0">

            <tr>
                <td>
                    <label for="product-move">Перенести</label>
                    <input type="radio" id="product-move" name="product_move" value="move" checked="checked"/>
                </td>

                <td>
                    <label for="product-copy">Копіювати</label>
                    <input type="radio" id="product-copy" name="product_move" value="copy"/>
                </td>
            </tr>
            <tr><td colspan="2">
                <select name="new_supplier">
                    <option value="0">Виберіть постачальника</option>
                    <% _.each(suppliers, function(supplier) { %>
                    <option value="<%=supplier.id%>"><%=supplier.name%></option>
                    <% }); %>
                </select>
            </td></tr>

        </table>
	</form>

    <button type="button" id="move-product-button">Перенести</button>
</div>
</script>

<script id="productTemplate" type="text/template">

<td class="supplier-product-name"><%- name %></td>
<td class="supplier-product-price"><span><%= Shop.util.getPriceView(price) %></span></td>
<td class="supplier-product-margin"><input type="text" class='changeable hidden' name="margin" value="<%=margin%>"/><span><%= margin %></span></td>
<td class="supplier-product-margin-price"><span><b><%= Shop.util.getPriceView(marginPrice) %></b></span></td>
<td class="supplier-product-control">
<a href="#" class="edit-product"><img src="/img/edit2.png" alt="Редагувати" width=25px/></a>
<a href="#" class="remove-product"><img src="/img/delete.png" alt="Видалити" width=25px/></a></td>

</script>


<div id='supplier-wrapper' style="">

	<div class="horizontal-layout" style="height:80%;">
		<div id="suppliers-wrapper" style="position:relative;height:100%">
			
			<div id="suppliers-controls">
				<button id="add-supplier" type="button" class="uibutton">Додати</button>
				
				<a id="find-old-products" href="#" type="button"><img src="/img/warn_prod.png"/></a>
				
				<div id="magic-search"><input type="text" style=" width: 100%;font-size:16px; padding: 3px 5px;" name="searchKey" class="search-key" placeholder="Пошук..."/></div>
			</div>
			<ul id="suppliers" style="overflow: auto;position:absolute;top:100px;bottom:5px;width:300px">
		
			</ul>
		</div>
		
		<div id="supplier-products-wrapper" >
			
			
			<div id="old-products">
				<span class="old-products-title">Знайти продукти які вже не використовуються</span>
				<input type="text" class="not-used-days" value="30"/> 
				<span class="old-products-title"> і більше днів</span> 
				<button type="button" class="uibutton" id="find-unused-products" style="font-size: 15px;">Знайти</button>
			</div>
			
			
			<div id="supplier-products"></div>
		

		</div>
		
	</div>

</div>

<script>
//facepalm
$('#suppliers-wrapper').css('height',($( window ).height() - 132) + "px")

</script>