<div id="header-wrapper">

<nav class="main-menu">
<ul class="horizontal">
<li class="logo">
<a href="/invoice"><img src="/img/logo.png" width="30px" alt="logo"/></a>
</li>

<#list menuList as menuItem>
	<#assign cssClass = "">
	<#if url == menuItem.url>
	  <#assign cssClass = "selected">
	<#elseif menuItem.subMenu??>
		<#list menuItem.subMenu as subMenuItem>
			<#if url == subMenuItem.url>
				<#assign cssClass = "selected">
			</#if>
		</#list>
	</#if>
	
	<#if cssClass == 'selected' && menuItem.subMenu??>
		<#assign subMenu = menuItem.subMenu>
	</#if>
	
	 <li class="menu-el ${cssClass}">
			<a href="${menuItem.url}">${menuItem.name}</a>
	 </li>
</#list>
</ul>
</nav>

<div id="user-form">
<#if username??> 
	Привіт, <b>${username}</b>, <a href="/logout">Вийти</a>
<#else>
	<a href="/login">Ввійти</a>
	</#if>
</div>

</div>

<#if subMenu??>
<div id='submenu-controls'>
	<ul class="horizontal">
			<#list subMenu as menuItem>
			
				<#assign cssClass = "">
				<#if url == menuItem.url>
				  <#assign cssClass = "selected">
				</#if>
				<li class="${cssClass}"><a href="${menuItem.url}"><span>${menuItem.name}</span></a></li>
			</#list>
	</ul>
</div>

</#if>
