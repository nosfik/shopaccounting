<div id='statistic-wrapper'>
	<div id="statistic-block-wrapper">
		<div id="sallary-module">
			
			<h2>Розрахунок зарплати</h2>
			
			<table style="width:100%;padding: 20px 0;">
				<tr>
				
				<td  style="width:50%;vertical-align:top;padding-left:20px">
					
					<form id="sallary-form">
						<div id="date-part">
							<span class="title">Виберіть місяць</span>
							<select id="months">
							</select>
							<select id="years">
							</select>
							
							<button type="button" id="get-sallary" style="float:right" class="uibutton">Розрахувати</button>
						</div>
						
						<div id="percent-part">
							<span class="title">Відсоток</span> <input type="text" id="input-percent"/><span class="title">%</span>
						</div>
						
						
						
					</form>
					
					<div id="salary-result">
					
						<div id="shift-warn" style="background-color:#D4B307;display:none;margin-bottom:10px">
							<h2>Увага! Кількість змін в місяці не відповідає кількості змін, які були внесені до програми</h2>
						</div>
						<table id="salary-general-info">
							<tr><td style="padding-right:80px">Всього виручки</td><td><span id="summ-proceed"></span></td></tr>
							<tr><td>Зарплата</td><td><span id="summ-salary"></span></td></tr>
							<tr><td>Змін в місяці</td><td><span id="summ-shiftsInMonth"></span></td></tr>
							<tr><td>Всього змін</td><td><span id="summ-totalShifts"></span></td></tr>
							<tr><td>Зарплата за зміну</td><td><span id="summ-shiftSalary"></span></td></tr>
						</table>
						<h2 style="margin:15px 0">До видачі</h2>
						<table id="seller-salary-table">
							
						</table>
					
					</div>
				
				</td>
				<td style="width:50%;vertical-align: top;">
					<div id="container-proceed" style="height: 550px; margin:0 15px;"></div>
				</td>				
				
				</tr>
			</table>
		</div>
	</div>
</div>

<script src="/highchart/highcharts.js"></script>