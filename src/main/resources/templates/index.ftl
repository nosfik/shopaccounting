<!DOCTYPE html>

<html lang="en">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<script src="/js/jquery.js"></script>
  <script src="/js/jquery-ui.js"></script>
 <script src="/js/underscore.js"></script>
 <script src="/js/backbone.js"></script>
 <script src="/js/main.js?t=${timeStamp}"></script>
 
 <link rel="stylesheet" href="/css/base.css?t=${timeStamp}"/>
 <link rel="stylesheet" href="/css/${theme}/jquery-ui.css"/>
 <link rel="stylesheet" href="/css/${theme}/jquery-ui.structure.css"/>
 <link rel="stylesheet" href="/css/${theme}/jquery-ui.theme.css"/>
 <#include 'head_js.ftl'>
</head>

 <body>
 	<div id="dialog-popup"></div>
	<div id="wrapper">
		<header>
			<#include 'header.ftl'>
		</header>
	
	<#if template??>
 		 <#include template + '.ftl'>
	</#if>
	
	</div>
	
	<#if template?? && defaultJs == true>
	 <script src="/js/${template}/model.js?t=${timeStamp}"></script>
	 <script src="/js/${template}/view.js?t=${timeStamp}"></script>
	 </#if>
	 
	 
	
</body>

</html>