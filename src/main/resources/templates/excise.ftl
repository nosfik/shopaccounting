<script id="exciseTypeTemplate" type="text/template">
	<input type="text" style="width:120px" class="excise_type" name="name" value="<%- name %>"/> <a class="delete-type" href="#"> Видалити</a>
</script>


<script id="categoriesTemplate" type="text/template">
<div id="category-tabs">
<ul>
<% _.each(groups, function(group, i) { %>  
	<li data-id="<%=group.id%>"><a href="#tabs-<%=group.id%>"><%-group.name%></a><span class="ui-icon ui-icon-close" role="presentation">Видалити</span></li>
<% }); %>
</ul>

<% _.each(groups, function(group, i) { %>  
	
	<div style="overflow:hidden;padding:10px;background-color:#F6F6F6" id="tabs-<%=group.id%>">
		<input type="hidden" class="group_id" name="group_id" value="<%=group.id%>"/>
		<input type="hidden" class="group_name" name="group_name" value="<%-group.name%>"/>
		<table style="width:100%;"><tr><td>
		<table style="width:100%;" id="excise-product-table">
<tr>


<% if(typeof(groupId) !== "undefined" && groupId == group.id) { %>

<th class="sortable <%if(field == 'name') {%> <%=asc%>  <%}%>" data-field="name">Продукт</th>
<th class="sortable <%if(field == 'excisePrice') {%> <%=asc%>  <%}%>" data-field="excisePrice">Ціна<br/>(з акцизом)</th>
<th class="sortable <%if(field == 'price') {%> <%=asc%>  <%}%>" data-field="price">Ціна<br/>(без акцизу)</th>
<th class="sortable <%if(field == 'marginPrice') {%> <%=asc%>  <%}%>" data-field="marginPrice">Ціна</th>
<th class="sortable <%if(field == 'volume') {%> <%=asc%>  <%}%>" data-field="volume">Об'єм<br/>(літр/шт)</th>
<th class="codeSort number sortable <%if(field == 'code') {%> <%=asc%>  <%}%>" data-field="code">Код</th>
<th class="excise sortable <%if(field == 'exciseType') {%> <%=asc%>  <%}%>" data-field="exciseType">Тип</th>

<% } else { %>

<th class="sortable" data-field="name">Продукт</th>
<th class="sortable" data-field="excisePrice">Ціна<br/>(з акцизом)</th>
<th class="sortable" data-field="price">Ціна<br/>(без акцизу)</th>
<th class="sortable" data-field="marginPrice">Ціна</th>
<th class="sortable" data-field="volume">Об'єм<br/>(літр/шт)</th>
<th class="codeSort number sortable" data-field="code">Код</th>
<th class="sortable excise" data-field="exciseType">Тип</th>
<% }
%>

</tr>
		<% _.each(group.products, function(product, i) { %>  
		<% if (typeof(regexp) !== "undefined" && (regexp.test(product.name) ||  regexp.test(product.code))) { %>
			<tr class="excise-match-row">
<% } else { %>
			<tr>
<% } %>
			<input type="hidden" name="product_id" value="<%=product.id%>"/>
			<td class="excise-product-name"><%-product.name%></td>
			<td class="excise-product-price"><%=product.excisePrice%></td>
			
			<td class="excise-product-price"><%=product.price%></td>
			<td class="excise-product-marginprice"><%=product.marginPrice%></td>
			<td><input style="width:80px" type="text" class="excise-product-volume" name="volume" data-val="<%=product.volume%>" value="<%=product.volume%>"/></td>
			<td><input style="width:80px" type="text" class="excise-product-code" name="code" autocomplete="off" data-val="<%=product.code%>" value="<%=product.code%>"/></td>
			
			<td>

			<select name="excise-product-type" class="excise-product-type" >
				<% if(product.exciseType  && product.exciseType.id) { %>
					<option value="<%=product.exciseType.id%>" ><%-product.exciseType.name%></option>
				<% } %>
					<option value=""></option>
				<% _.each(exciseTypes, function(type, i) { %>  
					<option value="<%=type.id%>" ><%-type.name%></option>
				<% }); %>
			</select>
			</td>



		</tr>
		<% }); %>
		</table>
	</td>
<td style="vertical-align:top">
<div id="excise-control" style="width:65px; padding:0 5px 0 10px">
	<button class="pointer" onclick="$('#dialog-categ').html('');dialog.dialog('open');categoryDialogView.render($('#tabs-<%=group.id%>'))"><img src="/img/edit.png" width="50px" alt="Редагувати"/></button>
	<br/><br/>
	<button class="pointer" onclick="printo()"><img src="/img/print.png" width="50px" alt="Друк"/></button>
</div>
</td>
</tr>
</table>

	</div>

<% }); %>


</div>


</script>

<script id="categoryTemplate" type="text/template">
  <form id="category-form">
<div style="padding:15px 0;font-size:20px">
      <label for="tab_title">Назва категорії</label>
	  <input type="hidden" name="cat-id" id="cat-id" value="<%=id%>"/>
      <input type="text" name="cat-name" id="cat-name" class="ui-widget-content ui-corner-all" value="<%-name%>"/>
</div>
<fieldset>
<div style="padding:5px">
<div class="col-1"></div>
<% var ig = 0; %>
<% _.each(suppliers, function(supplier, i) { %>  
	
	<div class="excise-product-column">
	
	<h3 class='supplier-header'><%-supplier.name%></h3>
<ul>
	<% _.each(supplier.products, function(product, t) { %>  
		
<% if(products[product.id]) { %>
 		<li><input type="checkbox" name="product" id="product-<%=product.id%>" value="<%=product.id%>" checked="checked"/> <label for="product-<%=product.id%>"><%-product.name%></label></li>
 <% } else { %>
<li><input type="checkbox" name="product" id="product-<%=product.id%>" value="<%=product.id%>"/> <label for="product-<%=product.id%>"><%-product.name%></label></li>
<% } %>
	 	<% }); %>
</ul>
</div>
<% }); %>
</div>
</fieldset>
  </form>
</script>
<script>
function printo() {
	
	var printEl = $('#print-area'), table1 = printEl.find('tr:first .print-table-1'), table2 = printEl.find('tr:first .print-table-2');
	printEl.find('h2').html($($('#category-tabs').tabs().data('uiTabs').active).find('a').html());
	
	
	table1Tmp = table1.clone();
	table2Tmp = table2.clone();
	
	table1.find( 'tr:not(:first)' ).remove();
	table2.find( 'tr:not(:first)' ).remove();
	printEl.find( 'tr.toRemove' ).remove();
	table1Tmp = table1.clone();
	table2Tmp = table2.clone();
	
	$('.codeSort').click();
	
	
	var isSmallerA4Size = function(px) {   
		   return 25 > Math.floor(px/$('#my_cm').height()); 
		 };
	
	
	
	var table1Full = false, table1Rows = 0, table2Rows = 0;
	$('#excise-product-table:visible tr').each(function(i, el){
		
		
		var name = $(el).find('.excise-product-name').html();
		var code = $(el).find('.excise-product-code').val();
		var price = $(el).find('.excise-product-marginprice').html();
		
		
		
		if(name) {
			
			
			if(!table1Full) {
				table1.append('<tr><td>' + name + '</td><td>' + price + '</td><td><b>' +code+'</b></td></tr>');
				table1Full = !isSmallerA4Size(table1.height());
				table1Rows++;
			} else {
				if(table2Rows < table1Rows) {
					table2.append('<tr><td>' + name + '</td><td>' + price + '</td><td><b>' +code+'</b></td></tr>')
					table2Rows++
				} else {
					
					var body = printEl.find('#print-body'), row = $('<tr>').attr('class', 'toRemove');
					
					table1 = table1Tmp.clone();
					table2 = table2Tmp.clone();
					row.append($('<td>').append(table1));
					row.append($('<td>').append('&nbsp;').css('width','10px'));
					
					
					row.append($('<td>').append(table2));
					
					body.append($('<tr class="toRemove" ><td colspan=2 style="height:80px"></td></tr>'))
					body.append(row)
					table1.append('<tr><td>' + name + '</td><td>' + price + '</td><td><b>' +code+'</b></td></tr>');
					table1Rows =1;
					table2Rows = 0;
					table1Full = false;
					
				}
			}
			
			
		}
		
		
		
	
		
		
		
	})
	
	
	 var popup = window.open('', 'print', 'height=768,width=1024');
	popup.document.write('<html><head><title>Друк</title>');
	popup.document.write('<link rel="stylesheet" href="/css/base.css" type="text/css" />');
	   
	popup.document.write('</head><body >');
	popup.document.write(document.getElementById("print-area").innerHTML);
	popup.document.write('</body></html>');

	popup.document.close(); // necessary for IE >= 10
	popup.focus(); // necessary for IE >= 10

	setTimeout(function(){
		popup.print();
		popup.close();
		
	}, 500);
	
        return true;
	
	

	
}


</script>

<div id="my_cm" style="height:1cm;display:none"></div>
<div id="print-area">
	<h2 class="print-title"></h2>
	<table id="print-body">
	<tr>
	
	<td>
	
	<table class="print-table-1 print-table">
		<tr class="print-table-header">
			<th >Продукт</th>
			<th>Ціна</th>
			<th>Код</th>
		</tr>
	</table>
	
	</td>
	<td style="width:10px">&nbsp;</td>
	
	<td>
		
		<table class="print-table-2 print-table">
		<tr class="print-table-header">
			<th >Продукт</th>
			<th>Ціна</th>
			<th>Код</th>
		</tr>
	</table>
	
	</td>
	
	
	
	</tr>
	
	</table>
	
	
</div>

<div id='excise-wrapper'>

<div id="excise-control" style="padding:5px">
	<div id="excise-search" >
	Пошук
	<input type="text" name="search" autocomplete="off"/>
	</div>
	
	<div>
	<button class="uibutton" style="float:right" id="add-excise">Додати акцизну категорію</button>
	
	<button class="uibutton" style="float:right" id="excise-types">Акцизні типи</button>
	
	</div>
	
</div>

<div id="excise-category-view" style="width:100%"></div>

<div id="excise-type-view" class="hidden">
<div id="excise-type-view-body">

</div>
<div id="excise-type-add"><button><img src="/img/add.png" width="50px" alt="Додати"/></button></div>
</div>




<div id="dialog-categ" style="display:none" title="Нова категорія">
  
</div>


</div>