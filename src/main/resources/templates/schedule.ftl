<script id="sellerTemplate" type="text/template">
	<td><input type="text" name="name" value="<%- name %>"</td><td><a class="delete-seller" href="#"> Видалити</td>
</script>

<script id="dayBlock" type="text/template">
	<div id="block-day" >
			<form>
				<fieldset>
				<legend><%=day%></legend>
				<table>
				<tr class="first-shift"><td>I&nbsp;зм.</td><td>
					<select class="seller-select" id="firstSeller">

					<% if(typeof(firstSeller) !== "undefined" && firstSeller != null && firstSeller.id) { %>
						<option value="<%=firstSeller.id%>"><%=firstSeller.name%></option>
					<% } %>

					<option></option>
					<% _.each(sellers, function(s) { %>  
						<option value="<%=s.id%>"><%-s.name%></options>
					<% }); %>
					</select>
				</td></tr>
				<tr class="first-shift"><td></td><td>
					<select class="seller-select" id="secondSeller">
					<% if(typeof(secondSeller) !== "undefined" && secondSeller != null && secondSeller.id) { %>
						<option value="<%=secondSeller.id%>"><%=secondSeller.name%></option>
					<% } %>
					<option></option>
					<% _.each(sellers, function(s) { %>  
						<option value="<%=s.id%>"><%-s.name%></options>
					<% }); %>
					</select>
				</td></tr>

					<tr class="seller-break"><td>&nbsp;</td></tr>
					<tr class="second-shift"><td>II&nbsp;зм.</td><td>
					<select class="seller-select" id="thirdSeller">
					<% if(typeof(thirdSeller) !== "undefined" && thirdSeller != null && thirdSeller.id) { %>
						<option value="<%=thirdSeller.id%>"><%=thirdSeller.name%></option>
					<% } %>
					<option></option>
					<% _.each(sellers, function(s) { %>  
						<option value="<%=s.id%>"><%-s.name%></options>
					<% }); %>
					</select>
				</td></tr>
				<tr class="second-shift"><td></td><td>
					<select class="seller-select" id="fourthSeller">
					<% if(typeof(fourthSeller) !== "undefined" && fourthSeller != null && fourthSeller.id) { %>
						<option value="<%=fourthSeller.id%>"><%=fourthSeller.name%></option>
					<% } %>
					<option></option>
					<% _.each(sellers, function(s) { %>  
						<option value="<%=s.id%>"><%-s.name%></options>
					<% }); %>
					</select>
				</td></tr>
				</table>
				</fieldset>
			</form>
		</div>

</script>

<div id='schedule-wrapper'>

<div id="datepicker" style="z-index:1"></div>
	<div id="date-block">
		<input type="hidden" id="schedule-year" />
		<input type="hidden" id="schedule-month" />
		<table style="margin:0 auto"><tr>
			<td><a href="#" class="month-control left"/><img style="transform: rotate(180deg);" src="/img/arrow1.png" width="45px" alt="left"/></a></td>
			<td><span></span></td>
			<td><a href="#" class="month-control right"/> <img src="/img/arrow1.png" width="45px" alt="right"/></a></td>
		</tr></table>
	</div>

	<div class="schedule-layout" >
	<table style="width:100%">
		<tr>
		
			<td style="width:80%">
				<div class="calendar-layout">
				<table id="schedule-seller-table">
					<thead>
						<tr>
							<th>Понеділок</th>
							<th>Вівторок</th>
							<th>Середа</th>
							<th>Четвер</th>
							<th>П'ятниця</th>
							<th>Субота</th>
							<th>Неділя</th>
						</tr>
					</thead>
					<tbody>
					
					</tbody>
				</table>
				</div>
			</td>
			<td valign="top">
				<div class="sellers-layout">
					<h2 class="seller-title">Продавці</h2>
					
					<div id="seller-controls">
						<button type="button" id="add-new-seller" class="uibutton">Додати</button>
					</div>
					
					<table id="seller-list">
						
					</table>
				
				</div>
			
			</td>
		
		</tr>
	
	</table>
		
	</div>

</div>
<style>
.ui-datepicker-calendar {
    display: none;
    }
</style>
<script>
function setCurrentDate() {
	
	var month = +$('#schedule-month').val();
	var year = +$('#schedule-year').val();

	$('#date-block span').html(monthNamesInf[month-1] + ' ' + year)
}

	 var date = new Date();
	 $('#schedule-year').val(date.getFullYear());
	 $('#schedule-month').val(date.getMonth() + 1);
	 setCurrentDate();
	 $('.month-control').click(function(e){
		 
		 var month = +$('#schedule-month').val();
		 var year = +$('#schedule-year').val();
		 
		 if($(this).hasClass("left")) {
			if(month == 1) {
				$('#schedule-month').val(12);
				$('#schedule-year').val(--year);
			} else {
				$('#schedule-month').val(--month);
			}
		 } else {
			 if(month == 12) {
				 $('#schedule-month').val(1);
				 $('#schedule-year').val(++year);
			 } else {
				 $('#schedule-month').val(++month);
			 }
		 }
		 
		 setCurrentDate()
		 listView.renderSchedule();
		 
	 });
</script>