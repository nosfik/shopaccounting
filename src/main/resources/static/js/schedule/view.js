var SellerListView = Backbone.View.extend({
	initialize: function () {
		var self = this;
		this.collection.fetch({ success: function () { 
				self.scheduleListView = new ScheduleSellerListView({ sellers: self.collection, collection: new ScheduleSellerList(), el: $("#schedule-seller-table tbody") });
				self.render();
				self.listenTo(self.collection, 'add', self.render);
			} 
		});
		
		this.controls = new Controls({el:$('#seller-controls'), parent:this});
		
	},
	renderSchedule : function(){
		this.scheduleListView.collection.fetch({reset:true});
	},
	render: function() {
		console.log('render');
		this.$el.html('');
		this.collection.each(function(seller) {
			var sellerView = new SellerView({model: seller, parent:this});
			this.$el.append(sellerView.render().el);
		}, this);
		return this;
	}
});

var Controls =  Backbone.View.extend({
	
	initialize: function (options) {
		this.parent = options.parent;
	},
	
	events: {
	    "click #add-new-seller" :"add",
	},
	add: function() {
		this.parent.collection.add(new Seller({name:""}));
	},
	render: function() {
		this.el;
	}

});

var SellerView = Backbone.View.extend({
	initialize: function (options) {
		this.parent = options.parent;
	},
	tagName: 'tr',
	template: _.template($('#sellerTemplate').html() ),
	events: {
	    "click .delete-seller" : "remove",
	    "change input": "fieldChanged",
	},
	
	remove : function(e) {
		var parent = this.parent;
		this.model.destroy({success: function() {
			parent.render();
			parent.renderSchedule();
		}})
	},
	
	fieldChanged: function(e){
		var field = $(e.currentTarget);
		var data = {};
		data[field.attr('name')] = field.val();
	    this.model.set(data);
	    var parent = this.parent;
	    this.model.save(null,{ 
	    	error:function(model) {
	    		Shop.Popup.error("Продавець '"+model.get('name')+"' вже є в базі. Якщо такого імені немає серед активних продавців, спробуйте створити нового продавця з таким іменем");
	    	},
	    	success:function() {
	    		parent.scheduleListView.collection.fetch({reset:true})
	    	}
	    });
    },
		
	render : function(){
		this.$el.html($(this.template(this.model.toJSON())));
		return this;
	}
});


var ScheduleSellerListView = Backbone.View.extend({
	initialize: function (options) {
		var self = this;
		this.sellers = options.sellers;
		this.scheduleMap = {};
		
		this.collection.on("reset", function(collection) {
			self.scheduleMap = {};
			collection.each(function(schedule){
				self.scheduleMap[schedule.get('date')] = schedule;
			});
			self.render();
		    }, this);
		this.collection.fetch({reset:true});
	},
	getDate : function(year, month, day){
		var result = year + "-";
		result += (month < 10) ? "0" + month + "-" : month + "-"
		result += (day < 10) ? "0" + day : day;
		return result;
	},
	render: function() {
		 this.$el.html('');
		 var year = +$('#schedule-year').val(), month=+$('#schedule-month').val() ;
		 var date = new Date(year + "-" + month +"-" + 1);
		 var dayInMonth = new Date(year, month, 0).getDate();
		 var day = (date.getDay() == 0) ? 7 : date.getDay();

		 var row = $('<tr/>');
		 for(var i = 1; i <  day; i++) {
			 row.append("<td></td>")
		 }
		 for(var i = 0; i < dayInMonth; i++,day++) {
			 if(day == 8) {
				 day = 1;
				 this.$el.append(row);
				 row = $('<tr/>');
			 }
			 model = this.scheduleMap[this.getDate(year,month,i+1)];
			 if(!model) {
				 model = new ScheduleSeller({date : this.getDate(year,month,i+1)});
			 }
			 row.append(new ScheduleSellerView({model:model, sellers:this.sellers, day :i+1}).render().el)
		 }
		 this.$el.append(row);
		 
		return this;
	}
});


var ScheduleSellerView = Backbone.View.extend({
	initialize: function (options) {
		this.model = options.model;
		this.sellers = options.sellers;
		this.day = options.day;
		
	},
	events : {
		"change .seller-select" : "selectChange" 
		
	},
	
	selectChange:function(e) {
		var data = {}
		data[e.currentTarget.id] = {id:$(e.currentTarget).val()};
		this.model.set(data);
		console.log(this.model);
		this.model.save();
		
	},
	
	template: _.template($('#dayBlock').html() ),
	tagName: 'td',
	render: function() {
		var params = this.model.toJSON();
		params.sellers = this.sellers.toJSON();
		params.day = this.day;
		this.$el.html($(this.template(params)));
		return this;
	}
});


var listView = new SellerListView({ collection: new SellerList(), el: $("#seller-list") });