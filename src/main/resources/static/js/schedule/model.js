var Seller = Backbone.Model.extend({
	urlRoot : '/rest/schedule/seller',
});

var SellerList = Backbone.Collection.extend({
	model : Seller,
	url : '/rest/schedule/seller',
});


var ScheduleSeller = Backbone.Model.extend({
	urlRoot : '/rest/schedule',
});

var ScheduleSellerList = Backbone.Collection.extend({
	model : ScheduleSeller,
	url : function(){
		return '/rest/schedule/' + $('#schedule-year').val() + '/' + $('#schedule-month').val();
	},
});



