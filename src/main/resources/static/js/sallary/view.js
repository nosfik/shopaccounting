Shop.Sallary = {
		config : {
			yearsInSelect : 2 
		},
		
		init : function(){
			this.initSelects();
			this.initGraph();
			this.initBinding()
		},
		
		initBinding : function() {
			var self = this;
			$('#sallary-form select').on('change', function(){
				self.initGraph();
				$('#salary-result').hide();
			});
			
			$('#get-sallary').click(function(){
				
				if($('#input-percent').val() == '') {
					Shop.Popup.warn("Увага", "Поле з процентом - не заповнене");
					return;
				}
					
				$.ajax({
					url :'/rest/statistic/salary/' + $('#years').val() + '/' + $('#months').val(),
					data : {'percent' : $('#input-percent').val()},
					success:function(response){
						
						
						
						$('#summ-proceed').html(response.proceed);
						$('#summ-salary').html(response.salary);
						$('#summ-shiftsInMonth').html(response.shiftsInMonth);
						$('#summ-totalShifts').html(response.totalShifts);
						$('#summ-shiftSalary').html(response.shiftSalary);
						
						if(response.shiftsInMonth != response.totalShifts) {
							$('#shift-warn').show();
						} else {
							$('#shift-warn').hide();
						}
						
						$("#seller-salary-table").empty();
						_.each(response.sellerSalary, function(value, key){
							$("#seller-salary-table").append("<tr><td>"+key+"</td><td>"+value+"</td></tr>");
						});
						
						
						$('#salary-result').show();
					}
				});
			});
		},
		
		initSelects : function() {
			var date = new Date();
			for(var i = date.getFullYear() - this.config.yearsInSelect; i <= date.getFullYear(); i++) {
				if(i == date.getFullYear()) {
					$('#years').append("<option value='"+i+"' selected>"+i+"</option>")
				} else {
					$('#years').append("<option value='"+i+"'>"+i+"</option>")
				}
			}
			
			for(var i = 0; i < monthNamesInf.length; i++) {
				if(i == date.getMonth()) {
					$('#months').append("<option value='"+(i+1)+"' selected>"+monthNamesInf[i]+"</option>")
				} else {
					$('#months').append("<option value='"+(i+1)+"'>"+monthNamesInf[i]+"</option>")
				}
			}
			
			if ($('#months').val() == 0 ){
				$('#years').val(+$('#years').val() - 1);
				$('#months').val(11)
			} else {
				$('#months').val(+$('#months').val() - 1)
			}
			
		},
		
		initGraph : function() {
			var self = this;
			$.ajax({
				url :'/rest/statistic/salary/shift/' + $('#years').val() + '/' + $('#months').val(),
				success:function(response){
					self.buildGraph(response.data1, response.data2);
				}
			});

		},
		
		
		buildGraph : function(data1, data2){
			 $('#container-proceed').highcharts({
			        chart: {
			            type: 'column'
			        },
			        title: {
			            text: 'Кількість змін продавців'
			        },
			        tooltip: {
			            pointFormat: '<b>{point.y:,.0f}</b>',
			          	style : { fontSize: "18px" }
			        },
			        xAxis: {
			        	categories: data1,
			        	labels : {
			        		style : { fontSize: "18px" }
			        	}
			        },
			        yAxis: {
			            title: {
			                text: 'Зміни'
			            },
			            labels: {
			           		 style : { fontSize: "18px" },
			                formatter: function () {
			                    return this.value;
			                }
			            }
			        },
			      
			        series:data2
			    });
		}
};

Shop.Sallary.init();


