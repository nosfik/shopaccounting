var Supplier = Backbone.Model.extend({
	urlRoot : '/rest/suppliers'
});

var SupplierList = Backbone.Collection.extend({
	model : Supplier,
	url : '/rest/suppliers'

});

var ExciseType = Backbone.Model.extend({
	urlRoot : '/rest/product/excise/type'
});

var ExciseTypeList = Backbone.Collection.extend({
	url : '/rest/product/excise/type',
	model : ExciseType
});

var ExciseGroup = Backbone.Model.extend({
	urlRoot : '/rest/product/excise'
});

var ExciseGroupList = Backbone.Collection.extend({
	url : '/rest/product/excise',
	model : ExciseGroup,
	sort_key : 'name',
	comparator: function(item) {
		return !item.get(this.sort_key);
	},
	
	sortByField: function(fieldName) {
		this.sort_key = fieldName;
		this.sort();
	},

	search : function(key) {
		if (key == "")
			return this;
		var pattern = new RegExp(key, "gi");
		return new ExciseGroupList(this.filter(function(data) {
			var found = false;
			data.get('products').forEach(
					function(product) {
						pattern.lastIndex = 0;
						if (pattern.test(product["name"])
								|| pattern.test(product["code"])) {
							found = true;
						}
					});
			return found;
		}))
	}

});