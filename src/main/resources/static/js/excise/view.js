var dialog = $( "#dialog-categ" ).dialog({
      autoOpen: false,
      modal: true,
      width:'80%',
      position: { my: "center top", at: "center top"},
      open: function( event, ui ) {
    	  
      },
      buttons: {
        'Додати': function() {
        	$('#category-form').submit();
        	 $( this ).dialog( "close" );
        },
        'Закрити': function() {
          $( this ).dialog( "close" );
        }
      },
      close: function() {
        
      }
    });


$('#add-excise').click(function() {
	$("#dialog-categ").html('');
	dialog.dialog( "open" );
	categoryDialogView.render();
});



$('#excise-types').click(function() {
	$( "#excise-type-view" ).dialog({
		title: "Акцизні типи",
	    height: 'auto',
	    modal: true
	});
});





var ExciseTypesView = Backbone.View.extend({
	el : $('#excise-type-view'),
	elBody : $('#excise-type-view-body'),
	initialize : function() {
		this.collection = new ExciseTypeList();
		this.listenTo( this.collection, 'sync', this.render );
		this.collection.fetch({success:function(){
			this.listenTo( this.collection, 'add', this.render );
		}.bind(this)});
	},
	
	events : {
		"click #excise-type-add" : "addType"
	},
	
	
	addType : function(e) {
		this.collection.add(new ExciseType());
	},
	
	render : function() {
		var $ul = $('<ul/>');
		this.collection.each(function(item) {
			if(!item.deleted) {
				$ul.append(new ExciseTypeView({model:item, collection:this.collection, parent:this}).render())
			}
			
		}.bind(this));
		this.elBody.html($ul);
		
	}
});

var ExciseTypeView = Backbone.View.extend({
	tagName: 'li',
    template : _.template( $( '#exciseTypeTemplate' ).html()),
	initialize : function(options) {
		this.model = options.model;
		this.collection = options.collection;
		this.parent = options.parent;
	},
	
	events : {
		"click .delete-type" : "deleteT",
		"change .excise_type" : "update"
	},
	
	deleteT : function(e) {
		var self = this;
		this.model.destroy({success: function(model, response) {
			self.parent.render();
			}});
	},
	
	update : function(e) {
		console.log($(e.currentTarget).val())
		this.model.save({name: $(e.currentTarget).val()});
	},
	
	render : function() {
		 this.$el.html( this.template( this.model.attributes ) );
	     return this.$el;
	}
	
});


var CategoryExciseView = Backbone.View.extend({
	template : _.template($('#categoriesTemplate').html() ),
	initialize: function (options) {
		var self = this;
		this.exciseTypes = new ExciseTypeList();
		this.collection.fetch({success:function(){
			self.exciseTypes.fetch({success:function(){
				self.render();
			}});
		}});
	},
	
	events : {
		"change .excise-product-code" : "updateProductCode",
		"change .excise-product-volume" : "updateProductVolume",
		"change .excise-product-type" : "updateProductExciseType"
	},
	
	updateProductExciseType : function(e) {
		console.log(2)
		var form = $(e.currentTarget).closest('tr');
		
		$.ajax({
			url : '/rest/suppliers/product/' + form.find('input[name="product_id"]').val() + '/excise/' + form.find('select[name="excise-product-type"]').val(),
			method : "PATCH",
			success : function(resp){
				$(e.currentTarget).removeClass('red-border2');
				$(e.currentTarget).addClass('green-border2');
				form.find('select[name="excise-product-type"]').data('val' , form.find('select[name="excise-product-type"]').val())
			},
			error: function(a,b,c) {
				$(e.currentTarget).removeClass('green-border2');
				$(e.currentTarget).addClass('red-border2');
				Shop.Popup.error("Тип ["+$(e.currentTarget).val()+"] не зберігся");
				form.find('select[name="excise-product-type"]').val(form.find('select[name="excise-product-type"]').data('val'));
			}
		});
		
	},
	
	updateProductVolume : function(e) {
		
		var form = $(e.currentTarget).closest('tr');
		$.ajax({
			url : '/rest/suppliers/product/' + form.find('input[name="product_id"]').val() + '/volume/',
			data:'volume=' + form.find('input[name="volume"]').val(),
			method : "PATCH",
			success : function(resp){
				$(e.currentTarget).removeClass('red-border2');
				$(e.currentTarget).addClass('green-border2');
				form.find('input[name="volume"]').data('val' , form.find('input[name="code"]').val())
			},
			error: function(a,b,c) {
				$(e.currentTarget).removeClass('green-border2');
				$(e.currentTarget).addClass('red-border2');
				Shop.Popup.error("Ціна ["+$(e.currentTarget).val()+"] не збереглася");
				form.find('input[name="volume"]').val(form.find('input[name="volume"]').data('val'));
			}
		});
		
	},
	
	updateProductCode : function(e) {
		
		var form = $(e.currentTarget).closest('tr');
		$.ajax({
			url : '/rest/suppliers/product/' + form.find('input[name="product_id"]').val() + '/code/' + form.find('input[name="code"]').val(),
			method : "PATCH",
			success : function(resp){
				$(e.currentTarget).removeClass('red-border2');
				$(e.currentTarget).addClass('green-border2');
				form.find('input[name="code"]').data('val' , form.find('input[name="code"]').val())
			},
			error: function(a,b,c) {
				$(e.currentTarget).removeClass('green-border2');
				$(e.currentTarget).addClass('red-border2');
				
				if(a.responseJSON && a.responseJSON.product) {
					Shop.Popup.error("Код [<b>"+$(e.currentTarget).val()+"</b>] для продукту [<b>"+form.find('.excise-product-name').html().replace(' ', '&nbsp;')+"</b>] не зберігся. <br/><br/> Продукт <b>"+a.responseJSON.product.name+"</b> має цей код");
				} else {
					Shop.Popup.error("Код ["+$(e.currentTarget).val()+"] не зберігся");
				}
				
				form.find('input[name="code"]').val(form.find('input[name="code"]').data('val'));
			}
		});
		
	},
	
	
	initTabs : function() {
		var tabs = $('#category-tabs').tabs();
		tabs.delegate( "span.ui-icon-close", "click", function() {
			var li = $(this).closest( "li" );
			Shop.Popup.confirm("Видалення", "Ви дійсно бажаєте видалити групу", function(){
				 var model = new ExciseGroup();
			      model.set({'id' : li.data('id')})
			      model.destroy({
					    success: function (model, response) {
					    	var panelId = li.remove().attr( "aria-controls" );
						      $( "#" + panelId ).remove();
						      tabs.tabs( "refresh" );
					    },
					    error: function (model, response) {
					        console.log("error");
					        console.log(response);
					        Shop.Popup.error("Помилка видалення");
					    }
			      })
			})
		});
		$('.sortable').click(function(){
			var asc = true;
			if($(this).hasClass('asc')) {
				$(this).removeClass('asc');
				$(this).addClass('desc');
				asc = false;
			} else {
				$('.sortable').removeClass('asc');
				$('.sortable').removeClass('desc');
				$(this).addClass('asc');
			}
			var selected = $($('#category-tabs').tabs().data('uiTabs').active).data('id');
			var activeTab = $('#category-tabs').tabs( "option", "active")
			var field = $(this).data('field');
			var isInteger = $(this).hasClass('number');
			var excise = $(this).hasClass('excise');
			exciseGroupList.each(function(item) {
		        if(item.get('id') == selected) {
		        	var sortedArray = _.sortBy(item.get('products'), function(o) { 
		        		if(isInteger) {
		        			return +o[field];
		        		} else if(excise) {
		        			if(o[field]) {
		        				return o[field].id;
		        			}
		        		} else {
		        			return o[field];
		        		}
		        	});
		        	if(!asc) {
		        		sortedArray = sortedArray.reverse();
		        	}
		        	item.set('products', sortedArray);
		        	categoryExciseView.renderSort(selected, field, asc);
		        	$('#category-tabs').tabs( "option", "active", activeTab);
		        	
		        }
		    });
		});
	},
	
	renderSearch : function(regexp) {
		var start = new Date().getTime();
		var params = {'groups' : this.collection.search(regexp).toJSON(), 'exciseTypes' : this.exciseTypes.toJSON()};
		params.regexp = new RegExp(regexp, "gi");
		var el = this.template(params);
		console.log("render1 - " + (start - new Date().getTime()));
		this.el.innerHTML = el;
		console.log("render2 - " + (start - new Date().getTime()))
		this.initTabs();
		$('html, body').animate({
	        scrollTop: $(".excise-match-row").offset().top - 200
	    }, 100);
		console.log("render2 - " + (start - new Date().getTime()))
		
		
	},
	
	renderSort : function(groupId, field, asc) {
		this.$el.html('');
		var params = {'groups' : this.collection.toJSON()};
		params['groupId'] = groupId;
		params['field'] = field;
		params['asc'] = asc ? 'asc' : 'desc';
		params['exciseTypes'] = this.exciseTypes.toJSON();
		this.el.innerHTML = this.template(params);
	//	this.$el.html($(this.template(params)));
		this.initTabs();
	},
	
	render : function() {
		this.$el.html('');
		this.$el.html($(this.template({'groups' : this.collection.toJSON(), 'exciseTypes' : this.exciseTypes.toJSON()})));
		this.initTabs();
	return this;
	}
});

var CategoryDialogView = Backbone.View.extend({
	template : _.template($('#categoryTemplate').html() ),
	initialize: function (options) {
		var self = this;
		this.suppliers = options.suppliers;
		this.suppliers.fetch();
	},
	
	events : {
		"click .supplier-header" : "selectSupplierProducts",
		"submit #category-form" : "submit"
	},
	
	submit : function(e) {
		e.preventDefault();
		var checkedVals = $('#category-form input[type=checkbox]:checked').map(function() {
		    return {'id' : this.value};
		}).get();
		
		var model = new ExciseGroup();
		
		model.set({'id' : $('#category-form #cat-id').val()});
		model.set({'name' : $('#category-form #cat-name').val()});
		model.set({'products' : checkedVals});
		model.save(null, {
			
			success: function (model, response) {
				location.reload();
			},
		    error: function (model, response) {
		    	Shop.Popup.error("Помилка зберiгання. Можливо один з продуктів належить до іншої акцизної категорії");
		    }
		
		});
		
	},
	
	selectSupplierProducts : function(e) {
		$(e.currentTarget).closest('.excise-product-column').find('input[type=checkbox]').each(function(i, input){
			$(input).prop("checked", !$(input).prop("checked"));
		});
	},
	
	render: function(el) {
		this.$el.html('');
		var params;
		var model = new ExciseGroup();
		if(el) {
			var products = el.find('input[name=product_id]').map(function() {
			    return this.value;
			}).get();
			
			var productsMap = {};
			products.forEach(function(o){
				productsMap[o] = 1
			});
			
			model.set({'id': el.find('.group_id').val(), 'name' : el.find('.group_name').val(), 'products':productsMap});
			
		} else {
			model.set({'id': 0, 'name':'','products':{}});
		}
		var params = model.toJSON();
		params['suppliers'] = this.suppliers.toJSON();
		var result = $(this.template(params));
		this.$el.html(result);
		return this;
	}
});

var mainList = new SupplierList();
var categoryDialogView = new CategoryDialogView({ suppliers: mainList, el: $("#dialog-categ") });
var exciseGroupList = new ExciseGroupList()
var categoryExciseView = new CategoryExciseView({collection: exciseGroupList, el : $('#excise-category-view')});
new ExciseTypesView();


$('#excise-search input').on('keyup', function(e){
	
	if($(this).val() == '') {
		categoryExciseView.render();
	} else {
		categoryExciseView.renderSearch($(this).val())
	}
	
	
	
	
});