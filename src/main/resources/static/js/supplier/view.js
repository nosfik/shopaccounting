//View for all people
var SupplierListView = Backbone.View.extend({
	initialize: function () {
		var self = this;
		this.collection.fetch({ success: function () { 
				self.render();
				self.listenTo(self.collection, 'remove', self.render);
				self.listenTo(self.collection, 'add', self.render);
			} 
		});
		
		this.controls = new SuppliersControls({el:$('#suppliers-controls'),parent:this});
	},
	
	renderAfterRemove : function() {
		var self = this;
		$('#supplier-products').empty();
		this.collection.reset();
		this.collection.fetch({ success: function () { 
			self.render();
		}	 
		});
	},
	
	renderList : function(regexp){
		var collection = this.collection.search(regexp);
		this.$el.html('');
		collection.each(function(supplier) {
			var supplierView = new SupplierView({model: supplier, suppliers:collection, regexp:regexp});
			this.$el.append(supplierView.render().el);
		}, this);
		return this;
	},
	
	renderUnusedProduct : function(unusedProducts) {
		
		var collection = this.collection.filterByProducts(unusedProducts);
		this.$el.html('');
		collection.each(function(supplier) {
			var supplierView = new SupplierView({model: supplier,  suppliers:collection, unusedProducts : unusedProducts[supplier.get('id')]});
			this.$el.append(supplierView.render().el);
		}, this);
		return this;
		
	},
	
	render: function(activeModel) {
		this.$el.html('');
		var self = this;
		this.collection.each(function(supplier) {
			var supplierView = new SupplierView({model: supplier, suppliers:self.collection, active:(supplier == activeModel)});
			this.$el.append(supplierView.render().el);
		}, this);
		return this;
	}
});

var SuppliersControls = Backbone.View.extend({
	oldProdEl:$('#old-products'),
	initialize: function (options) {
		this.parent = options.parent;
		this.listenTo(this.parent.collection, 'change', this.listRender);
	},
	events: {
	    "click #add-supplier" :"addSupplier",
	    "click #find-supplier" :"findSupplier",
	    "click #find-old-products" :"findOldProducts"
	   
	},

	
	findOldProducts: function(event) {
		event.preventDefault();
		$('.supplier-item-full').html('');
		if (this.oldProdEl.is(":visible")) {
			this.oldProdEl.hide();
			$(event.currentTarget).removeClass('active');
			listView.render();
		} else {
			this.oldProdEl.show();
			$(event.currentTarget).addClass('active');
		}
	},
	addSupplier: function() {
		this.parent.collection.add(new Supplier());
	},
	render: function() {
		this.el;
	}
});

var SupplierFullView = Backbone.View.extend({
	initialize: function (options) {
		this.suppliers = options.suppliers;
		this.categories = new ProductCategoryList();
		this.categories.fetch();
	},
	template: _.template($('#supplierTemplateFull').html() ),
	events: {
	    "click #supplier-delete" : "remove",
	    "click #add-product" : "addProduct",
	    "change input" :"updateTitle",
	    "change textarea" :"updateTitle",
	    "change select" :"updateCategory",
	    "click #edit-pic" : "toggleEditMode"
	},
	
	toggleEditMode: function(e) {
		var field = $(e.currentTarget), editBlock = field.closest('.edit-block'),
		title = editBlock.find('.edit-block-title'),
		commentsBlock = $('#supplier-comment div'),
		commentsTextArea = $('#supplier-comment textarea'),
		input = editBlock.find('.edit-block-input');
		select = editBlock.find('.edit-block-select');
		
		if(title.is(':visible')) {
			title.hide();
			commentsBlock.hide();
			commentsTextArea.show();
			if(input.length) {
				input.show();
			} else {
				select.show();
				setTimeout(function(){
					 var dropdown = document.getElementById('supplier-category');
					    var event;
					    event = document.createEvent('MouseEvents');
					    event.initMouseEvent('mousedown', true, true, window);
					    dropdown.dispatchEvent(event);
					
				}, 10)
			}
		} else {
			commentsBlock.html(commentsTextArea.val());
			commentsBlock.show();
			commentsTextArea.hide();
			title.show();
			if(input.length) {
				title.html(input.val());
				input.hide();
			} else {
				title.html(select.find('option:selected').html());
				select.hide();
			}
		}
	},
	
	updateTitle: function(e){
		var field = $(e.currentTarget), newField = {}, isNewSupplier = !this.model.get('id'), view = this;
		console.log(isNewSupplier)
		newField[field.attr('name')] = field.val();
		this.model.set(newField);
		this.model.save(null, {
			success:function(){
				if(isNewSupplier) {
					view.render();
				}
			},
			error:function(){
				Shop.Popup.error("Помилка зберiгання");
			}
		});
	},
	
	updateCategory: function(e) {
		var field = $(e.currentTarget), newField = {};
		newField[field.attr('name')] = {id : field.val()};
		this.model.set(newField);
		this.model.save(null, {
			error:function(){
				Shop.Popup.error("Помилка зберiгання");
			}
		});
		this.toggleEditMode(e);
	},
	
	renderCategories : function() {
		
		var supplierCategorySelectEl = $('#supplier-category');
		supplierCategorySelectEl.empty();
		supplierCategorySelectEl.append("<option></option>");
		var me = this;
		
		this.categories.each(function(el){
			if(me.model.get('productCategory') && el.get('id') == me.model.get('productCategory').id) {
				$('.supplier-category .supplier-category-title').html(el.get('name'));
				supplierCategorySelectEl.append("<option value=" + el.get('id') + " selected=\"selected\">"+el.get('name')+"</option>");
			} else {
				supplierCategorySelectEl.append("<option value=" + el.get('id') + ">"+el.get('name')+"</option>");
			}
		});
	},
	
	remove : function(e) {
		
		
		
		Shop.Popup.confirm("Видалення постачальника","Ви дійсно бажаєте видалити  <b>'"+this.model.get('name')+"'</b>",
    			
		    	function(){
		    		
		    		
		    		this.model.destroy(
		    				{
		    					success: function(model, response) {
		    						this.suppliers.remove(model);
		    						this.parent.render();
		    						this.$el.empty();
		    					}.bind(this),
		    					error : function(model, response) {
		    						Shop.Popup.error("Помилка видалення")
		    					}
		    				})
		    		
			    	
		    	}.bind(this)
		    	)
	},
	
	addProduct: function(e) {
		var newProduct = new Product({name:'Новий продукт',price:'0.0',margin:'',marginPrice:'0.0', excisePrice:'',code:null,volume:null});
		if(this.model.get("products").length === 0) {
			this.model.set({'products' : new ProductList()});
		}
		this.model.get("products").add(newProduct);
		
		var productView = new ProductView({model:newProduct, parent:this})
		$("#supplier-products-container tbody").append(productView.render().el)
		productView.editProduct(null,this.model.get("id"));
		
	},

	highLightUnusedProducts : function(products){
		var i,id;
		this.$el.find(".product-item").each(function(i, el) {
			id = +el.id;
			for(i = 0; i < products.length; i++) {
				if(id == products[i]) {
					$(el).addClass("highlight-unused");
					break;
				}
			}
		});
	},
	
	highLight : function(regexp){
		var length = regexp.length;
		this.$el.find(".supplier-product-name").each(function(i, el) {
			
			if(el.innerHTML.toLowerCase().indexOf(regexp.toLowerCase()) >= 0) {
				$(el).closest('tr').addClass("highlight")
			}
		});
	},
	
	render : function(){
		var result = $(this.template(this.model.toJSON()));
		var productContainer = result.find("#supplier-products-container tbody");
		var parentView = this;
		if(this.model.get('products').length) {
			this.model.get('products').each(function(product, index) {
				if(!product.get('delete')) {
					productContainer.append(new ProductView({model:product, parent:parentView, id:product.get('id')}).render().el)
				}
			}, this);
		}
		
		this.$el.html(result);
		this.renderCategories();
		this.parent.highLight();
		this.$el.find('.uibutton-addp').button();
		return this;
	}
})

var ProductMoveView = Backbone.View.extend({
	template : _.template($('#productMove').html() ),

	initialize : function (options) {
		this.render();
	 },

	validateAndSubmit: function(){
		var valid = true;

		if(!$('input[name="product_move"]:checked').val()) {
			valid = false
            $('input[name="product_move"]').closest('td').css({'border': '1px solid red'})
		} else {
            $('input[name="product_move"]').closest('td').css({'border': ''})
		}

        if(!(+$('select[name=new_supplier]').val())) {
            valid = false
            $('select[name=new_supplier]').closest('td').css({'border': '1px solid red'})
        } else {
            $('select[name=new_supplier]').closest('td').css({'border': ''})
        }

        if(valid) {
			///product/{productId}/move/{supplierId}
            $.ajax({
                async: false,
                url : '/rest/suppliers/product/' + $('input[name=product_id]').val() +'/' + $('input[name="product_move"]:checked').val() + '/' + $('select[name=new_supplier]').val(),
                method : "PUT",
                success : function(){
                   location.reload();
                },
                error: function(a,b,c) {
                    Shop.Popup.error("Помилка");
                }
            });
		}

	},

	 render : function(){
		 var params = this.model.toJSON();
		 params.suppliers = mainList.toJSON();
		 var submit = this.validateAndSubmit
		 $('<div />').html( $(this.template(params))).dialog({
			 title:"Перенести продукт",
			 width:340,
			 modal:true,
			 open: function(){
                 $('#move-product-button').click(submit);
			 },
			 close: function(){
                 $(this).remove();
			 }
		 });

	 }
});
	

var ProductView = Backbone.View.extend({
		initialize : function (options) {
		    this.parent = options.parent;
		    this.odd = options.odd;
		  },
		template : _.template($('#productTemplate').html() ),
		templateFull : _.template($('#productFullTemplate').html() ),
		tagName: 'tr',
		className: 'product-item',
		
		events: {
		    "change input.product-input": "fieldChanged",
		    "click .remove-product" : "removeProduct",
		    "click .edit-product" : "editProduct",
		    "keyup .changeable" : "calculate"
		    
		},
		
		moveProduct: function(e) {
			e.preventDefault();
			new ProductMoveView({model:e.data})
		},
		
		
		checkCode : function(el){
			
        		var response = true;
        		
        		if(el.val() != '') {
        			$.ajax({
        				async: false,
            			url : '/rest/suppliers/product/code/'  + el.val() + "?productId=" + this.model.get('id'),
            			method : "GET",
            			success : function(resp){
            				el.removeClass('red-border2');
            				el.addClass('green-border2');
            				el.data('val' ,el.val());
            				el.data('valid', true);
            			},
            			error: function(a,b,c) {
            				response = false;
            				el.removeClass('green-border2');
            				el.addClass('red-border2');
            				el.data('valid', false);
            				if(a.responseJSON && a.responseJSON.product) {
            					Shop.Popup.error("Даний код зайнятий. <br/><br/> <b>"+a.responseJSON.product.name+"</b><br/> має цей код");
            				} else {
            					Shop.Popup.error("Даний код зайнятий");
            				}
            				
            				el.val(el.data('val'));
            			}
            		});
        		}
        		
        		return response;
			
		},
		
	
		editProduct : function(e, supplierId){
			if(e)
				e.preventDefault();
			$('#dialog-product').remove();
			var model = this.model, parent = this.parent, self=this;
			 $(this.templateFull(this.getParams())).dialog({
				 title : model.get('name'),
				 modal:true,
				 width:'450px',
				 buttons : [{text: "Зберегти",click: function(e) { 
					 
					 $('#dialog-product input').each(function(i,el){
						 var field = {};
						 field[$(el).attr('name')] = $(el).val() == '' ? null : $(el).val() ;
						 model.set(field)
					 })
					 
					 if(self.checkCode($('#dialog-product input[name=code]'))) {
						 var config = {};
						 config.success = function() {
						 		$( this ).dialog( "close" );
						 		parent.render();
						 	}.bind(this);
						 config.error = function(){
							 Shop.Popup.error("Помилка зберiгання");
						 	}.bind(this);
						 
						 if(supplierId) {
							 model.set({'supplier':{'id':supplierId}});
						 }
						 model.save(null, config);
					 }
					 }},
					          {text: "Відміна", click: function() { $( this ).dialog( "close" ); }}]
			 });
			 $('#dialog-product .changeable').on('keyup',this.calculate);
			 $('#dialog-product #product-move').click(this.model, this.moveProduct);
			 
		},
		render: function() {
			this.$el.html( this.template(this.getParams()) );
			return this;
		},
		
		getParams : function() {
			var params = this.model.toJSON();

			if(!params.id) {
				params.id = 0;
			}
			if(params.marginPrice && params.price && params.price != 0.0) {
				params.margin = (params.marginPrice / params.price * 100) - 100;
				params.margin = params.margin.toFixed(2);
			} else {
				params.margin = "";
			}
			return params;
		},
		
		
		
		calculate : function(e) {
			var dialog = $('#dialog-product');
			var price=+dialog.find("input[name=price]").val(), marginPrice=+dialog.find("input[name=marginPrice]").val(), margin=+dialog.find("input[name=margin]").val()
			
			if( price ) {
				if($(e.currentTarget).attr("name") == "marginPrice") {
					var newMargin = (marginPrice / price * 100) - 100;
					dialog.find("input[name=margin]").val(newMargin.toFixed(2));
					
				} else {
					var newMarginPrice = price * (100 + margin) / 100;
					dialog.find("input[name=marginPrice]").val(newMarginPrice.toFixed(2));
					dialog.find("input[name=marginPrice]").trigger("change");
				}
			} 
		},
		
		fieldChanged: function(e) {
			var field = $(e.currentTarget);
			var data = {};
			data[field.attr('name')] = field.val();
			this.model.set(data);
	    },
	    
	    removeProduct: function(e) {
	    	e.preventDefault();
	    	
	    	
	    	Shop.Popup.confirm("Видалення продукту","Ви дійсно бажаєте видалити  <b>'"+this.model.get('name')+"'</b>",
	    			
	    	function(){
	    		
	    		
	    		this.model.destroy(
	    				{
	    					success: function(model, response) {
	    						this.parent.render();
	    					}.bind(this),
	    					error : function(model, response) {
	    						Shop.Popup.error("Помилка видалення")
	    					}
	    				})
	    		
		    	
	    	}.bind(this)
	    	)
	    	
	    	
	    }
	
});


var SupplierView = Backbone.View.extend({
	initialize : function(options) {
		this.listenTo(this.model, 'change', this.render);
		this.suppliers = options.suppliers;
		this.regexp = options.regexp;
		this.unusedProducts = options.unusedProducts;
		if(options.active) {
			this.renderFull();
		}
	},
	tagName: 'li',
	template: _.template($('#supplierTemplate').html() ),
	events: {
	    "click .supplier-item" : "renderFull"
	},
	
	fullView: new SupplierFullView({el:$('#supplier-products')}),
	
	highLight : function() {
		if(this.regexp) {
			this.fullView.highLight(this.regexp);
		}
		
		if(this.unusedProducts) {
			this.fullView.highLightUnusedProducts(this.unusedProducts);
		}
	},
	
	renderFull : function() {
		this.fullView.unbind();        
		this.fullView.model = this.model;
		this.fullView.suppliers = this.suppliers;
		this.fullView.parent = this;
		this.fullView.render();
		this.highLight();
	},
	
	render: function() {
		this.$el.html( this.template(this.model.toJSON()) );
		return this;
	}
});

var mainList = new SupplierList()
var listView = new SupplierListView({ collection: mainList, el: $("#suppliers") });

$("#magic-search .search-key").keyup(function(e){
	var key = $("#magic-search .search-key").val();
	listView.renderList(key);
	var pattern = new RegExp(key,"gi")
	$('#suppliers li').each(function(i, el) {
		var itemName = $(el).find('.supplier-item').html();
		 $(el).find('.supplier-item').html(itemName.replace(pattern, "<span class='highlight'>$&</span>"));
	});
})

$('#find-unused-products').click(function(){
	
	$.getJSON("/rest/suppliers/unusedproducts/" + $('#old-products .not-used-days').val(), function(unusedProducts){
		
		var totalObj = {}; 
		mainList.each(function(supplier){ 
			var sid = supplier.get('id'); 
			var products = supplier.get('products');
			products.each(function(product) {
			   totalObj[product.get('id')] = sid;
			})
		});
		
		var productsToHighLight = {};
		_.each(unusedProducts, function(prodId) {
			var sid = totalObj[prodId]
			
			if(productsToHighLight[sid]) {
				productsToHighLight[sid].push(prodId);
			} else {
				productsToHighLight[sid] = [];
				productsToHighLight[sid].push(prodId);
			}
		});
		
		listView.renderUnusedProduct(productsToHighLight);
		
	});
	
});
