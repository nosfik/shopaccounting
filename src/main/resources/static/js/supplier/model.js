var Supplier = Backbone.Model.extend({
	urlRoot : '/rest/suppliers',
	initialize : function() {
		this.set({'products' : new ProductList(this.get('products'))});
	}
});

var Product = Backbone.Model.extend({
	urlRoot:'/rest/suppliers/product',
	initialize : function() {
		
	}
		
});

var ProductList = Backbone.Collection.extend({
	model : Product
});

var SupplierList = Backbone.Collection.extend({
	model : Supplier,
	url : '/rest/suppliers',
	filterByProducts : function(unusedSupplierProducts) {
		
		return _(this.filter(function(data) {
			return unusedSupplierProducts.hasOwnProperty(data.get('id'));
		}));
	},
	search : function(key){
		if(key == "") return this;
		var pattern = new RegExp(key,"gi"), prod;
		return _(this.filter(function(data) {
			pattern.lastIndex = 0;
			if(!pattern.test(data.get("name"))){
				prod = false;
		  		data.get('products').each(function(product){
		  			pattern.lastIndex = 0;
		  			if(pattern.test(product.get("name"))) {
		  				prod = true;
		  			}
		  		});
		  		return prod;
		  	} else {
		  		return true;
		  	}
		  
		}));
	}
});

var ProductCategory = Backbone.Model.extend({
	urlRoot : '/rest/product/categories',
});

var ProductCategoryList = Backbone.Collection.extend({
	model : ProductCategory,
	url : '/rest/product/categories',
});

