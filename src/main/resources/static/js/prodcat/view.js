var ProductCategoryListView = Backbone.View.extend({
	initialize: function () {
		var self = this;
		this.collection.fetch({ success: function () { 
				self.render();
				self.listenTo(self.collection, 'add', self.render);
			} 
		});
		
		this.controls = new Controls({el:$('#category-poduct-control'), parent:this});
		
	},
	renderAfterRemove : function() {
		console.log('render after remove')
		var self = this;
		this.collection.reset();
		this.collection.fetch({ success: function () { 
			self.render();
		}	 
		});
	},
	render: function() {
		console.log('render');
		this.$el.html('');
		this.collection.each(function(productCategory) {
			var productCategoryView = new ProductCategoryView({model: productCategory, parent:this});
			this.$el.append(productCategoryView.render().el);
		}, this);
		return this;
	}
});

var Controls =  Backbone.View.extend({
	
	initialize: function (options) {
		this.parent = options.parent;
	},
	
	events: {
	    "click #add-new-category" :"addCategory",
	    "click #category-save" :"save"
	},
	addCategory: function() {
		this.parent.collection.add(new ProductCategory({name:"Нова категорiя"}));
	},
	render: function() {
		this.el;
	}

});

var ProductCategoryView = Backbone.View.extend({
	initialize: function (options) {
		this.parent = options.parent;
	},
	tagName: 'tr',
	template: _.template($('#productTemplate').html() ),
	events: {
	    "click .delete-category" : "remove",
	    "change input": "fieldChanged",
	},
	
	remove : function(e) {
		var parent = this.parent;
		this.model.destroy({success: function(model, response) {
			parent.render();
		}})
	},
	
	fieldChanged: function(e){
		var field = $(e.currentTarget);
		var data = {};
		data[field.attr('name')] = field.val();
	    this.model.set(data);
	    this.model.save();
    },
		
	render : function(){
		this.$el.html($(this.template(this.model.toJSON())));
		return this;
	}
});

var listView = new ProductCategoryListView({ collection: new ProductCategoryList(), el: $("#category-poducts-content") });