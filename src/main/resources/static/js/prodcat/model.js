var ProductCategory = Backbone.Model.extend({
	urlRoot : '/rest/product/categories',
});

var ProductCategoryList = Backbone.Collection.extend({
	model : ProductCategory,
	url : '/rest/product/categories',
});