var invoiceEvents = _.extend({}, Backbone.Events);

var RevenueExpensesListView  = Backbone.View.extend({
	event : invoiceEvents,
	initialize: function () {
		_.bindAll(this, "fetchAndRender");
		this.event.bind("reloadRevenueList", this.fetchAndRender)
		this.fetchAndRender();
	},
	fetchAndRender : function() {
		var self = this;
		this.collection.reset();
		console.log(this.collection);
		this.collection.fetch({success:function(){
			self.render();
		}});
	},
	
	render: function() {
	//	console.log('start rendering');
		this.$el.empty();
		var self = this;
		var number = 1;
		$('.blockable').show();
		this.collection.each(function(revenueExpenses) {
			if(revenueExpenses.get('type') == 1) {
				$('.blockable').hide();
				Shop.invoice.currentView = "invoiceBlocked";
			}
			this.$el.append(new RevenueExpensesView({model:revenueExpenses, number:number++}).render().el)
		}, this);
		return this;
	}
	
});

var RevenueExpensesView   = Backbone.View.extend({
	
	initialize: function (options) {
		this.model = options.model;
		this.number = options.number;
	},
	events : {
		'change td.revenue-print input' : 'selectPrint',
	},
	
	selectPrint: function(e) {
		this.model.set({'print':$(e.currentTarget).prop('checked')});
	},
	revenueTypes: revenueTypes,
	className: 'revenue-row',	
	tagName: 'table',
	revenueTypes: revenueTypes,
	template: _.template($('#revenueExpensesRow').html() ),
	render : function() {
		var params = this.model.toJSON();
		params['number'] = this.number;
		params['type_name'] = this.revenueTypes[params.type];
		this.$el.html($(this.template(params)));
		return this;
	}
});






var InvoiceListView = Backbone.View.extend({
	
	el: $('#invoice-table'),
	
	initialize: function (options) {
		this.credit = options.credit;
		if(!this.credit) {
			this.fetchAndRender();
			this.event = options.event;
			_.bindAll(this, "fetchAndRender");
			this.event.bind("reloadInvoiceList", this.fetchAndRender);
			this.listenTo( this.collection, 'sort', this.render ); 
		}
	},
	
	events : {
		'click th.sortable' : "sortBy"
	},
	
	
	sortBy : function(e) {
		var el = $(e.currentTarget), asc = true;
		
		if(el.hasClass('asc')) {
			el.removeClass('asc');
			el.addClass('desc');
			asc = false;
		} else {
			$('.sortable').removeClass('asc');
			$('.sortable').removeClass('desc');
			el.addClass('asc');
		}
		
		var field = el.data('field');
		this.collection.sortByField(field, asc)
		
	},
	
	fetchAndRender : function() {
		var self = this;
		this.collection.fetch({reset:true, success:function(){
			self.render();
		}});
	},
	
	render: function() {
		console.log('start rendering');
		this.$el.find('#invoice-table-body').empty();
		var self = this;
		var number = 1;
		this.collection.each(function(invoice) {
			this.$el.find('#invoice-table-body').append(new InvoiceView({event:this.event, credit:this.credit, model:invoice, number:number++}).render().el)
		}, this);
		return this;
	}
});


var InvoiceFullView = Backbone.View.extend({
	initialize: function (options) {
		this.model = options.model;
	},
	event : invoiceEvents,
	template: _.template($('#invoice-full').html()),
	className: 'full-view',
	tagName : "tr",
	
	render : function() {
		this.$el.html($(this.template(this.model.toJSON())));
		return this;
	}
	
});


var InvoiceView = Backbone.View.extend({
	
	initialize: function (options) {
		this.event = options.event;
		this.model = options.model;
		this.number = options.number;
		this.credit = options.credit;
	},
	className: 'invoice-row invoice-row-value',	
	
	events : {
		'dblclick td' : 'showFull',
		'change .print-column input' : 'selectPrint',
	},
	
	selectPrint: function(e) {
		var row = $(e.currentTarget).closest('tr');
		if(row.hasClass('checked')) {
			row.removeClass('checked')
		} else {
			row.addClass('checked')
		}
		
		
		this.model.set({'print':$(e.currentTarget).prop('checked')});
	},
	
	showFull: function() {
		
		var fullView = this.$el.find('.full-view');
		if(fullView.length > 0) {
			fullView.remove()
		} else {
			this.$el.find('tbody').append(new InvoiceFullView({event:this.event, model:this.model}).render().el);
		}
		
	},
	
	tagName: 'table',
	template: _.template($('#invoiceRow').html() ),
	render : function() {
		var params = this.model.toJSON();
		params['number'] = this.number;
		params.credit = this.credit;
		
		if(this.model.get('credit')){
			this.$el.addClass('credit-invoice')
		}
		
		this.$el.html($(this.template(params)));
		return this;
	}
	
});

var invoiceList = new InvoiceList();
var revenueList = new RevenueExpensesList();


var view = new InvoiceListView({event : invoiceEvents, collection: invoiceList});

var revenueView = new RevenueExpensesListView({collection: revenueList, el:$('#expenses-table')});

Shop.invoice = {};
Shop.invoice.currentView = "invoice";


//Shop.invoice.togglePrint = false;
$('#invoice-container th.print-column').click(function(){
	$('#invoice-container input[type=checkbox]').click();
})

$('#expenses-container th.revenue-print').click(function(){
	$('#expenses-container input[type=checkbox]').click();
})


$('#build-print-view').click(function() {
	
	$.ajax({
		url:'/rest/balance/' + $('#invoiceDate').val().replace(/-/g,'')+'/true',
		method:"GET",
		success: function(data) {
			if(data && data.balance){
				$('#total-print .balance').html(data.balance);
			}
		}
	})
	
	$('#invoice-print-products tbody').empty();
	var invoicePrintTemplate = _.template($('#invoicePrint').html());
	var revenuePrintTemplate = _.template($('#revenuePrint').html());
	
	Shop.invoice.totalInvoiceRevenue = 0;
	Shop.invoice.totalInvoiceExpenses = 0;
	Shop.invoice.totalRevenueRevenue = 0;
	Shop.invoice.totalRevenueExpenses = 0;
	
	var elements = 0;
	invoiceList.each(function(invoice) {
		if(invoice.get('print')) {
			Shop.invoice.totalInvoiceRevenue += invoice.get('totalWithMargin');
			var expenses = invoice.get('expenses');
			if(expenses) {
				Shop.invoice.totalInvoiceExpenses += expenses;
			}
			$('#invoice-print-products tbody').append(invoicePrintTemplate(invoice.toJSON()));
			elements++;
		}
	});
	
	if(elements == 0) {
		$('#invoice-print-products').hide();
	} else {
		elements = 0;
	}
	
	Shop.invoice.invoiceTable = $('#invoice-print-products').clone();

	revenueList.each(function(revenue) {
		if(revenue.get('print')) {
			var expenses = revenue.get('expenses');
			if(expenses) {
				Shop.invoice.totalRevenueExpenses += expenses;
			}
			var rev = revenue.get('revenue');
			if(rev) {
				Shop.invoice.totalRevenueRevenue += rev;
			}
			$('#revenue-print tbody').append(revenuePrintTemplate(revenue.toJSON()));
			elements++
		}
	});
	
	if(elements == 0) {
		$('#revenue-print').hide();
	} else {
		elements = 0;
	}
	
	Shop.invoice.totalRevenue = Shop.invoice.totalInvoiceRevenue + Shop.invoice.totalRevenueRevenue;
	Shop.invoice.totalExpenses = Shop.invoice.totalInvoiceExpenses + Shop.invoice.totalRevenueExpenses;
	
	Shop.invoice.revenueTable = $('#revenue-print').clone();
	
	$('#total-print .invoice-revenue').html(Shop.invoice.totalInvoiceRevenue.toFixed(2));
	$('#total-print .invoice-expenses').html(Shop.invoice.totalInvoiceExpenses.toFixed(2));
	$('#total-print .revenue-revenue').html(Shop.invoice.totalRevenueRevenue.toFixed(2));
	$('#total-print .revenue-expenses').html(Shop.invoice.totalRevenueExpenses.toFixed(2));
	$('#total-print .revenue').html(Shop.invoice.totalRevenue.toFixed(2));
	$('#total-print .expenses').html(Shop.invoice.totalExpenses.toFixed(2));
	
	
	$("#invoice-title").html('Видаткова накладна - ' +  $('#date-block span').html());
	
	$('#control-print').show()
	$('#print-area').show();
	$('#page-emulator').show();
	$('#invoice-print-container').hide();
	$('#build-print-view').hide();
	
});

$('#invoiceDate').on('change', function(){
	Shop.invoice.currentView = "invoice";
	view.fetchAndRender();
	revenueView.fetchAndRender();
});
