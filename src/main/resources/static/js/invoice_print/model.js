var Invoice = Backbone.Model.extend({
	urlRoot : '/rest/invoices',
	initialize : function() {
		//this.set({'products' : new InvoiceProductList(this.get('products'))});
	}
});

var InvoiceCreditList = Backbone.Collection.extend({
	model : Invoice,
	url : function(){
		return '/rest/invoices/credit';
	}
});

var InvoiceList = Backbone.Collection.extend({
	model : Invoice,
	sort_key : 'name',
	sort_order : 'asc',
	comparator: function(item) {
		var result;
		var arr = this.sort_key.split('.');
		if(arr.length === 1 ) {
			result = item.get(this.sort_key);
		} else {
			return item.get(arr[0])[arr[1]];
		}
		
		return "asc" == this.sort_order ? result: -1*result ;
	},
	
	sortByField: function(fieldName, sortOrder) {
		this.sort_key = fieldName;
		this.sort_order = sortOrder ? 'asc' : 'desc';
		this.sort();
	},
	
	
	url : function(){
		return '/rest/invoices/date/' + $('#invoiceDate').val().replace(/-/g,'');
	}
});


var InvoiceProduct = Backbone.Model.extend({
	
});

var InvoiceProductList = Backbone.Collection.extend({
	model : InvoiceProduct
});


var ProductCategory = Backbone.Model.extend({
	
});

var ProductCategories = Backbone.Collection.extend({
	url : '/rest/product/categories'
});

var Suppliers = Backbone.Collection.extend({
	initialize : function(options) {
		this.category = options.category;
	},
	url : function(){
		return (this.category > 0) ? '/rest/suppliers/category/'+this.category : '/rest/suppliers';
	}
});


var RevenueExpenses  = Backbone.Model.extend({
	urlRoot : '/rest/revenue',
});

var RevenueExpensesList = Backbone.Collection.extend({
	model : RevenueExpenses,
	url : function(){
		return '/rest/revenue/date/' + $('#invoiceDate').val().replace(/-/g,'');
	}
});

