

var Shop = {};
Shop.util = {};



Shop.util.uuid = function(text) {

	function s4() {
		return Math.floor((1 + Math.random()) * 0x10000).toString(16)
				.substring(1);
	}
	return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4()
			+ s4() + s4();

}

Shop.util.calculateString = function(text) {
	
	
	if(typeof text === 'string') {
		text = text.replace(/[^0-9\.\+\-\*\/\(\)]/g,'');
		if(text.length) {
			return eval(text).toFixed(2);
		} 
	} 
	
	return text
	
}

Shop.util.getPriceView = function(price, scale){
	if (typeof price === 'string') {
		return price;
	} else if(typeof price === 'number') {
		
		if(price % 1 === 0) {
			return price.toString();
		} else {
			if(scale) {
				return price.toFixed(scale);
			} else {
				var priceStr = price + "";
				var length = priceStr.substr(priceStr.indexOf('.') + 1, priceStr.length).length;
				length = (length == 3) ? 3 : 2;
				return price.toFixed(length);
			}
		}
		
	} else {
		return price;
	}
}

Shop.util.formatPrice = function(price) {
	return price.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')
}

var monthNames = ["Січня", "Лютого", "Березня", "Квітня", "Травня", "Червня", 
                  "Липня", "Серпня", "Вересня", "Жовтня", "Листопада", "Грудня"];

var monthNamesInf =['Січень','Лютий','Березень','Квітень','Травень','Червень',
  	'Липень','Серпень','Вересень','Жовтень','Листопад','Грудень'];
                  
$.datepicker.regional['uk'] = {
		closeText: 'Закрити',
		prevText: '&#x3C;',
		nextText: '&#x3E;',
		currentText: 'Сьогодні',
		monthNames: monthNamesInf,
		monthNamesShort: ['Січ','Лют','Бер','Кві','Тра','Чер',
		'Лип','Сер','Вер','Жов','Лис','Гру'],
		dayNames: ['неділя','понеділок','вівторок','середа','четвер','п’ятниця','субота'],
		dayNamesShort: ['нед','пнд','вів','срд','чтв','птн','сбт'],
		dayNamesMin: ['Нд','Пн','Вт','Ср','Чт','Пт','Сб'],
		weekHeader: 'Тиж',
		dateFormat: 'dd.mm.yy',
		firstDay: 1,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''};
$.datepicker.setDefaults($.datepicker.regional['uk']);



function setCurrentDate(date, trigger, el) {
	
	var year, month, day, dateEl;
	dateEl = el ? el : $('#invoiceDate');
	if(date.currentYear) {
		year = date.currentYear;
		month = date.currentMonth;
		day = date.currentDay;
	} else {
		year = date.getFullYear();
		month = date.getMonth();
		day = date.getDate();
	}
	month += 1;
	if(month <= 9) {
		month = "0" + month;
	}
	if(day <= 9) {
		day = "0" + day;
	}
	dateEl.val("" + year +'-'+ month +'-'+ day);
	 
	 if(trigger) {
		 dateEl.trigger(trigger);
	 }
}

$(function() {
	$('.uibutton').button();
	
	Shop.Popup = {
			dialog : $("#dialog-popup"),
			close: function() {
				this.dialog.dialog("destroy");
				this.dialog.empty();
			},
			open : function(title, message, cssClass, buttons) {
				if(this.dialog.dialog) {
				}
				this.dialog.empty();
				this.dialog.html(message);
				var conf = {
					  dialogClass: "shop-dialog-" + cssClass,
					  title: title,
					  modal:true,
					  minHeight: 200,
					  minWidth:200,
					
				}
				
				conf.buttons = (buttons) ? buttons : [];
				
				this.dialog.dialog(conf);
				
			},
			confirm : function(title, message, success, cancel) {
				
				
				var buttons =  [
						          {text: "Так",click: function() { $( this ).dialog( "close" ); if(success){success()} }},
						          {text: "Ні", click: function() { $( this ).dialog( "close" ); if(cancel){cancel()} }}
						        ];
				
				this.open(title, message, "warning", buttons);
				
			},
			warn : function(title, message, buttons) {
				this.open(title, message, "warning", buttons)
			},
			info : function(title, message) {
				this.open(title, message,"info")
			},
			error : function(message) {
				this.open("Помилка", message,"error")
			},
			errorFromException : function(exception, message) {
				  if(exception == "com.shop.base.exception.BalanceNotFoundException") {
					  this.open("Помилка", "Помилка! Баланс вiдсутнiй", "error");
			      } else if(exception == "com.shop.base.exception.AccessDeniedException") {
			    	  this.open("Помилка", "Помилка! Операцiя не доступна", "error");
			      } else if(exception == "com.shop.base.exception.ShopException") {
			    	  if("shop.first.proceed.require" == message) {
			    		  this.open("Помилка", "Ранішня виручка не внесена!", "error");
			    	  } else if("shop.second.proceed.already.saved" == message) {
			    		  this.open("Помилка", "Друга виручка вже була внесена", "error");
			    	  } else if("shop.first.proceed.already.saved" == message) {
			    		  this.open("Помилка", "Перша виручка вже була внесена", "error");
			    	  } else {
			    		  this.open("Помилка", "Помилка!", "error");
			    	  }
			      } else if(exception == "com.shop.base.exception.NotClosedDayException") {
			    	  this.open("Помилка", "Помилка! Операцiя не доступна. <br/> Виручка не внесена за попереднi днi. <br/> <strong>" + message + "</strong>", "error");
			      } else {
			    	  this.open("Помилка", "Помилка","error")
			      }
				  
			},
		};
});








