var Invoice = Backbone.Model.extend({
	urlRoot : '/rest/invoices',
	initialize : function() {
		//this.set({'products' : new InvoiceProductList(this.get('products'))});
	}
});

var InvoiceCreditList = Backbone.Collection.extend({
	model : Invoice,
	url : function(){
		return '/rest/invoices/credit';
	}
});

var InvoiceList = Backbone.Collection.extend({
	model : Invoice,
	url : function(){
		return '/rest/invoices/date/' + $('#invoiceDate').val().replace(/-/g,'');
	}
});

var InvoiceSearchList = Backbone.Collection.extend({
	model : Invoice,
	initialize : function(params) {
		this.qParams = params.qParams;
	},
	url : function(){
		return '/rest/invoices/search/?' + this.qParams
	}
});

var InvoiceProduct = Backbone.Model.extend({
	
});

var InvoiceProductList = Backbone.Collection.extend({
	model : InvoiceProduct
});


var ProductCategory = Backbone.Model.extend({
	
});

var ProductCategories = Backbone.Collection.extend({
	url : '/rest/product/categories'
});

var Suppliers = Backbone.Collection.extend({
	initialize : function(options) {
		this.category = options.category;
	},
	url : function(){
		return (this.category > 0) ? '/rest/suppliers/category/'+this.category : '/rest/suppliers';
	}
});


var RevenueExpenses  = Backbone.Model.extend({
	urlRoot : '/rest/revenue',
});

var RevenueExpensesList = Backbone.Collection.extend({
	model : RevenueExpenses,
	url : function(){
		return '/rest/revenue/date/' + $('#invoiceDate').val().replace(/-/g,'');
	}
});

