var invoiceEvents = _.extend({}, Backbone.Events);

var RevenueExpensesListView  = Backbone.View.extend({
	event : invoiceEvents,
	initialize: function () {
		_.bindAll(this, "fetchAndRender");
		this.event.bind("reloadRevenueList", this.fetchAndRender)
		this.fetchAndRender();
	},
	fetchAndRender : function() {
		var self = this;
		this.collection.reset();
		this.collection.fetch({success:function(){
			self.render();
		}});
	},
	
	render: function() {
		this.$el.empty();
		var self = this;
		var number = 1;
		$('.blockable').show();
		this.collection.each(function(revenueExpenses) {
			if(revenueExpenses.get('type') == 1 && !Shop.User.isAdmin) {
				$('.blockable').hide();
				Shop.invoice.currentView = "invoiceBlocked";
			}
			this.$el.append(new RevenueExpensesView({model:revenueExpenses, number:number++}).render().el)
		}, this);
		return this;
	}
	
});

var RevenueExpensesView   = Backbone.View.extend({
	initialize: function (options) {
		this.model = options.model;
		this.number = options.number;
	},
	revenueTypes: revenueTypes,
	className: 'revenue-row',	
	events : {
		'click td' : 'showDialog',
	},
	showDialog : function(){
		$("#dialog-revenue" ).data("model", this.model);
		dialogRevenue.dialog("open");
	},
	tagName: 'table',
	revenueTypes: revenueTypes,
	template: _.template($('#revenueExpensesRow').html() ),
	render : function() {
		var params = this.model.toJSON();
		params['number'] = this.number;
		params['type_name'] = this.revenueTypes[params.type];
		this.$el.html($(this.template(params)));
		return this;
	}
	
});

var RevenueExpensesDialogView = Backbone.View.extend({
	
	el:$('#revenue-dialog-view'),
	revenueTypes: revenueTypesArr,
	template: _.template($('#revenue-dialog').html() ),
	event : invoiceEvents,

	initialize: function (options) {
		this.model = (options && options.model) ? options.model : new RevenueExpenses();
		this.exist = (options && options.model);
		$('#save-revenue').button();
		$('#delete-revenue').button();
	},
	
	reinit : function(options) {
		this.model = (options && options.model) ? options.model : new RevenueExpenses();
		this.exist = (options && options.model);
		this.dialog = options.dialog;
		this.render();
	},
	
	events : {
		"click #save-revenue" : "save",
		"click #delete-revenue" : "destroy",
		"change #revenue-dialog-form .field": "fieldChanged",
		"change #revenue_type" : "selectChanged",
	},
	
	selectChanged : function(){
		var val = $('#revenue_type').val();
		
		if(val == '1' || val == '5' || val == '4') {
			$('#revenue-revenue').closest('tr').hide();
		} else {
			$('#revenue-revenue').closest('tr').show();
		}
	},
	
	fieldChanged: function(e){
		var field = $(e.currentTarget);
		var data = {};
		data[field.attr('name')] = field.val();
	    this.model.set(data);
    },
	
	destroy : function() {
		var self = this;
		this.model.destroy( {
		    success: function (model, response) {
		        self.dialog.dialog("close");
		        self.event.trigger("reloadRevenueList");
		        $('body').trigger("loadSummary");
		    },
		    error: function (model, response) {
		        console.log("error");
		        console.log(response);
		        Shop.Popup.error("Помилка зберiгання");
		    }
		});
		
	},
	
	save : function(event, force) {
		var self = this;
		if(!force && this.model.get('type') == 1) {
			var buttons =  [
			          {text: "Зберегти",click: function() { $( this ).dialog( "close" );self.save(null,true)}},
			          {text: "Вiдмiна", click: function() { $( this ).dialog( "close" ); }}
			        ]
			Shop.Popup.warn("Увага", "Внесення виручкi закриє день.", buttons);
		} else {
			
			console.log(this.model.get("type"))
			
			if(!this.model.get("type") || this.model.get("type")== 0) {
				Shop.Popup.warn("Увага", '<strong>Форма заповнена не вiрно.</strong><br/><br/> Необхiдно вибрати "Tип"');
				return;
			}
			
			if(!this.model.get('revenue') && !this.model.get('expenses')) {
				Shop.Popup.warn("Увага", '<strong>Форма заповнена не вiрно.</strong><br/><br/> Поля "Витрати" i "Приход" не можуть бути пустими одночасно');
				return;
			}
			
			if(this.model.get('revenue') && !$.isNumeric(this.model.get('revenue'))) {
				Shop.Popup.warn("Увага", '<strong>Форма заповнена не вiрно.</strong><br/><br/> Поле "приход" мусить бути числом');
				return;
			}
			
			if(this.model.get('expenses') && !$.isNumeric(this.model.get('expenses'))) {
				Shop.Popup.warn("Увага", '<strong>Форма заповнена не вiрно.</strong><br/><br/> Поле "витрати" мусить бути числом');
				return;
			}
			
			
			
			this.model.set({'date' : $('#invoiceDate').val()});
			
			
			
			
			this.model.save(null, {
			    success: function (model, response) {
			        console.log("success");
			        self.dialog.dialog("close");
			        self.event.trigger("reloadRevenueList");
			        $('body').trigger("loadSummary");
			    },
			    error: function (model, response) {
			        console.log("error");
			        console.log(response);
			        var r = response.responseJSON;
			        Shop.Popup.errorFromException(r.exception, r.message);
			    }
			});
		}
		
	},
	
	render : function() {
		
		var params = this.model.toJSON();
		params.r_types = this.revenueTypes;
		
		if(!this.exist) {
			params.comment = '';
			params.revenue = '';
			params.expenses = '';
		}
		
		this.$el.html($(this.template(params)));
	}
});




var InvoiceListView = Backbone.View.extend({
	
	initialize: function (options) {
		this.credit = options.credit;
		this.showDate = options.showDate
		if(!this.credit) {
			if(!options.lazy) {
				this.fetchAndRender();
			}
			this.event = options.event;
			_.bindAll(this, "fetchAndRender");
			this.event.bind("reloadInvoiceList", this.fetchAndRender);
		}
		
	},
	
	fetchAndRender : function(callback) {
		var self = this;
		this.collection.fetch({reset:true, success:function(){
			self.render();
			if(callback)
				callback();
		}});
	},
	
	render: function() {
		this.$el.empty();
		var self = this;
		var number = 1;
		this.collection.each(function(invoice) {
			this.$el.append(new InvoiceView({event:this.event, credit:this.credit, excise:this.excise, model:invoice, number:number++, showDate:this.showDate}).render().el)
		}, this);
		return this;
	}
});


var InvoiceFullView = Backbone.View.extend({
	initialize: function (options) {
		this.model = options.model;
		this.excise = options.excise;
	},
	event : invoiceEvents,
	template: _.template($('#invoice-full').html()),
	className: 'full-view',
	tagName : "tr",
	
	events : {
		"click #edit-invoice" : "showInvoiceDialod",
		"click #delete-invoice" : "deleteInvoiceDialod",
		"click #fix-credit" : "fixCredit"
	},
	
	fixCredit : function() {
		var self= this;
		$.ajax({
			method:"PUT",
			url: "/rest/invoices/" + this.model.get('id') + "/credit",
			success : function(){
				 console.log("success");
				 if(Shop.invoice.currentView == 'credit') {
					 $('#credit-invoices-link').click();
				 } else {
					 self.event.trigger("reloadInvoiceList");
				 }
				 
			     $('body').trigger("loadSummary");
			},
			error : function(){
			   console.log("error");
		        if(response.responseJSON.exception) {
		        	Shop.Popup.errorFromException(response.responseJSON.exception)
		        } else {
		        	Shop.Popup.error("Помилка зберiгання");
		        }
			}
		});
	},
	
	deleteInvoiceDialod : function() {
		var self = this;
		this.model.destroy({
			success : function(){
				  self.event.trigger("reloadInvoiceList");
			       $('body').trigger("loadSummary");
			},
			error: function(){
				Shop.Popup.error("Помилка видалення");
			}
		});
	},
	
	showInvoiceDialod: function(){
		$("#dialog-form").data("model", this.model);
		
		var supplier = this.model.get("supplier");
		dialog.dialog( "option", "title",  supplier.productCategory.name+" | "+ supplier.name );
		dialog.dialog("open");
	},
	
	render : function() {
		var params = this.model.toJSON();
		params.excise = this.excise;
		this.$el.html($(this.template(params)));
		return this;
	}
	
});


var InvoiceView = Backbone.View.extend({

	initialize: function (options) {
		this.event = options.event;
		this.model = options.model;
		this.number = options.number;
		this.showDate = options.showDate;
	},
	
	className: 'invoice-row invoice-row-value',	
	events : {
		'click tr' : 'showFull',
	},
	
	showFull: function() {
		
		var fullView = this.$el.find('.full-view');
		if(fullView.length > 0) {
			fullView.remove()
		} else {
			this.$el.find('tbody').append(new InvoiceFullView({event:this.event, excise:this.model.get('excise'), model:this.model}).render().el);
		}
		
	},
	
	tagName: 'table',
	template: _.template($('#invoiceRow').html() ),
	render : function() {
		var params = this.model.toJSON();
		params['number'] = this.number;
		params['showDate'] = this.showDate;
		
		if(this.model.get('credit') && this.model.get('excise')) {
			this.$el.addClass('credit-excise-invoice')
		} else if(this.model.get('credit')){
			this.$el.addClass('credit-invoice')
		} else if(this.model.get('excise')){
			this.$el.addClass('excise-invoice')
		}
		
		this.$el.html($(this.template(params)));
		return this;
	}
	
});


var InvoiceSearchView = Backbone.View.extend({
	el:$('#invoice-search-view'),
	categories : new ProductCategories(),
	suppliers : new Suppliers({category:''}),
	template: _.template($('#search-dialog').html() ),
	
	initialize: function (options) {
		this.listenTo( this.categories, 'sync', this.render );
		this.listenTo( this.suppliers, 'sync', this.render );
		this.categories.fetch();
		this.model = new Backbone.Model();
	},
	
	events : {
		"change #select-category": "categoryChecked",
		"change #select-supplier": "supplierChecked",
		"click #search-inv-button" : "find",
		"change #select-product" : "productChecked"
	},
	
	find : function(e) {
		e.preventDefault();
		Shop.invoice.currentView = "search";
		$('#invoice-container #invoice-table-header .date-column').show();
		$('#invoice-container #invoice-table-header .number-column').hide();
		$('.lockable-header').hide();
		$('#date-block span').html("Накладні");
		
		
		$('#table-header-info').addClass('info');
		
		
		var callback = function() {
			
			if(this.model.get('selected_product')) {
					var firstRow = $('#invoice-table .invoice-row tr')[0];
					$(firstRow).click();
					$(firstRow).next('.full-view').find("tr").each(function(i,el){
						if($(el).data('id') == this.model.get('selected_product')) {
							$(el).addClass('animate')
						}
					}.bind(this))
				
			}
			
		}.bind(this);
		
		new InvoiceListView({lazy:true, event : invoiceEvents, showDate:true, 
			collection: new InvoiceSearchList({ qParams:$('#dialog-search-form').serialize()}), el:$('#invoice-table')}).fetchAndRender(callback);
		
		dialogSearch.dialog("close");
		
		
	},
	
	categoryChecked : function(el) {
		
		var category = $(el.currentTarget).val();
		this.model.set({selected_category:category});
		this.suppliers.category = category;
		this.suppliers.fetch();
		this.model.unset('selected_supplier');
		this.model.unset('supplier');
		$( "#search-inv-button" ).button({
			  disabled: true
			});
	},
	
	supplierChecked : function(el) {
		var supplier = +$(el.currentTarget).val();
		this.model.set({selected_supplier:supplier});
		this.model.set({supplier:this.suppliers.where({id:supplier})[0].toJSON()});
		this.model.unset('selected_product');
		this.model.unset('product');
		this.render();
		$( "#search-inv-button" ).button({
			  disabled: false
			});
	},
	
	productChecked : function(el) {
		var productId = +$(el.currentTarget).val();
		var product = this.model.get('supplier').products.find(function(p){return p.id == productId})
		this.model.set({selected_product:productId});
		this.model.set({product:product});
		
	},
	
	render : function() {
		var params = this.model.toJSON();
		params.categories = this.categories.toJSON();
		params.suppliers = this.suppliers.toJSON();
		this.$el.html($(this.template(params)));
		this.$el.find('.uibutton').button();
	}
	
});

var InvoiceDialogView = Backbone.View.extend({
	
	el:$('#invoice-dialog-view'),
	event : invoiceEvents,
	categories : new ProductCategories(),
	suppliers : new Suppliers({category:''}),
	template: _.template($('#invoice-dialog').html() ),
	templateProduct: _.template($('#invoice-dialog-product').html() ),
	
	initialize: function (options) {
		this.model = (options && options.model) ? options.model : new Invoice();
		this.exist = (options && options.model);
		this.localStore = new Backbone.Model();
		$('#select-product').button();
		$('#un-select-product').button();
	},
	
	reinit : function(options) {
		if (options && options.model){
			this.model = options.model;
		} else {
			this.model = new Invoice();
			this.model.set({'expenses':""});
			this.model.set({'credit' : ""});
			this.model.set({'excise' : false});
			this.model.set({'date' : $('#invoiceDate').val()});
		}
		this.localStore.clear();
		this.exist = (options && options.model);
		this.dialog = options.dialog;
		this.render();
	},
	
	events : {
		"click #excise" : "exciseToggle",
		 "change #select-category": "categoryChanged",
		 "change #select-supplier": "supplierChanged",
		 "click #select-product" : "selectProducts",
		 "click #select-all-product" : "checkAllProducts",
		 "click #un-select-product" : "renderProductsStep1",
		 "click #add-product" : "addProduct",
		 "click #save-invoice":"saveInvoice",
		 "change input.model-part" : "fieldChanged",
		 "change #invoice-document" : "uploadInvoiceDoc",
		 "click .sortable" : "sortProducts",
		 "change .invoice-product-item input" : "updateInvoiceProduct"
			 
	},
	
	updateInvoiceProduct : function(e) {
		
		var product = $(e.currentTarget).closest('.invoice-product-item'); 
			productId = product.find('input[name=product_id]').val(),
			quantity = product.find('input[name=quantity]').val(),
			price = product.find('input[name=price]').val(),
			marginPrice = product.find('input[name=margin_price]').val(),
			excisePrice = product.find('input[name=excisePrice]').val(),
			name = product.find('input[name=product_name]').val(),
			uuid = product.find('input[name=uuid]').val(),
			margin = product.find('input[name=margin]').val()
			
			console.log('gonna change ' + productId)
		_.each(this.localStore.get("supplier-products-selected"), function(el){
			
			if(el.newProduct) {
				if( uuid == el.uuid) {
					el.name = name
					el.quantity = quantity;
					el.price = price;
					el.marginPrice = marginPrice;
					el.excisePrice = excisePrice;
					el.margin = margin;
				}
				
			} else 	if(el.productId == productId) {
				console.log('changed')
					el.quantity = quantity;
					el.price = price;
					el.marginPrice = marginPrice;
					el.excisePrice = excisePrice
				}
			});
			
	},
	
	exciseToggle : function(el){
		
		if($(el.currentTarget).prop('checked') == true) {
			this.$el.find('.product-exc-price').show();
			this.$el.find('.product-def-price').hide();
		} else {
			this.$el.find('.product-exc-price').hide();
			this.$el.find('.product-def-price').show();
		}
		var self = this;
		$('#products-step-2 .invoice-product-item').each(function(i, el) {
			self.recalculateMargin(null, el);
		});
	},
	
	recalculateMargin : function(el, item) {
			var productTr = (item) ? $(item) : $(el.currentTarget).closest('.invoice-product-item');
			var isExcisePrice = productTr.find('input[name=excisePrice]').is(':visible');
			var price = (!isExcisePrice) ? +productTr.find('input[name=price]').val() :  +productTr.find('input[name=excisePrice]').val();
			var marginPrice = +productTr.find('input[name=margin_price]').val();
			var margin = +productTr.find('input[name=margin]').val();
			
			
			
			if( price ) {
				if( (el && $(el.currentTarget).attr("name") == "margin_price") || isExcisePrice || !el) {
					var newMargin = (marginPrice / price * 100) - 100;
					productTr.find('input[name=margin]').val(Shop.util.getPriceView(newMargin));
				} else {
					var newMarginPrice = price * (100 + margin) / 100;
					productTr.find('input[name=margin_price]').val(Shop.util.getPriceView(newMarginPrice));
				}
			} 
		
	},
	
	addProduct: function() {
		var newElModel = {id:'',productId:'', name:'Новий продукт',price:0,excisePrice:0, excise:this.model.get('excise'), margin:'', marginPrice:0,quantity:0, newProduct:true, uuid:Shop.util.uuid()};
		this.localStore.get("supplier-products-selected").push(newElModel)
		$('#products-step-2 table').append(this.templateProduct(newElModel));
		$('#products-step-2 table tr').last().find('input.changeble').on('keyup', this.recalculateMargin);
	},
	
	

	
	fieldChanged : function(e) {
		var obj = {}, name = $(e.currentTarget).attr("name");
		if($(e.currentTarget).attr('type') == 'checkbox') {
			obj[name] = $(e.currentTarget).is(':checked');
		} else {
			obj[name] = Shop.util.calculateString($(e.currentTarget).val());
			$(e.currentTarget).val(obj[name])
		}
		this.model.set(obj);
	},
	
	saveInvoice : function() {
			var productExtension = '';
			if(!this.exist) {
				this.model.set({'supplier':{'id':$('#select-supplier').val()}});
				
				if($('#select-supplier').val() == 0) {
					Shop.Popup.warn("Увага", '<strong>Виберiть постачальника.</strong>');
					return;
				}
			}
			
			if($('#select-supplier').val() == 81) {
				productExtension = 'cigar';
			}
			
			
			var products = [];
			var newProducts = [];
			$('#products-step-2 .invoice-product-item').each(function(number, el) {
				var quantity = $(el).find('input[name=quantity]').val();
				if(quantity !== "" && quantity != "0") {
					var product = {};
				    product.id = $(el).find('input[name=id]').val();
					product.price = $(el).find('input[name=price]').val();
					product.excisePrice = $(el).find('input[name=excisePrice]').val();
					product.marginPrice = $(el).find('input[name=margin_price]').val();
					product.quantity = $(el).find('input[name=quantity]').val();
					
					product.product = {'id' : $(el).find('input[name=product_id]').val(), 'name' : $(el).find('input[name=product_name]').val(), "marginPrice" : product.marginPrice, "price" : product.price,"excisePrice" : product.excisePrice};
					
					if('cigar' == productExtension) {
						product.product.code = $(el).find('input[name=code]').val();
						product.product.barcode = $(el).find('input[name=barcode]').val();
						product.product.type = "Cigarette"
					}
					products.push(product)
					if(!product.product.id) {
						newProducts.push(product)
					}
					
				}
			})
			
			if(products.length == 0) {
				Shop.Popup.warn("Увага", '<strong>Продукти не були вибранi.</strong><br/><br/> Перевiрте поле "Кiлькiсть" в накладнiй');
				return;
			}
			this.model.set({'products' : products});
		
			var self = this;
			this.model.save(null, {
			    success: function (model, response) {
			        self.event.trigger("reloadInvoiceList");
			        $('body').trigger("loadSummary");
			        self.dialog.dialog("close");
			        if(self.model.get('excise') && newProducts.length) {
			        	var productsToLook = [];
			        	 _.each(model.get('products'), function(product) {
					        	_.each(newProducts, function(newProduct) {
					        		if(newProduct.product.name == product.name && newProduct.marginPrice == product.marginPrice && newProduct.quantity == product.quantity) {
					        			productsToLook.push(product.product)
					        		}
						        });
					        });
			        	 
			        	 self.getExciseView().renderProducts(productsToLook); 
			        }
			       
			        
			    },
			    error: function (model, response) {
			        console.log("error");
			        if(response.responseJSON.exception) {
			        	Shop.Popup.errorFromException(response.responseJSON.exception)
			        } else {
			        	Shop.Popup.error("Помилка зберiгання");
			        }
			    }
			});
	},
	
	getExciseView : function() {
		if(!this.exciseTypeView) {
			this.exciseTypeView = new ExciseTypeView();
		}
		return this.exciseTypeView;
	},
	
	
	checkAllProducts : function(){
		var productTable = this.$el.find('#products-step-1 table');
		productTable.find('input[type=checkbox]').each(function(){
			$(this).prop("checked", true);
		})
		
	},
	
	selectProducts : function(el){
		var self = this, selected= [], productTable = this.$el.find('#products-step-1 table');
		
		if(productTable.find('input:checked').length == 0) {
			Shop.Popup.warn("Увага", "Продукти не були вибранi");
			return;
		}
		
		var selected = this.localStore.get("supplier-products-selected") ? this.localStore.get("supplier-products-selected") : [];
		var newSelected = [];
		
		productTable.find('input:checked').each(function(number, checkbox) {
			_.each(self.localStore.get('supplier-products'), function(product){
				if(product.id == $(checkbox).val()) {
					
					var invoiceProductId = $(checkbox).data('invoiceProductId');
					
					var invoiceProduct = {name : product.name, productId: product.id}
					invoiceProduct.id = (invoiceProductId) ? invoiceProductId :"";
					
					var previousSelectedProduct;
					for(var j = 0; j < selected.length; j++) {
						if(invoiceProduct.productId == selected[j].productId) {
							previousSelectedProduct = selected[j];
							break;
						}
					}
					
					if(previousSelectedProduct) {
						invoiceProduct.price = previousSelectedProduct.price;
						invoiceProduct.margin = previousSelectedProduct.margin;
						invoiceProduct.marginPrice = previousSelectedProduct.marginPrice;
						invoiceProduct.quantity = previousSelectedProduct.quantity;
						invoiceProduct.excisePrice = previousSelectedProduct.excisePrice;
						if(product.code) {
							invoiceProduct.code = Shop.util.getPriceView(+product.code);
						}
						
					} else {
						invoiceProduct.price = Shop.util.getPriceView(+product.price);
						invoiceProduct.marginPrice = Shop.util.getPriceView(+product.marginPrice);
						invoiceProduct.excisePrice = Shop.util.getPriceView(+product.excisePrice);
						if(product.code) {
							invoiceProduct.code = Shop.util.getPriceView(+product.code);
						}
						
						if(self.model.get('excise')  ) {
							if ($.isNumeric( invoiceProduct.excisePrice ) && invoiceProduct.excisePrice > 0 &&invoiceProduct.marginPrice) {
								invoiceProduct.margin = Shop.util.getPriceView(invoiceProduct.marginPrice / invoiceProduct.excisePrice * 100 - 100);
							} else {
								invoiceProduct.margin = Shop.util.getPriceView(invoiceProduct.marginPrice / invoiceProduct.price * 100 - 100);
							}
						} else if(invoiceProduct.price && invoiceProduct.marginPrice) {
							invoiceProduct.margin = Shop.util.getPriceView(invoiceProduct.marginPrice / invoiceProduct.price * 100 - 100);
						}
						
					}
					newSelected.push(invoiceProduct);
				}
			});
		});
		
		this.localStore.set({"supplier-products-selected":newSelected});
		this.renderProductsStep2();
	},
	
	renderProductsStep1 : function() {
		this.dialog.css('height','auto');
		var self = this;
		this.$el.find('#products-step-2').hide();
		this.$el.find('#products-step-1').show();
		
		if(_.isEmpty(this.localStore.get("supplier-products"))) {
			$.ajax({
				  url: '/rest/suppliers/' + self.model.get('supplier').id + '/products',
				  dataType: 'json',
				  async: false,
				  success: function(data) {
					  self.localStore.set({'supplier-products' : data})
				  }
				});
		}
		
		this.$el.find('#products-step-1 .uibutton').button();
		
		var productTable = this.$el.find('#products-step-1 table');
		productTable.empty();
		
		var allProducts = this.localStore.get("supplier-products");
		var selectedProducts = this.localStore.get("supplier-products-selected") ? this.localStore.get("supplier-products-selected") : [];
		
		_.each(allProducts, function(el){
			
			var currProduct;
			_.each(selectedProducts, function(product){
				if(product.productId == el.id) {
					currProduct = product;
				}
			});
			
			if(currProduct) {
				productTable.append('<tr><td><div class="product-checkbox" data-id="'+el.id+'"><input type="checkbox" data-invoice-product-id="'+currProduct.id+'" name="product" value="'+el.id+'" id="product-'+el.id+'" checked/><label for="product-'+el.id+'">'+el.name+'</label></td></tr>')
			} else {
				productTable.append('<tr><td><div class="product-checkbox" data-id="'+el.id+'"><input type="checkbox" name="product" value="'+el.id+'" id="product-'+el.id+'"/><label for="product-'+el.id+'">'+el.name+'</label></td></tr>')
			}
		});
    },
    
    sortProducts : function(e) {
    	var self = this, el = $(e.currentTarget);
    	
			var asc = true;
			if(el.hasClass('asc')) {
				el.removeClass('asc');
				el.addClass('desc');
				asc = false;
			} else {
				$('.sortable').removeClass('asc');
				$('.sortable').removeClass('desc');
				el.addClass('asc');
			}
			
			var field = el.data('field');
			var isInteger = el.hasClass('number');
		
        	var sortedArray = _.sortBy(self.localStore.get("supplier-products-selected"), function(o) { 
        		if(isInteger) {
        			return +o[field];
        		} else {
        			return o[field];
        		}
        	});
        	if(!asc) {
        		sortedArray = sortedArray.reverse();
        	}
        	
        	self.renderProductsStep2(sortedArray)
        	
    	
    },
	
	renderProductsStep2 : function(sortedList) {
		var self = this;
		
		
		
		this.$el.find('#products-step-2').show();
		this.$el.find('#products-step-1').hide();
		this.$el.find('#save-invoice').show();
		
		this.$el.find('#products-step-2 .uibutton').button();
		
		

		var productTable = this.$el.find('#products-step-2 table');
		
		productTable.find('tr:not(:first)').remove();
		
		var selectedProducts = sortedList ? sortedList : this.localStore.get("supplier-products-selected");
		
		_.each(selectedProducts, function(product) {
			
			if(!product.quantity) {
				product.quantity = 0;
			}
			product.excise = self.model.get('excise');
			productTable.append(self.templateProduct(product));
		});
		
		productTable.find('input').on('keydown', function(e) {
			
			var el = $(e.currentTarget).closest("tr"), newEl;
			
			if(e.which == 40) { // down
				newEl = el.next('tr');
			}
			if(e.which == 38) { // up
				newEl = el.prev('tr');
			}
			
			if(newEl){
			  e.preventDefault(); 
			  newEl.find('input[name='+$(e.currentTarget).attr("name")+']').focus();
			}
			
		});
		
		productTable.find('input.deletable').on('focusin', function(el) {
			if($(el.currentTarget).val() === "0") {
				$(el.currentTarget).val("");
			}
			
		});
		
		productTable.find('input.deletable').on('focusout', function(el) {
			if($(el.currentTarget).val() === "") {
				$(el.currentTarget).val("0");
			}
		});
		
		productTable.find('input.changeble').on('keyup', function(el) {
			self.recalculateMargin(el);
			
		});
		
		if(self.model.get('excise')) {
			productTable.find('.product-exc-price').show();
			productTable.find('.product-def-price').hide();
			
		}

	},
	
	
	uploadInvoiceDoc : function(e) {
		
		var me = this;
		 var formData = new FormData($('#add-invoice-form')[0]);
		    $.ajax({
		        url: '/rest/invoices/cigarette/file',  //Server script to process data
		        type: 'POST',
		        success: function(resp) {
		        	$('#dialog-cigarette').empty();
		        	var dialog = $( "#dialog-cigarette" ).dialog({
		        	      autoOpen: false,
		        	      height: 700,
		        	      width: '90%',
		        	      modal: true,
		        	      buttons: {
		        	    	        	        
		        	        "Відміна" : function() {
		        	        	dialog.dialog('close');
		        	        },
		        	        "Все вірно" : function(){
		        	        	
		        	        	var i = 0;
		        	        	$('#cigarette-parsed-table input:not([type=hidden])').each(function(a, val) {
		        	        		if(val.value != '') {
		        	        			$(val).removeClass("red-border3")
		        	        		} else {
		        	        			$(val).addClass("red-border3");
		        	        			i++;
		        	        		}
		        	        	});
		        	        	
		        	        	if(i > 0) {
		        	        		Shop.Popup.warn("Увага", "Не всі поля заповнені");
		        	        		return;
		        	        	}
		        	        	
		        	        	var cigarretes = [];
		        	        	$('#cigarette-parsed-table tr:not(:first)').each(function(i, val) {
		        	        		var row = $(val);
		        	        		var cig = {};
		        	        		cig.id = "";
		        	        		cig.productId = row.find('input[name="product_id"]').val();
		        	        		cig.name = row.find('input[name="product_name"]').val();
		        	        		cig.barcode = row.find('input[name="product_barcode"]').val();
		        	        		cig.marginPrice = row.find('input[name="marginPrice"]').val();
		        	        		if(me.model.get('excise')) {
		        	        			cig.excisePrice = row.find('input[name="price"]').val();
		        	        			cig.price = row.find('.old-price').html();
		        	        			cig.margin = Shop.util.getPriceView((+cig.marginPrice / +cig.excisePrice * 100) - 100);
		        	        		} else {
		        	        			cig.excisePrice = row.find('.old-excise-price').html();
		        	        			cig.price = row.find('input[name="price"]').val();
		        	        			cig.margin = Shop.util.getPriceView((+cig.marginPrice / +cig.price * 100) - 100);
		        	        		}
		        	        		
		        	        		
		        	        		
		        	        		cig.quantity = row.find('input[name="quantity"]').val();
		        	        		cig.code = row.find('input[name="code"]').val();
		        	        		cigarretes.push(cig);
		        	        	})
		        	        	
		        	        	me.localStore.set({"supplier-products-selected":cigarretes});
		        	        	me.renderProductsStep2();
		        	        	$( "#dialog-cigarette" ).dialog('close');
		        	        }
		        	      },
		        	      close: function() {
		        	      
		        	      }
		        	    });
		        	$('#dialog-cigarette').html(_.template($('#cigaretteParsedList').html())({'cig' : resp, excise:me.model.get('excise')}));
		        	
		        	
		        	
		        	var barcodes = {};
		        	$('#cigarette-parsed-table tr:not(:first)').each(function(i, val){
		        		
		        		var row = $(val);
		        		
		        		var id = row.find('input[name=product_id]').val();
		        		var oldMarginPrice = row.find('.old-margin-price').html();
		        		var marginPrice = row.find('input[name=marginPrice]').val();
		        		var barcode = row.find('input[name=product_barcode]').val();
		        		
		        		if(!barcodes[barcode]) {
		        			barcodes[barcode] = row;
		        		} else {
		        			row.addClass("warning");
		        			var parentRow = barcodes[barcode];
		        			if(!parentRow.hasClass("warning")) {
		        				parentRow.addClass("warning");
		        			}
		        		}
		        		
		        		if(id == 0) {
		        			row.addClass("new-item");
		        		} else if(oldMarginPrice != marginPrice) {
		        			row.addClass("price-changed");
		        		}
		        		
		        	});
		        	
		        	$('#cigarette-parsed-table .code input').change(function(e){
		        		
		        		
		        		$.ajax({
		        			url : '/rest/suppliers/product/code/'  + $(e.currentTarget).val(),
		        			method : "GET",
		        			success : function(resp){
		        				$(e.currentTarget).removeClass('red-border2');
		        				$(e.currentTarget).addClass('green-border2');
		        				$(e.currentTarget).data('val' ,$(e.currentTarget).val());
		        				$(e.currentTarget).data('valid', true);
		        			},
		        			error: function(a,b,c) {
		        				$(e.currentTarget).removeClass('green-border2');
		        				$(e.currentTarget).addClass('red-border2');
		        				$(e.currentTarget).data('valid', false);
		        				if(a.responseJSON && a.responseJSON.product) {
		        					Shop.Popup.error("Код [<b>"+$(e.currentTarget).val()+"</b>] для продукту [<b>"+$(e.currentTarget).closest('tr').find('.cigarette-item-name').html().replace(' ', '&nbsp;')+"</b>] не зберігся. <br/><br/> Продукт <b>"+a.responseJSON.product.name+"</b> має цей код");
		        				} else {
		        					Shop.Popup.error("Код ["+$(e.currentTarget).val()+"] не зберігся");
		        				}
		        				
		        				$(e.currentTarget).val($(e.currentTarget).data('val'));
		        			}
		        		});
		        		
		        		
		        		
		        	});
		        	$( "#dialog-cigarette" ).dialog('open');
		        	
		        	
		        },
		        error: function(err, erre) {
		        	 Shop.Popup.error("Помилка зчитування");
		        },
		        data: formData,
		        cache: false,
		        contentType: false,
		        processData: false
		    });
		
		
		
	},
	
	supplierChanged: function(e) {
		var field = $(e.currentTarget), selectedProducts = {};
		
		if(field.val() == 81) {
			$('#invoice-file').show();
		} else {
			$('#invoice-file').hide();
		}
		
		this.suppliers.each(function(el) {
			if(field.val() == el.get('id')) {
				_.each(el.get('products'), function(product){
					selectedProducts[product.id] = product
				});
				
			}
		});
		this.localStore.set({"supplier-products":selectedProducts});
		this.renderProductsStep1();
		
	},
	
	categoryChanged: function(e){
		var field = $(e.currentTarget);
		this.suppliers.category = field.val();
		
		var self = this;
		this.suppliers.fetch({success:function(collection) {
			var supplierSelect = self.$el.find('select[name=supplier]');
	 		supplierSelect.find('option:not(:first)').remove();
	 		collection.each(function(el){
	 			supplierSelect.append("<option value=" + el.get('id') + ">" + el.get('name')+"</option>");
        	});
			
		}});
	},
	
	fillCategory : function(){
		var select = $(this.el).find('select[name=category]');
		select.find('option.item').remove();
		
		if(this.exist) {
			select.attr('disabled', 'disabled');
			var productCategory = this.model.get("supplier").productCategory;
			select.append("<option value=" + productCategory.id + " selected='selected'>" + productCategory.name +"</option>");
			var supplierSelect =  $(this.el).find('select[name=supplier]')
			
			supplierSelect.attr('disabled', 'disabled');
			supplierSelect.find('option:not(:first)').remove();
			var supplier = this.model.get("supplier");
			supplierSelect.append("<option value=" + supplier.id + " selected='selected'>" + supplier.name+"</option>");
			
		} else {
			select.removeAttr('disabled');
			this.categories.each(function(el){
	    		select.append("<option class='item' value=" + el.get('id') + ">" + el.get('name')+"</option>");
	    	});
		}
		
		
	},
	
	render : function() {
		
		if(this.exist) {
			this.$el.html($(this.template(this.model.toJSON())));
			this.fillCategory();
			this.localStore.set({"supplier-products-selected":this.model.get('products')});
			
			
			
			var selectedProducts = {};
			_.each(this.model.get('supplier').products, function(product){
				selectedProducts[product.id] = product
			});
			this.localStore.set({"supplier-products":selectedProducts});
			this.renderProductsStep2();
		} else {
			var params = {'categories' : this.categories.toJSON()};
			this.$el.html($(this.template(this.model.toJSON())));
			var self = this;
			this.categories.fetch({success:function(){
				self.fillCategory();
			}});
		}
		
		return this;
		
	}
});


var ExciseTypeView = Backbone.View.extend({
	
	el:$('#excise-types-product-view'),
	template: _.template($('#productExciseTypeAndGroup').html() ),
	
	events : {
		'change .excise-type' : 'udpateExciseType',
		'change .excise-group' : 'udpateExciseGroup',
		'change .excise-code' : 'udpateExciseCode',
		'change .excise-volume' : 'udpateExciseVolume'
	},
	
	udpateExciseCode : function(e) {
		e.preventDefault();
		var productId = $(e.currentTarget).closest('.excise-product-item').find('input[name="product_id"]').val();
		var exciseCode = $(e.currentTarget).val();
		$.ajax({
			url : '/rest/suppliers/product/' + productId + '/code/' + exciseCode,
			method : "PATCH",
			success : function(resp){
				$(e.currentTarget).removeClass('red-border2');
				$(e.currentTarget).addClass('green-border2');
			},
			error: function(a,b,c) {
				$(e.currentTarget).removeClass('green-border2');
				$(e.currentTarget).addClass('red-border2');
				Shop.Popup.error("Помилка зберігання");
			}
		});
		
	},
	
	udpateExciseVolume : function(e) {
		e.preventDefault();
		var productId = $(e.currentTarget).closest('.excise-product-item').find('input[name="product_id"]').val();
		$.ajax({
			url : '/rest/suppliers/product/' + productId + '/volume',
			data : 'volume=' + $(e.currentTarget).val(),
			method : "PATCH",
			success : function(resp){
				$(e.currentTarget).removeClass('red-border2');
				$(e.currentTarget).addClass('green-border2');
			},
			error: function(a,b,c) {
				$(e.currentTarget).removeClass('green-border2');
				$(e.currentTarget).addClass('red-border2');
				Shop.Popup.error("Помилка зберігання");
			}
		});
		
		
	},

	udpateExciseType : function(e) {
		e.preventDefault();
		var productId = $(e.currentTarget).closest('.excise-product-item').find('input[name="product_id"]').val();
		var exciseTypeId = $(e.currentTarget).val();
		
		$.ajax({
			url : '/rest/suppliers/product/' + productId + '/excise/' + exciseTypeId,
			method : "PATCH",
			success : function(resp){
				$(e.currentTarget).removeClass('red-border2');
				$(e.currentTarget).addClass('green-border2');
			},
			error: function(a,b,c) {
				$(e.currentTarget).removeClass('green-border2');
				$(e.currentTarget).addClass('red-border2');
				Shop.Popup.error("Помилка зберігання");
			}
		});
		
		
	},
	
	udpateExciseGroup : function(e) {
		e.preventDefault();
		var productId = $(e.currentTarget).closest('.excise-product-item').find('input[name="product_id"]').val();
		var exciseGroupId = $(e.currentTarget).val();
		$.ajax({
			url : '/rest/product/excise/product/' + productId + '/' + exciseGroupId,
			method : "PATCH",
			success : function(resp){
				$(e.currentTarget).removeClass('red-border2');
				$(e.currentTarget).addClass('green-border2');
			},
			error: function(a,b,c) {
				$(e.currentTarget).removeClass('green-border2');
				$(e.currentTarget).addClass('red-border2');
				Shop.Popup.error("Помилка зберігання");
			}
		});
	},
	
	initialize : function(options) {
		this.groups = [];
		this.types = [];
		$.ajax({
			method:"GET",
			url: "/rest/product/excise/noproduct",
			async:false,
			success : function(response){
				this.groups = response
			}.bind(this)
		});
		$.ajax({
			method:"GET",
			url: "/rest/product/excise/type",
			async:false,
			success : function(response){
				this.types = response
			}.bind(this)
		});
	},
	
	
	renderProducts : function(products) {
		if(this.groups.length > 0 && this.types.length > 0) {
			this.$el.empty();
			var params = {};
			params.products =products;
			params.groups = this.groups;
			params.types = this.types;
			this.$el.append(this.template(params));
			
			this.$el.dialog({
			    autoOpen: true,
			    height: 400,
			    width: 600,
			    modal: true
			});
		}
		this.delegateEvents();
	}

});

var view = new InvoiceListView({event : invoiceEvents, collection: new InvoiceList(), el:$('#invoice-table')});

var creditView = new InvoiceListView({event : invoiceEvents, credit:true, showDate:true, collection: new InvoiceCreditList(), el:$('#invoice-table')});

var revenueView = new RevenueExpensesListView({collection: new RevenueExpensesList(), el:$('#expenses-table')});

Shop.invoice = {};
Shop.invoice.currentView = "invoice";

$('#invoiceDate').on('change', function(){
	Shop.invoice.currentView = "invoice";
	view.fetchAndRender();
	revenueView.fetchAndRender();
	$('#invoice-container #invoice-table-header .date-column').hide();
	$('#invoice-container #invoice-table-header .number-column').show();
	$('.lockable-header').show();
	
	$('#table-header-info').removeClass('info');
	$('#table-header-info span').empty();
	$('body').trigger("loadSummary");
});

$('#credit-invoices-link').click( function(e){
	Shop.invoice.currentView = "credit";
	e.preventDefault();
	$('#invoice-container #invoice-table-header .date-column').show();
	$('#invoice-container #invoice-table-header .number-column').hide();
	$('.lockable-header').hide();
	$('#table-header-info').removeClass('info');
	$('#table-header-info span').empty();
	$('#date-block span').html("Накл. з боргом");
	creditView.fetchAndRender();
	
});