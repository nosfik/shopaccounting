package com.shop.user.repository;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name = "authorities")
public class Authority {

	@EmbeddedId
	private UserPk userPk;
	

	public UserPk getUserPk() {
		return userPk;
	}

	public void setUserPk(UserPk userPk) {
		this.userPk = userPk;
	}



	@Embeddable
	class UserPk implements Serializable {

		@ManyToOne
		@JoinColumn(name = "username")
		private Userx user;

		@Column(length = 50)
		private String authority;

		public Userx getUser() {
			return user;
		}

		public void setUser(Userx user) {
			this.user = user;
		}

		public String getAuthority() {
			return authority;
		}

		public void setAuthority(String authority) {
			this.authority = authority;
		}

	}

}
