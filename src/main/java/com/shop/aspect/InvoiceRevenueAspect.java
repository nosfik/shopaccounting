package com.shop.aspect;

import java.sql.Date;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.shop.base.exception.AccessDeniedException;
import com.shop.base.exception.BalanceNotFoundException;
import com.shop.invoice.repository.Invoice;
import com.shop.invoice.repository.InvoiceDAO;
import com.shop.invoice.service.BalanceService;
import com.shop.revenue.repository.RevenueExpenses;
import com.shop.revenue.repository.RevenueExpensesDao;

@Aspect
@Component
public class InvoiceRevenueAspect {
	private final Logger log = LoggerFactory.getLogger(InvoiceRevenueAspect.class);

	@Autowired
	private RevenueExpensesDao revenueDao;

	@Autowired
	private InvoiceDAO invoiceDao;

	@Autowired
	private BalanceService balanceService;

	@Around("execution(* com.shop.revenue.service.RevenueExpensesService.saveRevenue(..)) && args(revenueExpenses)")
	public RevenueExpenses saveRevenue(ProceedingJoinPoint pjp, RevenueExpenses revenueExpenses) throws Throwable {
		log.info("save revenue");
		checkBalance(revenueExpenses.getDate());
		checkAccess(revenueExpenses.getDate());
		return (RevenueExpenses) pjp.proceed();
	}
	
	@Around("execution(* com.shop.revenue.service.RevenueExpensesService.update(..)) && args(revenueExpenses)")
	public RevenueExpenses updateRevenue(ProceedingJoinPoint pjp, RevenueExpenses revenueExpenses) throws Throwable {
		log.info("update revenue");
		checkBalance(revenueExpenses.getDate());
		checkAccess(revenueExpenses.getDate());
		return (RevenueExpenses) pjp.proceed();
	}

	@Around("execution(* com.shop.revenue.service.RevenueExpensesService.delete(..)) && args(id)")
	public boolean deleteRevenue(ProceedingJoinPoint pjp, Long id) throws Throwable {
		log.info("delete revenue");
		checkAccess(revenueDao.findOne(id).getDate());
		return (boolean) pjp.proceed();
	}

	@Around("execution(* com.shop.invoice.service.InvoiceService.save(..)) && args(invoice)")
	public Invoice saveInvoice(ProceedingJoinPoint pjp, Invoice invoice) throws Throwable {
		log.info("save invoice");
		checkBalance(invoice.getDate());
		checkAccess(invoice.getDate());
		return (Invoice) pjp.proceed();
	}
	
	@Around("execution(* com.shop.invoice.service.InvoiceService.update(..)) && args(invoice)")
	public Invoice updateInvoice(ProceedingJoinPoint pjp, Invoice invoice) throws Throwable {
		log.info("update invoice");
		checkBalance(invoice.getDate());
		checkAccess(invoice.getDate());
		return (Invoice) pjp.proceed();
	}

	@Around("execution(* com.shop.invoice.service.InvoiceService.deleteInvoice(..)) && args(id)")
	public boolean deleteInvoiceInvoice(ProceedingJoinPoint pjp, Long id) throws Throwable {
		log.info("delete invoice");
		checkAccess(invoiceDao.findOne(id).getDate());
		return (boolean) pjp.proceed();
	}

	private void checkBalance(Date date) throws BalanceNotFoundException {
		balanceService.getBalance(date);
	}

	private void checkAccess(Date date) throws AccessDeniedException {
		RevenueExpenses procced = revenueDao.getProceeds(date);
		if (procced != null) {
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			if (auth != null) {
				for (GrantedAuthority authority : auth.getAuthorities()) {
					if ("ROLE_ADMIN".equals(authority.getAuthority())) {
						return;
					}
				}
			}
			throw new AccessDeniedException("You are not allowed to change data for blocked day");
		}
	}
}
