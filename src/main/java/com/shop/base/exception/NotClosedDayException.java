package com.shop.base.exception;

import java.sql.Date;
import java.util.List;

public class NotClosedDayException extends ShopException {
	private static final long serialVersionUID = 1L;
	
	public static NotClosedDayException build(List<Date> dates) {
		
		StringBuilder sb = new StringBuilder();
		
		for(Date date : dates) {
			sb.append(date);
		}
		
		return new NotClosedDayException(sb.toString());
		
	}
	
	public NotClosedDayException(String message) {
		super(message);
	}
	
}
