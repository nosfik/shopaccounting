package com.shop.base.service;

import com.shop.base.exception.AccessDeniedException;

public interface GenericRestService<T, PK> {
	
	Iterable<T> getAll();
	
	T get(PK pk);
	
	boolean delete(PK pk) throws AccessDeniedException;
	
	T save(T t);

}
