package com.shop.base.navigation;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;

@Service
public class NavigationService {
	
	private List<MenuItem> navigation;
	
	@PostConstruct
	public void init() throws Exception {
		
		String  jsonString = "["
				+ "{'url':'/invoice', 'name':'поставки'},"
				+ "{'url':'/supplier', 'name':'постачальники', "
					+ "'subMenu':["
						+ "{'url':'/supplier', 'name':'Постачальники'},"
						+ "{'url':'/supplier/products/category', 'name':'Категорії продуктiв'},"
						+ "{'url':'/supplier/products/excise', 'name':'Акциз'}"
					+ "]},"
				+ "{'url':'/invoice/print', 'name':'видаткова накладна'},"
				+ "{'url':'/schedule', 'name':'графік'},"
				+ "{'url':'/statistic', 'name':'статистика', "
					+ "'subMenu':["
						+ "{'url':'/statistic', 'name':'Статистика'},"
						+ "{'url':'/statistic/salary', 'name':'Зарплата'},"
						+ "{'url':'/statistic/excise', 'name':'Акциз'}"
					+ "]}"
				+ "]";
		
		jsonString = jsonString.replace("'", "\"");
		
		navigation = new ObjectMapper().readValue(jsonString,
				TypeFactory.defaultInstance().constructCollectionType(List.class,  
						MenuItem.class));
	}

	public List<MenuItem> getNavigation() {
		return navigation;
	}
	
}
