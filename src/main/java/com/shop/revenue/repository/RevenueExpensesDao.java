package com.shop.revenue.repository;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.shop.revenue.repository.model.RevenueExpensesSumByType;

public interface RevenueExpensesDao extends CrudRepository<RevenueExpenses, Long> {
	List<RevenueExpenses> findByDateOrderByIdAsc(Date date);

	@Query("FROM RevenueExpenses r WHERE r.date=?1 and r.type = 1")
	RevenueExpenses getProceeds(Date date);

	@Query(value = "select b.date from revenue_expenses as a RIGHT JOIN (select date from revenue_expenses where date < :to group by date) as b "
			+ "ON a.date=b.date AND a.type=1 where a.id is null", nativeQuery = true)
	List<Date> getDateWithoutProceeds(@Param("to") Date to);

	@Query("Select r.date, SUM(expenses) FROM RevenueExpenses r WHERE r.date between :from and :to and (r.type = 1 or r.type = 5) group by date")
	List<Object[]> getProceeds(@Param("from") Date from, @Param("to") Date to);

	@Query("select new com.shop.revenue.repository.model.RevenueExpensesSumByType( SUM(revenue), SUM(expenses), type) from RevenueExpenses WHERE (type <> 1 and type <> 5) and date between :from and :to group by type")
	List<RevenueExpensesSumByType> getOtherRevenueExpenses(@Param("from") Date from, @Param("to") Date to);

	@Query("SELECT SUM(revenue) FROM RevenueExpenses WHERE date = :date")
	BigDecimal getDayRevenue(@Param("date") Date date);

	@Query("SELECT  SUM(expenses) FROM RevenueExpenses WHERE date = :date")
	BigDecimal getDayExpenses(@Param("date") Date date);

	@Query("Select SUM(expenses) FROM RevenueExpenses WHERE date between :from and :to and (type = 1 or type = 5)")
	BigDecimal getProceedSum(@Param("from") Date from, @Param("to") Date to);

	@Query("FROM RevenueExpenses r WHERE r.date=:date and r.type = 5")
	RevenueExpenses getFirstProceed(@Param("date") Date date);
	
	@Query("Select concat(r.date, '_', r.type), expenses FROM RevenueExpenses r WHERE r.date between :from and :to and (r.type = 1 or r.type = 5)")
	List<Object[]> getProceedsApart(@Param("from") Date from, @Param("to") Date to);

}