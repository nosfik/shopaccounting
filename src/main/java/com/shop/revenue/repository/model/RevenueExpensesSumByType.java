package com.shop.revenue.repository.model;

import java.math.BigDecimal;

public class RevenueExpensesSumByType {
	
	private BigDecimal revenue;
	private BigDecimal expenses;
	private Integer type;
	
	public RevenueExpensesSumByType(){}

	public RevenueExpensesSumByType(BigDecimal revenue, BigDecimal expenses, Integer type) {
		this.revenue = revenue;
		this.expenses = expenses;
		this.type = type;
	}



	public BigDecimal getRevenue() {
		return revenue;
	}
	public void setRevenue(BigDecimal revenue) {
		this.revenue = revenue;
	}
	public BigDecimal getExpenses() {
		return expenses;
	}
	public void setExpenses(BigDecimal expenses) {
		this.expenses = expenses;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	
}
