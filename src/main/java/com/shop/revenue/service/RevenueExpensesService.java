package com.shop.revenue.service;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import com.shop.base.exception.AccessDeniedException;
import com.shop.base.exception.BalanceNotFoundException;
import com.shop.base.exception.ShopException;
import com.shop.revenue.repository.RevenueExpenses;

public interface RevenueExpensesService  {
	
	String TYPE_PROCEED = "1";
	String TYPE_WRITE_OFF = "2";
	String TYPE_REVALUATION = "3";
	String TYPE_OWN_EXPENSES = "4";
	String TYPE_PROCEED_MORNING = "5";
	String TYPE_OTHER = "10";
	
	List<RevenueExpenses> getAllByDate(Date date);

	Map<String, String> getRevenueType();
	
	RevenueExpenses saveRevenue(RevenueExpenses t) throws ShopException, AccessDeniedException;
	
	boolean isDayBlocked(LocalDate date);
	
	boolean delete(Long id) throws AccessDeniedException, BalanceNotFoundException;
	
	RevenueExpenses get(Long id);
	
	RevenueExpenses update(RevenueExpenses re) throws ShopException, AccessDeniedException;

}
