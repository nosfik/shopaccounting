package com.shop.revenue.service.impl;

import java.sql.Date;
import java.time.LocalDate;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shop.base.exception.AccessDeniedException;
import com.shop.base.exception.BalanceNotFoundException;
import com.shop.base.exception.NotClosedDayException;
import com.shop.base.exception.ShopException;
import com.shop.invoice.service.BalanceService;
import com.shop.revenue.repository.RevenueExpenses;
import com.shop.revenue.repository.RevenueExpensesDao;
import com.shop.revenue.service.RevenueExpensesService;

@Service
public class RevenueExpensesServiceImpl implements RevenueExpensesService {

	private final Logger log = LoggerFactory.getLogger(RevenueExpensesServiceImpl.class);

	@Autowired
	private RevenueExpensesDao repository;

	@Autowired
	private BalanceService balanceService;

	private static Map<String, String> types = new LinkedHashMap<>();

	static {
		types.put(TYPE_PROCEED_MORNING, "Виручка ранішня");
		types.put(TYPE_PROCEED, "Виручка вечірня");
		types.put(TYPE_WRITE_OFF, "Списано");
		types.put(TYPE_REVALUATION, "Переоцiнка");
		types.put(TYPE_OWN_EXPENSES, "Власнi витрати");
		types.put(TYPE_OTHER, "Iнше");
		types = Collections.unmodifiableMap(types);
	}

	@Override
	public List<RevenueExpenses> getAllByDate(Date date) {
		return repository.findByDateOrderByIdAsc(date);
	}

	@Override
	@Transactional
	public boolean delete(Long id) throws AccessDeniedException, BalanceNotFoundException {
		RevenueExpenses re = get(id);
		repository.delete(id);
		balanceService.calculateBalance(re.getDate(), balanceService.getLastCalculatedBalanceDate());
		return true;
	}
	
	
	@Override
	@Transactional
	public RevenueExpenses update(RevenueExpenses re) throws BalanceNotFoundException {
		RevenueExpenses old = repository.findOne(re.getId());
		old.setExpenses(re.getExpenses());
		old.setRevenue(re.getRevenue());
		old.setType(re.getType());
		old.setComment(re.getComment());
		balanceService.calculateBalance(old.getDate(), balanceService.getLastCalculatedBalanceDate());
		return re;
	}

	@Override
	@Transactional
	public RevenueExpenses saveRevenue(RevenueExpenses revenueExpenses) throws ShopException, AccessDeniedException {
		
		if(revenueExpenses.getId() != null && revenueExpenses.getId() > 0) {
			throw new IllegalArgumentException("shop.revenue.save.error.not.new");
		}
		
		if (revenueExpenses.getType() == 1) {
			List<Date> dates = repository.getDateWithoutProceeds(revenueExpenses.getDate());
			if (dates.size() > 0) {
				throw NotClosedDayException.build(dates);
			}
			// not used for now
			if (repository.getProceeds(revenueExpenses.getDate()) != null) {
				throw new ShopException("shop.second.proceed.already.saved");
			}

			if (repository.getFirstProceed(revenueExpenses.getDate()) == null) {
				throw new ShopException("shop.first.proceed.require");
			}
		} else if (revenueExpenses.getType() == 5) {
			if (repository.getFirstProceed(revenueExpenses.getDate()) != null) {
				throw new ShopException("shop.first.proceed.already.saved");
			}
		}

		RevenueExpenses re = repository.save(revenueExpenses);
		balanceService.calculateBalance(revenueExpenses.getDate(), balanceService.getLastCalculatedBalanceDate());
		return re;
	}

	@Override
	public Map<String, String> getRevenueType() {
		return types;
	}

	@Override
	public boolean isDayBlocked(LocalDate date) {
		return repository.getProceeds(Date.valueOf(date)) != null;
	}

	@Override
	public RevenueExpenses get(Long id) {
		return repository.findOne(id);
	}

}
