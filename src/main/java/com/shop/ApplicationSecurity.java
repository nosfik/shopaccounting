package com.shop;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@EnableGlobalMethodSecurity(prePostEnabled = true)
@EnableWebSecurity
@Configuration
public class ApplicationSecurity extends WebSecurityConfigurerAdapter{
	
		@Autowired
		private DataSource dataSource;

		@Override
		protected void configure(HttpSecurity http) throws Exception {
			http.csrf().disable().authorizeRequests().anyRequest().permitAll().and().formLogin().loginPage("/login")
					.failureUrl("/login?error").defaultSuccessUrl("/").permitAll().and().logout().logoutSuccessUrl("/")
					.permitAll();
		}

		@Override
		public void configure(AuthenticationManagerBuilder auth) throws Exception {
			auth.jdbcAuthentication().dataSource(this.dataSource)
					.usersByUsernameQuery("select username,password, enabled from users where username=?")
					.authoritiesByUsernameQuery("select username, authority from authorities where username=?");
		}
		/*
		 * @Autowired public void configureGlobal(AuthenticationManagerBuilder
		 * auth) throws Exception {
		 * auth.inMemoryAuthentication().withUser("user").password("password").
		 * roles("USER"); }
		 */

}
