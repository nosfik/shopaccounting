package com.shop.controller.resources;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.shop.base.exception.AccessDeniedException;
import com.shop.schedule.repository.Seller;
import com.shop.schedule.service.SellerScheduleService;

@RestController
@RequestMapping("/rest/schedule/seller")
public class SellerController {
	
	@Autowired
	private SellerScheduleService service;
	
	@RequestMapping(method=RequestMethod.GET)
	public Iterable<Seller> getSellerAll() {
		return service.getAllSellers();
	}
	
	@RequestMapping(value="{id}", method=RequestMethod.DELETE)
	public ResponseEntity<Boolean> deleteSeller(@PathVariable long id) throws AccessDeniedException {
		service.deleteSeller(id);
		return new ResponseEntity<Boolean>(true, HttpStatus.OK);
	}
	
	@RequestMapping( method=RequestMethod.POST)
	public Seller save (@RequestBody @Valid Seller seller) {
		return service.saveSeller(seller);
	}
	
	@RequestMapping(value="{id}", method=RequestMethod.PUT)
	public Seller update (@RequestBody @Valid Seller seller) {
		return service.saveSeller(seller);
	}
	
}
