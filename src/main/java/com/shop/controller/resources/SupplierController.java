package com.shop.controller.resources;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Date;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.shop.supplier.product.Product;
import com.shop.supplier.repository.Supplier;
import com.shop.supplier.service.SupplierService;

@RestController
@RequestMapping("/rest/suppliers")
public class SupplierController {
	
	@Autowired
	private SupplierService supplierService;
	
	@RequestMapping(method=RequestMethod.GET)
	public Iterable<Supplier> getAll() {
		return supplierService.getSuppliers();
	}
	
	@RequestMapping(value="/unusedproducts/{days}",method=RequestMethod.GET)
	public Set<BigInteger> getSuppliersWithUnusedProducts(@PathVariable long days) {
		return supplierService.getUnusedProductIds(Date.valueOf(LocalDate.now().minusDays(days)));
	}
	
	@RequestMapping(value="/{supplierId}", method=RequestMethod.GET)
	public Supplier getSupplier(@PathVariable long supplierId){
		return supplierService.getSupplier(supplierId);
	}
	
	@RequestMapping(value="/category/{id}", method=RequestMethod.GET)
	public Iterable<Supplier> getAllByCategory(@PathVariable long id){
		return supplierService.getSuppliersByCategory(id);
	}
	
	@RequestMapping(value="/product/{productId}", method=RequestMethod.DELETE)
	public ResponseEntity<Boolean> deleteProduct(@PathVariable long productId) {
		return new ResponseEntity<Boolean>(supplierService.deleteProduct(productId), HttpStatus.OK);
	}
	
	@RequestMapping(value="/product", method=RequestMethod.POST)
	public Product addProduct(@RequestBody @Valid Product product) {
		return supplierService.addProduct(product);
	}
	
	@RequestMapping(value="/product/{productId}", method=RequestMethod.PUT)
	public Product updateProduct(@RequestBody @Valid Product product, @PathVariable long productId) {
		return supplierService.updateProduct(product, productId);
	}

	@RequestMapping(value="/product/{productId}/move/{supplierId}", method=RequestMethod.PUT)
	public Product moveProduct(@PathVariable long productId, @PathVariable long supplierId) {
		return supplierService.moveProduct(productId, supplierId, false);
	}

	@RequestMapping(value="/product/{productId}/copy/{supplierId}", method=RequestMethod.PUT)
	public Product copyProduct(@PathVariable long productId, @PathVariable long supplierId) {
		return supplierService.moveProduct(productId, supplierId, true);
	}
	
	@RequestMapping(value="{supplierId}/products", method=RequestMethod.GET)
	public Iterable<Product> getSupplierProducts(@PathVariable long supplierId) {
		return supplierService.getSupplier(supplierId).getProducts();
	}
	
	
	@RequestMapping(value="/{supplierId}", method=RequestMethod.DELETE)
	public ResponseEntity<Boolean> deleteSupplier(@PathVariable long supplierId) {
		return new ResponseEntity<Boolean>(supplierService.deleteSupplier(supplierId), HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public Supplier saveSupplier (@RequestBody @Valid Supplier supplier) {
		return supplierService.save(supplier);
	}
	
	@RequestMapping(value="/{supplierId}", method=RequestMethod.PUT)
	public Supplier updateSupplier (@RequestBody @Valid Supplier supplier) {
		return supplierService.save(supplier);
	}
	
	@RequestMapping(value="/product/{id}/code/{code}", method=RequestMethod.PATCH)
	public ResponseEntity<Map> updateProductCode (@PathVariable long id, @PathVariable String code) {
		Product product = supplierService.updateProductCode(code, id);
		Map<String, Object> response = new HashMap<String, Object>();
		if(product != null) {
			response.put("success", false);
			response.put("product", product);
			return new ResponseEntity<Map>(response, HttpStatus.BAD_REQUEST);
		} else {
			response.put("success", true);
			return new ResponseEntity<Map>(response, HttpStatus.OK);
		}
	}
	
	@RequestMapping(value="/product/{id}/volume", method=RequestMethod.PATCH, params={"volume"})
	public ResponseEntity<Map> updateProductVolume (HttpServletRequest request, @PathVariable long id) {
		Map<String, Object> response = new HashMap<String, Object>();
		try {
			supplierService.updateProductVolume(id, new BigDecimal(request.getParameter("volume")));
			response.put("success", true);
			return new ResponseEntity<Map>(response, HttpStatus.OK);
		} catch (Exception e) {
			response.put("success", false);
			return new ResponseEntity<Map>(response, HttpStatus.BAD_REQUEST);
		}
	}
	
	@RequestMapping(value="/product/{id}/excise/{exciseId}", method=RequestMethod.PATCH)
	public ResponseEntity<Map> updateProductExciseType (HttpServletRequest request, @PathVariable long id,  @PathVariable long exciseId) {
		Map<String, Object> response = new HashMap<String, Object>();
		try {
			supplierService.updateProductExciseType(id, exciseId);
			response.put("success", true);
			return new ResponseEntity<Map>(response, HttpStatus.OK);
		} catch (Exception e) {
			response.put("success", false);
			return new ResponseEntity<Map>(response, HttpStatus.BAD_REQUEST);
		}
	}
	
	@RequestMapping(value="/product/code/{code}", method=RequestMethod.GET)
	public ResponseEntity<Map> checkProductCode (@PathVariable String code, @RequestParam(name="productId", required=false) Long productId) {
		Product product = supplierService.getProductByCode(code);
		Map<String, Object> response = new HashMap<String, Object>();
		if(product != null && product.getId() != productId) {
			response.put("success", false);
			response.put("product", product);
			return new ResponseEntity<Map>(response, HttpStatus.BAD_REQUEST);
		} else {
			response.put("success", true);
			return new ResponseEntity<Map>(response, HttpStatus.OK);
		}
	}
}
