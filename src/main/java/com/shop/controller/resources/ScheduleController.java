package com.shop.controller.resources;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.shop.schedule.repository.SellerSchedule;
import com.shop.schedule.service.SellerScheduleService;

@RestController
@RequestMapping("/rest/schedule")
public class ScheduleController {
	
	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyyMMdd");
	
	@Autowired
	private SellerScheduleService service;
	
	@RequestMapping(value="/{year}/{month}", method=RequestMethod.GET)
	public Iterable<SellerSchedule> getAll(@PathVariable int year, @PathVariable int month) {
		return service.getAllSchedules(year, month);
	}
	
	@RequestMapping(value="/{date}",method=RequestMethod.GET)
	public SellerSchedule get(@PathVariable("date") String date) throws ParseException {
		return service.get(new java.sql.Date(DATE_FORMAT.parse(date).getTime()));
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public SellerSchedule save (@RequestBody @Valid SellerSchedule schedule) {
		return service.saveSchedule(schedule);
	}
	
	@RequestMapping(method=RequestMethod.PUT)
	public SellerSchedule update (@RequestBody @Valid SellerSchedule schedule) {
		return service.saveSchedule(schedule);
	}

}
