package com.shop.controller.resources;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.shop.base.exception.AccessDeniedException;
import com.shop.base.exception.BalanceNotFoundException;
import com.shop.base.exception.ShopException;
import com.shop.revenue.repository.RevenueExpenses;
import com.shop.revenue.service.RevenueExpensesService;

@RestController
@RequestMapping("/rest/revenue")
public class RevenueExpensesController {
	
	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyyMMdd");
	
	
	@Autowired
	private RevenueExpensesService service;
	
	@RequestMapping(value="/date/{date}", method=RequestMethod.GET)
	public List<RevenueExpenses> getAll(@PathVariable("date") String date) throws ParseException {
		return service.getAllByDate( new java.sql.Date(DATE_FORMAT.parse(date).getTime()));
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public RevenueExpenses get(@PathVariable long id){
		return service.get(id);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<Boolean> delete(@PathVariable long id) throws BalanceNotFoundException, AccessDeniedException {
		return new ResponseEntity<Boolean>(service.delete(id), HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public RevenueExpenses save (@RequestBody @Valid RevenueExpenses revenueExpenses) throws ShopException, AccessDeniedException {
		return service.saveRevenue(revenueExpenses);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.PUT)
	public RevenueExpenses update (@RequestBody @Valid RevenueExpenses revenueExpenses) throws ShopException, AccessDeniedException {
		return service.update(revenueExpenses);
	}

}
