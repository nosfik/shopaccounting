package com.shop.controller.resources;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.shop.invoice.model.SummaryDto;
import com.shop.invoice.repository.Balance;
import com.shop.invoice.repository.BalancePK;
import com.shop.invoice.service.BalanceService;

@RestController
@RequestMapping("/rest/balance")
public class BalanceController {
	
	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyyMMdd");
	
	
	
	@Autowired
	private BalanceService balanceService;
	
	@RequestMapping(value="/{date}/{calculated}", method=RequestMethod.GET)
	public Balance get(@PathVariable("date") String date, @PathVariable("calculated") Boolean calculated) throws ParseException {
		return balanceService.get(new BalancePK(new java.sql.Date(DATE_FORMAT.parse(date).getTime()), calculated));
	}
	
	@RequestMapping(value="/summary/{date}", method=RequestMethod.GET)
	public SummaryDto getSummary(@PathVariable("date") String date) throws ParseException {
		return balanceService.getSummary(new java.sql.Date(DATE_FORMAT.parse(date).getTime()));
	}
	
	@RequestMapping(value="/{date}", method=RequestMethod.POST)
	public Balance save (@PathVariable("date") String date, @RequestParam("balance") String balance) throws ParseException {
		BalancePK balancePK = new BalancePK(new java.sql.Date(DATE_FORMAT.parse(date).getTime()), false);
		return balanceService.save(new Balance(balancePK, new BigDecimal(balance)));
	}
	
	@RequestMapping(value="/{date}", method=RequestMethod.PUT)
	public Balance update (@PathVariable("date") String date, @RequestParam("balance") String balance) throws ParseException  {
		BalancePK balancePK = new BalancePK(new java.sql.Date(DATE_FORMAT.parse(date).getTime()), false);
		return balanceService.save(new Balance(balancePK, new BigDecimal(balance)));
	}

}
