package com.shop.controller.resources;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.shop.statistic.GeneralStatisticService;
import com.shop.statistic.SalaryService;
import com.shop.statistic.model.ExciseDataDecorator;
import com.shop.statistic.model.SalaryData;
import com.shop.statistic.model.StatisticDataContainer;

@RestController
@RequestMapping("/rest/statistic")
public class StatisticController {
	
	private static ThreadLocal<SimpleDateFormat> DATE_FORMAT = new ThreadLocal<SimpleDateFormat>(){
		  @Override
	        protected SimpleDateFormat initialValue()
	        {
	            return new SimpleDateFormat("yyyyMMdd");
	        }
	};
	
	@Autowired
	private GeneralStatisticService service;
	
	@Autowired
	private SalaryService salaryService;
	
	@RequestMapping(value="/general/proceed/{from}/{to}",method=RequestMethod.GET)
	public StatisticDataContainer getProceeds(@PathVariable String from, @PathVariable String to) throws ParseException {
		return service.getProceeds(new java.sql.Date(DATE_FORMAT.get().parse(from).getTime()), new java.sql.Date(DATE_FORMAT.get().parse(to).getTime()));
	}
	
	@RequestMapping(value="/general/invoice/revenue/{from}/{to}",method=RequestMethod.GET)
	public StatisticDataContainer getRevenue(@PathVariable String from, @PathVariable String to) throws ParseException {
		return service.getInvoiceRevenues(new java.sql.Date(DATE_FORMAT.get().parse(from).getTime()), new java.sql.Date(DATE_FORMAT.get().parse(to).getTime()));
	}
	
	@RequestMapping(value="/general/invoice/supplier/{from}/{to}",method=RequestMethod.GET)
	public StatisticDataContainer getTopSuppliers(@PathVariable String from, @PathVariable String to) throws ParseException {
		return service.getTopSuppliers(new java.sql.Date(DATE_FORMAT.get().parse(from).getTime()), new java.sql.Date(DATE_FORMAT.get().parse(to).getTime()));
	}
	
	@RequestMapping(value="/general/invoice/supplier/{supplierId}/{from}/{to}",method=RequestMethod.GET)
	public StatisticDataContainer getSupplierInvoiceRevenue(@PathVariable long supplierId, @PathVariable String from, @PathVariable String to) throws ParseException {
		return service.getSupplierInvoiceRevenue(new java.sql.Date(DATE_FORMAT.get().parse(from).getTime()), new java.sql.Date(DATE_FORMAT.get().parse(to).getTime()), supplierId);
	}
	
	@RequestMapping(value="/general/other/{from}/{to}",method=RequestMethod.GET)
	public StatisticDataContainer getOtherRevenueExpenses(@PathVariable String from, @PathVariable String to) throws ParseException {
		return service.getOtherRevenueExpenses(new java.sql.Date(DATE_FORMAT.get().parse(from).getTime()), new java.sql.Date(DATE_FORMAT.get().parse(to).getTime()));
	}
	
	@RequestMapping(value="/salary/shift/{year}/{month}",method=RequestMethod.GET)
	public StatisticDataContainer getNumberOfShifts(@PathVariable int year, @PathVariable int month) throws ParseException {
		return salaryService.getSellerWorkDays(year, month);
	}
	
	@RequestMapping(value="/salary/{year}/{month}",method=RequestMethod.GET)
	public SalaryData getSalaryData(@PathVariable int year, @PathVariable int month, @RequestParam String percent) throws ParseException {
		return salaryService.getSalaryData(year, month, percent);
	}
	
	@RequestMapping(value="/excise/volume/{from}/{to}",method=RequestMethod.GET)
	public ExciseDataDecorator getExciseVolumeStat(@PathVariable String from, @PathVariable String to) throws ParseException {
		return service.getInvoiceExciseProducts(new java.sql.Date(DATE_FORMAT.get().parse(from).getTime()), new java.sql.Date(DATE_FORMAT.get().parse(to).getTime()));
	}
	
	@RequestMapping(value="/general/seller/revenue/{from}/{to}",method=RequestMethod.GET)
	public String getSellerRevenue(@PathVariable String from, @PathVariable String to) throws ParseException {
		return salaryService.getSellerRevenue(new java.sql.Date(DATE_FORMAT.get().parse(from).getTime()), new java.sql.Date(DATE_FORMAT.get().parse(to).getTime())).toString();
	}

}
