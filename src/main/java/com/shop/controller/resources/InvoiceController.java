package com.shop.controller.resources;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.shop.base.exception.AccessDeniedException;
import com.shop.base.exception.BalanceNotFoundException;
import com.shop.invoice.model.InvoiceDto;
import com.shop.invoice.repository.Invoice;
import com.shop.invoice.service.InvoiceService;
import com.shop.invoice.service.impl.PdfInvoiceParser;
import com.shop.supplier.product.CigaretteDTO;

@RestController
@RequestMapping("/rest/invoices")
public class InvoiceController {
	
	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyyMMdd");
	
	
	@Autowired
	private InvoiceService invoiceService;
	
	@Autowired
	private PdfInvoiceParser pdfInvoiceParser;
	
	
	
	@RequestMapping(value="/search", method=RequestMethod.GET)
	public List<InvoiceDto> findInvoices(@RequestParam("supplier") long supplierId, @RequestParam(value="product", required=false) long productId, @RequestParam("limit") int limit) throws ParseException {
		return invoiceService.getBySupplierOrProduct(supplierId, productId, limit);
	}
	
	@RequestMapping(value="/credit", method=RequestMethod.GET)
	public List<InvoiceDto> getAllCredit() throws ParseException {
		return invoiceService.getCredits();
	}
	
	@RequestMapping(value="/date/{date}", method=RequestMethod.GET)
	public List<InvoiceDto> getAll(@PathVariable("date") String date) throws ParseException {
		return invoiceService.getByDate(new java.sql.Date(DATE_FORMAT.parse(date).getTime()));
	}
	
	@RequestMapping(value="/{invoiceId}", method=RequestMethod.GET)
	public Invoice getInvoice(@PathVariable long invoiceId){
		return invoiceService.getInvoice(invoiceId);
	}
	
	@RequestMapping(value="/{invoiceId}", method=RequestMethod.DELETE)
	public ResponseEntity<Boolean> deleteInvoice(@PathVariable long invoiceId) throws BalanceNotFoundException, AccessDeniedException {
		return new ResponseEntity<Boolean>(invoiceService.deleteInvoice(invoiceId), HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public Invoice save (@RequestBody @Valid Invoice invoice) throws BalanceNotFoundException, AccessDeniedException {
		invoice.setCreatedDate(new Date());
		return invoiceService.save(invoice);
	}
	
	@RequestMapping(value="/{invoiceId}", method=RequestMethod.PUT)
	public Invoice update (@RequestBody @Valid Invoice invoice) throws BalanceNotFoundException, AccessDeniedException {
		return invoiceService.update(invoice);
	}
	
	@RequestMapping(value="/{invoiceId}/credit", method=RequestMethod.PUT)
	public ResponseEntity<Boolean> fixCredit (@PathVariable long invoiceId) throws BalanceNotFoundException, AccessDeniedException {
		return new ResponseEntity<Boolean>(invoiceService.fixCredit(invoiceId), HttpStatus.OK);
	}
	
	
	@RequestMapping(value="/cigarette/file", method=RequestMethod.POST)
	public List<CigaretteDTO> uploadFile(@RequestParam("file") MultipartFile multipartFile) throws BalanceNotFoundException, AccessDeniedException, IOException {
		return pdfInvoiceParser.parsePDF(multipartFile.getBytes());
	}

}
