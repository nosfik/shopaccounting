package com.shop.controller.resources;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.shop.base.exception.AccessDeniedException;
import com.shop.excise.repository.ExciseGroup;
import com.shop.excise.repository.ExciseType;
import com.shop.excise.service.ExciseGroupService;
import com.shop.excise.service.ExciseTypeService;

@RestController
@RequestMapping("/rest/product/excise")
public class ExciseController {
	
	@Autowired
	private ExciseGroupService service;
	
	@Autowired
	private ExciseTypeService serviceType;
	
	@RequestMapping(method=RequestMethod.GET)
	public Iterable<ExciseGroup> getAll() {
		return service.findAll();
	}
	
	@RequestMapping(value="/noproduct",method=RequestMethod.GET)
	public Iterable<ExciseGroup> findAllWithoutProducts() {
		return service.findAllWithoutProducts();
	}
	
	@RequestMapping(value="/product/{pId}/{gId}",method=RequestMethod.PATCH)
	public ResponseEntity<Boolean> findAllWithoutProducts(@PathVariable long pId, @PathVariable long gId) {
		return  new ResponseEntity<Boolean>(service.addProductToCategory(pId, gId), HttpStatus.OK);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public ExciseGroup get(@PathVariable long id){
		return service.findOne(id);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<Boolean> delete(@PathVariable long id) throws AccessDeniedException {
		return new ResponseEntity<Boolean>(service.delete(id), HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public ExciseGroup save (@RequestBody @Valid ExciseGroup productCategory) {
		return service.save(productCategory);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.PUT)
	public ExciseGroup update (@RequestBody @Valid ExciseGroup productCategory) {
		return service.save(productCategory);
	}
	
	
	@RequestMapping(value="/type", method=RequestMethod.GET)
	public Iterable<ExciseType> getAllTypes() {
		return serviceType.findAll();
	}
	
	@RequestMapping(value="/type/{id}", method=RequestMethod.GET)
	public ExciseType getType(@PathVariable long id){
		return serviceType.findOne(id);
	}
	
	@RequestMapping(value="/type/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<Boolean> deleteType(@PathVariable long id) throws AccessDeniedException {
		return new ResponseEntity<Boolean>(serviceType.delete(id), HttpStatus.OK);
	}
	
	@RequestMapping(value="/type", method=RequestMethod.POST)
	public ExciseType saveType (@RequestBody @Valid ExciseType productCategory) {
		return serviceType.save(productCategory);
	}
	
	@RequestMapping(value="/type/{id}", method=RequestMethod.PUT)
	public ExciseType updateType (@RequestBody @Valid ExciseType productCategory) {
		return serviceType.save(productCategory);
	}
	
	
	
}
