package com.shop.controller.resources;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.shop.base.exception.AccessDeniedException;
import com.shop.supplier.product.ProductCategory;
import com.shop.supplier.service.ProductCategoryService;

@RestController
@RequestMapping("/rest/product/categories")
public class ProductCategoryController {
	
	@Autowired
	private ProductCategoryService service;
	
	@RequestMapping(method=RequestMethod.GET)
	public Iterable<ProductCategory> getAll() {
		return service.getAll();
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public ProductCategory get(@PathVariable long id){
		return service.get(id);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<Boolean> delete(@PathVariable long id) throws AccessDeniedException {
		return new ResponseEntity<Boolean>(service.delete(id), HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public ProductCategory save (@RequestBody @Valid ProductCategory productCategory) {
		return service.save(productCategory);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.PUT)
	public ProductCategory update (@RequestBody @Valid ProductCategory productCategory) {
		return service.save(productCategory);
	}
}
