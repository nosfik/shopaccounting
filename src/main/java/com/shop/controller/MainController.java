package com.shop.controller;

import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.shop.base.navigation.NavigationService;
import com.shop.revenue.service.RevenueExpensesService;

@Controller
public class MainController {
	
	@Autowired
	private RevenueExpensesService revenueExpensesService;
	@Autowired
	private NavigationService navService;
	
	private ObjectMapper objectMapper = new ObjectMapper();
	
	private String timestamp = new Date().getTime() + "";
	
	
	@RequestMapping("/login")
	public String login(Map<String, Object> model) {
		model.put("template", "login");
		model.put("defaultJs", false);
		return "index";
	}
	
	@RequestMapping("/")
	public String index(Map<String, Object> model) {
		model.put("time", new Date());
		return "index";
	}

	@RequestMapping("/supplier")
	public String supplier(Map<String, Object> model) {
		model.put("template", "supplier");
		model.put("theme", "theme-blue");
		return "index";
	}

	@RequestMapping("/supplier/products/excise")
	public String supplierProductsExcise(Map<String, Object> model) {
		model.put("template", "excise");
		return "index";
	}
	
	@RequestMapping("/supplier/products/category")
	public String supplierProductsCategory(Map<String, Object> model) {
		model.put("template", "prodcat");
		return "index";
	}

	@RequestMapping("/invoice")
	public String invoice(Map<String, Object> model) throws JsonProcessingException {
		model.put("revenueType", objectMapper.writeValueAsString(revenueExpensesService.getRevenueType()));
		
		ArrayNode arr = objectMapper.createArrayNode();
		for(Map.Entry<String, String> entry : revenueExpensesService.getRevenueType().entrySet()) {
			ObjectNode node = objectMapper.createObjectNode();
			node.put("id", entry.getKey());
			node.put("name", entry.getValue());
			arr.add(node);
		}
		model.put("revenueTypeArr", arr);
		model.put("template", "invoice");
		return "index";
	}
	
	@RequestMapping("/invoice/print")
	public String invoicePrint(Map<String, Object> model) throws JsonProcessingException {
		model.put("template", "invoice_print");
		model.put("revenueType", objectMapper.writeValueAsString(revenueExpensesService.getRevenueType()));
		return "index";
	}
	
	@RequestMapping("/schedule")
	public String schedule(Map<String, Object> model) throws JsonProcessingException {
		model.put("template", "schedule");
		return "index";
	}
	
	@RequestMapping("/statistic")
	public String statistic(Map<String, Object> model) throws JsonProcessingException {
		model.put("template", "statistic");
		model.put("defaultJs", false);
		model.put("revenueType", objectMapper.writeValueAsString(revenueExpensesService.getRevenueType()));
		return "index";
	}
	
	@RequestMapping("/statistic/salary")
	public String sallary(Map<String, Object> model) throws JsonProcessingException {
		model.put("template", "sallary");
		return "index";
	}
	
	@RequestMapping("/statistic/excise")
	public String ExciseStat(Map<String, Object> model) throws JsonProcessingException {
		model.put("template", "excise_statistic");
		return "index";
	}
	
	@ModelAttribute
	void prepareBasicProps(HttpServletRequest request, Map<String, Object> model) {
		model.put("defaultJs", true);
		model.put("url", request.getRequestURI());
		model.put("menuList", navService.getNavigation());
		model.put("timeStamp", timestamp);
		model.put("theme", "theme-default");
		if(request.getUserPrincipal() != null) {
			model.put("username", request.getUserPrincipal().getName());
			model.put("isAdmin", request.isUserInRole("ROLE_ADMIN"));
		}
	}
	


}
