package com.shop.invoice.model;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Map;

import javax.persistence.EntityNotFoundException;

import org.springframework.util.StringUtils;

import com.shop.invoice.repository.InvoiceProduct;
import com.shop.util.ArithmeticUtil;

public class InvoiceProductDto {

	private long id;
	private String name;
	private BigDecimal price;
	private BigDecimal excisePrice;
	private BigDecimal margin;
	private BigDecimal marginPrice;
	private BigDecimal total;
	private BigDecimal totalWithMargin;
	private BigDecimal quantity;
	private Long productId;
	private String code;

	public InvoiceProductDto() {
	}

	public InvoiceProductDto(InvoiceProduct ip, boolean excise) {
		this.id = ip.getId();
		this.name = StringUtils.isEmpty(ip.getName()) ? ip.getProduct().getName() : ip.getName();
		this.price = ip.getPrice() == null ? ArithmeticUtil.ZERO : ip.getPrice();
		this.marginPrice = ip.getMarginPrice() == null ? ArithmeticUtil.ZERO : ip.getMarginPrice();
		this.quantity = ip.getQuantity();
		this.productId = ip.getProduct().getId();
		this.excisePrice = ip.getExcisePrice();
		if (excise) {
			this.total = this.excisePrice.multiply(quantity).setScale(3, RoundingMode.HALF_UP);
			this.margin = ArithmeticUtil.getMargin(marginPrice, excisePrice);
		} else {
			this.margin = ArithmeticUtil.getMargin(marginPrice, price);
			this.total = this.price.multiply(quantity).setScale(3, RoundingMode.HALF_UP);
		}
		this.totalWithMargin = this.marginPrice.multiply(quantity).setScale(2, RoundingMode.HALF_UP);
		
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public BigDecimal getMargin() {
		return margin;
	}

	public void setMargin(BigDecimal margin) {
		this.margin = margin;
	}

	public BigDecimal getMarginPrice() {
		return marginPrice;
	}

	public void setMarginPrice(BigDecimal marginPrice) {
		this.marginPrice = marginPrice;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public BigDecimal getTotalWithMargin() {
		return totalWithMargin;
	}

	public void setTotalWithMargin(BigDecimal totalWithMargin) {
		this.totalWithMargin = totalWithMargin;
	}

	public BigDecimal getQuantity() {
		return quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public Long getProductId() {
		return productId;
	}

	public void setProductId(Long productId) {
		this.productId = productId;
	}

	public BigDecimal getExcisePrice() {
		return excisePrice;
	}

	public void setExcisePrice(BigDecimal excisePrice) {
		this.excisePrice = excisePrice;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
}
