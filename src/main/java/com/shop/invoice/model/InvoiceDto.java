package com.shop.invoice.model;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.shop.invoice.repository.Invoice;
import com.shop.invoice.repository.InvoiceProduct;
import com.shop.supplier.repository.Supplier;
import com.shop.util.ArithmeticUtil;
import static com.shop.util.ArithmeticUtil.ZERO;

public class InvoiceDto {

	private long id;
	private Supplier supplier;
	private BigDecimal expenses;
	private java.sql.Date date;
	private Date createdDate;
	private Date updatedDate;
	private List<InvoiceProductDto> products;
	private BigDecimal averageMargin;
	private boolean credit;
	private boolean excise;
	private BigDecimal total;
	private BigDecimal totalWithMargin;

	public InvoiceDto() {
	}

	public InvoiceDto(Invoice invoice) {
		this.id = invoice.getId();
		this.supplier = invoice.getSupplier();
		this.supplier.setProducts(null); // prevent load products from db
		this.expenses = invoice.getExpenses();
		this.date = invoice.getDate();
		this.createdDate = invoice.getCreatedDate();
		this.updatedDate = invoice.getUpdatedDate();
		this.credit = invoice.isCredit();
		this.excise = invoice.isExcise();
		this.products = new ArrayList<>();
		for (InvoiceProduct ip : invoice.getProducts()) {
			products.add(new InvoiceProductDto(ip, excise));
		}
		if (products.size() > 0) {
			this.total = ZERO;
			this.totalWithMargin = ZERO;
			this.averageMargin = ZERO;
			for (InvoiceProductDto p : products) {
				this.total = this.total.add(p.getTotal());
				this.totalWithMargin = this.totalWithMargin.add(p.getTotalWithMargin());
			}
			this.total = this.total.setScale(2, RoundingMode.HALF_UP);
			this.totalWithMargin = this.totalWithMargin.setScale(2, RoundingMode.HALF_UP);
			this.averageMargin = ArithmeticUtil.getMargin(totalWithMargin, total);
		}
	}
	
	public InvoiceDto updateCodesAndReturnInvoice(Map<Long, String> productCodes) {
		
		this.products.stream().forEach (it -> {
			it.setCode(productCodes.get(it.getProductId()));
		});
		
		return this;
		
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Supplier getSupplier() {
		return supplier;
	}

	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}

	public BigDecimal getExpenses() {
		return expenses;
	}

	public void setExpenses(BigDecimal expenses) {
		this.expenses = expenses;
	}

	public java.sql.Date getDate() {
		return date;
	}

	public void setDate(java.sql.Date date) {
		this.date = date;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public List<InvoiceProductDto> getProducts() {
		return products;
	}

	public void setProducts(List<InvoiceProductDto> products) {
		this.products = products;
	}

	public BigDecimal getAverageMargin() {
		return averageMargin;
	}

	public void setAverageMargin(BigDecimal averageMargin) {
		this.averageMargin = averageMargin;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public BigDecimal getTotalWithMargin() {
		return totalWithMargin;
	}

	public void setTotalWithMargin(BigDecimal totalWithMargin) {
		this.totalWithMargin = totalWithMargin;
	}

	public boolean isCredit() {
		return credit;
	}

	public void setCredit(boolean credit) {
		this.credit = credit;
	}

	public boolean isExcise() {
		return excise;
	}

	public void setExcise(boolean excise) {
		this.excise = excise;
	}
	
}
