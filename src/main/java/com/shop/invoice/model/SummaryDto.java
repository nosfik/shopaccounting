package com.shop.invoice.model;

import java.math.BigDecimal;
import java.math.RoundingMode;
import static com.shop.util.ArithmeticUtil.ZERO;

import com.shop.schedule.repository.SellerSchedule;

public class SummaryDto {

	private SellerSchedule sellerSchedule;
	private BigDecimal invoiceRevenue;
	private BigDecimal invoiceExpenses;
	private BigDecimal revenueRevenue;
	private BigDecimal revenueExpenses;
	private BigDecimal totalRevenue;
	private BigDecimal totalExpenses;
	private BigDecimal invoiceSumm;
	private BigDecimal invoiceAverageMargin;
	private BigDecimal balanceCalculated;
	private Integer numberOfCreditInvoices;

	public SellerSchedule getSellerSchedule() {
		return sellerSchedule;
	}

	public void setSellerSchedule(SellerSchedule sellerSchedule) {
		this.sellerSchedule = sellerSchedule;
	}

	public BigDecimal getInvoiceRevenue() {
		return invoiceRevenue;
	}

	public void setInvoiceRevenue(BigDecimal invoiceRevenue) {
		if (invoiceRevenue == null) {
			this.invoiceRevenue = ZERO;
		} else {
			this.invoiceRevenue = invoiceRevenue.setScale(2, RoundingMode.HALF_UP);
		}
	}

	public BigDecimal getInvoiceExpenses() {
		return invoiceExpenses;
	}

	public void setInvoiceExpenses(BigDecimal invoiceExpenses) {
		if (invoiceExpenses == null) {
			this.invoiceExpenses = ZERO;
		} else {
			this.invoiceExpenses = invoiceExpenses.setScale(2, RoundingMode.HALF_UP);
		}

	}

	public BigDecimal getRevenueRevenue() {
		return revenueRevenue;
	}

	public void setRevenueRevenue(BigDecimal revenueRevenue) {
		if (revenueRevenue == null) {
			this.revenueRevenue = ZERO;
		} else {
			this.revenueRevenue = revenueRevenue.setScale(2, RoundingMode.HALF_UP);
		}
	}

	public BigDecimal getRevenueExpenses() {
		return revenueExpenses;
	}

	public void setRevenueExpenses(BigDecimal revenueExpenses) {
		if (revenueExpenses == null) {
			this.revenueExpenses = ZERO;
		} else {
			this.revenueExpenses = revenueExpenses.setScale(2, RoundingMode.HALF_UP);
		}
	}

	public BigDecimal getTotalRevenue() {
		return totalRevenue;
	}

	public void setTotalRevenue(BigDecimal totalRevenue) {
		this.totalRevenue = totalRevenue.setScale(2, RoundingMode.HALF_UP);
	}

	public BigDecimal getTotalExpenses() {
		return totalExpenses;
	}

	public void setTotalExpenses(BigDecimal totalExpenses) {
		this.totalExpenses = totalExpenses.setScale(2, RoundingMode.HALF_UP);
	}

	public BigDecimal getInvoiceSumm() {
		return invoiceSumm;
	}

	public void setInvoiceSumm(BigDecimal invoiceSumm) {
		if (invoiceSumm == null) {
			this.invoiceSumm = ZERO;
		} else {
			this.invoiceSumm = invoiceSumm.setScale(2, RoundingMode.HALF_UP);
		}
	}

	public BigDecimal getBalanceCalculated() {
		return balanceCalculated;
	}

	public void setBalanceCalculated(BigDecimal balanceCalculated) {
		this.balanceCalculated = balanceCalculated;
	}

	public BigDecimal getInvoiceAverageMargin() {
		return invoiceAverageMargin;
	}

	public void setInvoiceAverageMargin(BigDecimal invoiceAverageMargin) {
		this.invoiceAverageMargin = invoiceAverageMargin;
	}

	public Integer getNumberOfCreditInvoices() {
		return numberOfCreditInvoices;
	}

	public void setNumberOfCreditInvoices(Integer numberOfCreditInvoices) {
		this.numberOfCreditInvoices = numberOfCreditInvoices;
	}

}
