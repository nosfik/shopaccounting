package com.shop.invoice.service;

import java.sql.Date;

import com.shop.base.exception.BalanceNotFoundException;
import com.shop.base.service.GenericRestService;
import com.shop.invoice.model.SummaryDto;
import com.shop.invoice.repository.Balance;
import com.shop.invoice.repository.BalancePK;

public interface BalanceService extends GenericRestService<Balance, BalancePK> {

	void calculateBalance(Date date) throws BalanceNotFoundException;
	
	void calculateBalance(Date from, Date to) throws BalanceNotFoundException;

	Balance getBalance(Date date) throws BalanceNotFoundException;

	SummaryDto getSummary(Date date);
	
	Date getLastCalculatedBalanceDate();
}
