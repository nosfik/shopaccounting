package com.shop.invoice.service;

import java.util.List;

import com.shop.base.exception.AccessDeniedException;
import com.shop.base.exception.BalanceNotFoundException;
import com.shop.invoice.model.InvoiceDto;
import com.shop.invoice.repository.Invoice;

public interface InvoiceService {
	
	boolean deleteInvoice(long id) throws AccessDeniedException, BalanceNotFoundException;
	
	Invoice save(Invoice invoice) throws BalanceNotFoundException, AccessDeniedException;
	
	boolean fixCredit(long id);

	Invoice getInvoice(long id);

	List<InvoiceDto> getByDate(java.sql.Date date);
	
	List<InvoiceDto> getCredits();

	Invoice update(Invoice invoice) throws BalanceNotFoundException, AccessDeniedException;


	List<InvoiceDto> getBySupplierOrProduct(long supplierId, long productId, int limit);

}