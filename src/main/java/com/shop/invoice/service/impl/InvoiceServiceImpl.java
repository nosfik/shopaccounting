package com.shop.invoice.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.shop.base.exception.AccessDeniedException;
import com.shop.base.exception.BalanceNotFoundException;
import com.shop.invoice.model.InvoiceDto;
import com.shop.invoice.repository.Invoice;
import com.shop.invoice.repository.InvoiceDAO;
import com.shop.invoice.repository.InvoiceProduct;
import com.shop.invoice.repository.InvoiceSpecifications;
import com.shop.invoice.service.BalanceService;
import com.shop.invoice.service.InvoiceService;
import com.shop.supplier.product.Cigarette;
import com.shop.supplier.product.Product;
import com.shop.supplier.product.ProductDao;
import com.shop.supplier.product.ProductIdWithCode;
import com.shop.supplier.repository.Supplier;
import com.shop.supplier.repository.SupplierDao;
import com.shop.util.ArithmeticUtil;

@Service
public class InvoiceServiceImpl implements InvoiceService {

	@Autowired
	private InvoiceDAO invoiceDao;

	@Autowired
	private BalanceService balanceService;

	@Autowired
	private ProductDao productDao;

	@Autowired
	private SupplierDao supplierDao;

	@Override
	public List<InvoiceDto> getBySupplierOrProduct(long supplierId, long productId, int limit) {
		Specification<Invoice> spec = (productId == 0) ? InvoiceSpecifications.hasSupplier(supplierId) : InvoiceSpecifications.hasProduct(productId);
		Page<Invoice> invoices = invoiceDao.findAll(spec,  new PageRequest(0, limit, new Sort(Direction.DESC, "date")));
		return invoices.getContent().stream().map(i -> new InvoiceDto(i)).collect(Collectors.toList());
	}

	@Override
	public List<InvoiceDto> getByDate(java.sql.Date date) {
		List<Invoice> invoices = invoiceDao.findByDateOrderByIdAsc(date);
		List<ProductIdWithCode> products = productDao.findAllProductWithCode();
		Map<Long, String> productIdCode = products.stream().collect(Collectors.toMap(ProductIdWithCode::getId, ProductIdWithCode::getCode));
		return invoices.stream().map(i -> new InvoiceDto(i).updateCodesAndReturnInvoice(productIdCode)).collect(Collectors.toList());
	}


	@Override
	@Transactional
	public boolean deleteInvoice(long id) throws AccessDeniedException, BalanceNotFoundException {
		Invoice in = getInvoice(id);
		invoiceDao.delete(id);
		balanceService.calculateBalance(in.getDate(), balanceService.getLastCalculatedBalanceDate());
		return true;
	}

	@Override
	@Transactional
	public Invoice update(Invoice invoice) throws BalanceNotFoundException, AccessDeniedException {
		createOrUpdateSupplierProducts(invoice);
		Invoice in = invoiceDao.findOne(invoice.getId());
		in.setExcise(invoice.isExcise());
		in.setCredit(invoice.isCredit());
		in.setExpenses(invoice.getExpenses());
		in.setUpdatedDate(new Date());
		in.mergeInvoiceProducts(invoice.getProducts());
		balanceService.calculateBalance(invoice.getDate(), balanceService.getLastCalculatedBalanceDate());
		return in;
	}

	@Override
	@Transactional
	public Invoice save(Invoice invoice) throws BalanceNotFoundException, AccessDeniedException {
		if (invoice.getId() > 0) {
			throw new IllegalArgumentException("shop.invoice.save.error.not.new");
		}

		createOrUpdateSupplierProducts(invoice);
		Invoice in = invoiceDao.save(invoice);
		balanceService.calculateBalance(invoice.getDate(), balanceService.getLastCalculatedBalanceDate());
		return in;
	}

	private void createOrUpdateSupplierProducts(Invoice invoice) {
		Supplier supplier = supplierDao.findOne(invoice.getSupplier().getId());
		Set<String> barcodes = new HashSet<>();
		for (InvoiceProduct ip : invoice.getProducts()) {
			if (ip.getProduct() != null) {
				if (ip.getProduct().getId() == 0) {
					Product newProduct = (ip.getProduct() instanceof Cigarette) ? new Cigarette() : new Product();
					newProduct.setName(ip.getProduct().getName());
					newProduct.setMarginPrice(ip.getProduct().getMarginPrice());
					newProduct.setExcisePrice(ip.getProduct().getExcisePrice());
					newProduct.setPrice(ip.getProduct().getPrice());
					newProduct.setSupplier(supplier);
					if (newProduct instanceof Cigarette) {
						Cigarette cig = (Cigarette) newProduct;
						cig.setBarcode(((Cigarette) ip.getProduct()).getBarcode());
						cig.setCode(((Cigarette) ip.getProduct()).getCode());
						if (barcodes.contains(cig.getBarcode())) {
							Cigarette dbCig = productDao.getCigaretteByBarCode(cig.getBarcode());
							productDao.updateProductPrices(dbCig.getId(), ArithmeticUtil.ZERO, ArithmeticUtil.ZERO,
									ArithmeticUtil.ZERO);
							cig.setId(dbCig.getId());
						} else {
							barcodes.add(cig.getBarcode());
							productDao.save(cig);
						}

					} else {
						productDao.save(newProduct);
					}
					ip.getProduct().setId(newProduct.getId());
				} else {
					Product product = productDao.findOne(ip.getProduct().getId());

					if (product.getPrice() != null) {
						ip.setOldPrice(product.getPrice().setScale(3));
					}

					if (isNotEqual(product.getPrice(), ip.getPrice())
							|| isNotEqual(product.getMarginPrice(), ip.getMarginPrice())
							|| isNotEqual(product.getExcisePrice(), ip.getExcisePrice())) {

						productDao.updateProductPrices(ip.getProduct().getId(), ip.getPrice(), ip.getMarginPrice(),
								ip.getExcisePrice());
					}

				}
				ip.setName(ip.getProduct().getName());
			}
		}
	}

	@Override
	public Invoice getInvoice(long id) {
		return invoiceDao.findOne(id);
	}

	@Override
	public List<InvoiceDto> getCredits() {
		List<Invoice> invoices = invoiceDao.findCredits();
		List<InvoiceDto> result = new ArrayList<>(invoices.size());
		for (Invoice invoice : invoices) {
			result.add(new InvoiceDto(invoice));
		}
		return result;
	}

	@Override
	public boolean fixCredit(long id) {
		return invoiceDao.fixCredit(id) > 0;
	}

	private boolean isNotEqual(BigDecimal p1, BigDecimal p2) {
		if (p1 == p2 || p1 == null || p2 == null)
			return true;

		return p1.compareTo(p2) != 0;
	}

}
