package com.shop.invoice.service.impl;

import static com.shop.util.ArithmeticUtil.ZERO;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Date;
import java.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shop.base.exception.BalanceNotFoundException;
import com.shop.invoice.model.SummaryDto;
import com.shop.invoice.repository.Balance;
import com.shop.invoice.repository.BalanceDAO;
import com.shop.invoice.repository.BalancePK;
import com.shop.invoice.repository.InvoiceDAO;
import com.shop.invoice.service.BalanceService;
import com.shop.revenue.repository.RevenueExpensesDao;
import com.shop.schedule.repository.SellerScheduleDao;
import com.shop.util.ArithmeticUtil;

@Service
public class BalanceServiceImpl implements BalanceService {

	@Autowired
	private BalanceDAO repository;

	@Autowired
	private InvoiceDAO invoiceRepository;

	@Autowired
	private SellerScheduleDao sellerSchedule;

	@Autowired
	private RevenueExpensesDao revenueRepository;

	@Override
	public Iterable<Balance> getAll() {
		return repository.findAll();
	}

	@Override
	public Balance get(BalancePK pk) {
		return repository.findOne(pk);
	}

	@Override
	public boolean delete(BalancePK pk) {
		repository.delete(pk);
		return true;
	}

	@Override
	public Balance save(Balance t) {
		return repository.save(t);
	}

	@Override
	public SummaryDto getSummary(Date date) {
		SummaryDto result = new SummaryDto();
		result.setSellerSchedule(sellerSchedule.findOne(date));
		result.setNumberOfCreditInvoices(invoiceRepository.countCredit());

		Balance balance = get(new BalancePK(date, true));
		if (balance == null) {
			balance = get(new BalancePK(minusDay(date), true));

			if (balance != null) {
				result.setBalanceCalculated(balance.getBalance());
			}
			return result;
		}

		result.setBalanceCalculated(balance.getBalance());
		result.setInvoiceExpenses(invoiceRepository.getDayExpenses(date));
		result.setInvoiceRevenue(invoiceRepository.getDayRevenue(date));
		result.setInvoiceSumm(invoiceRepository.getDaySumm(date));
		BigDecimal dayExciseSum = invoiceRepository.getExciseDaySumm(date);
		if(dayExciseSum != null) {
			result.setInvoiceSumm(result.getInvoiceSumm().add(dayExciseSum));
		}
		
		result.setInvoiceAverageMargin(ArithmeticUtil.getMargin(result.getInvoiceRevenue(), result.getInvoiceSumm()));
		result.setRevenueExpenses(revenueRepository.getDayExpenses(date));
		result.setRevenueRevenue(revenueRepository.getDayRevenue(date));
		result.setTotalExpenses(result.getInvoiceExpenses().add(result.getRevenueExpenses()));
		result.setTotalRevenue(result.getInvoiceRevenue().add(result.getRevenueRevenue()));

		return result;

	}

	@Override
	public void calculateBalance(Date date) throws BalanceNotFoundException {

		Balance balance = getBalance(date);
		BigDecimal dayInvoiceExpenses = invoiceRepository.getDayExpenses(date);
		if (dayInvoiceExpenses == null)
			dayInvoiceExpenses = ZERO;
		BigDecimal dayInvoiceRevenue = invoiceRepository.getDayRevenue(date);
		if (dayInvoiceRevenue == null)
			dayInvoiceRevenue = ZERO;
		BigDecimal dayRevenueRevenue = revenueRepository.getDayRevenue(date);
		if (dayRevenueRevenue == null)
			dayRevenueRevenue = ZERO;
		BigDecimal dayRevenueExpenses = revenueRepository.getDayExpenses(date);
		if (dayRevenueExpenses == null)
			dayRevenueExpenses = ZERO;

		BigDecimal newB = balance.getBalance().add(dayRevenueRevenue).subtract(dayRevenueExpenses)
				.add(dayInvoiceRevenue).subtract(dayInvoiceExpenses).setScale(3, RoundingMode.HALF_UP);

		Balance newBalance = new Balance(new BalancePK(date, true), newB);
		save(newBalance);

	}

	@Override
	public Balance getBalance(Date date) throws BalanceNotFoundException {
		Balance balance = get(new BalancePK(date, false));

		if (balance == null) {
			balance = get(new BalancePK(minusDay(date), true));
		}

		if (balance == null) {
			throw new BalanceNotFoundException("Balance for date '" + date.toString() + "' was not found");
		}

		return balance;
	}

	private Date minusDay(Date d) {
		return Date.valueOf(d.toLocalDate().minusDays(1)); // Magic happens here!
	}

	@Override
	public Date getLastCalculatedBalanceDate() {
		return repository.getLastCalculatedBalance();
	}

	@Override
	public void calculateBalance(Date from, Date to) throws BalanceNotFoundException {
		if (to != null && from.before(to)) {
			for (LocalDate fromDate = from.toLocalDate(), toDate = to.toLocalDate().plusDays(1); fromDate
					.isBefore(toDate); fromDate = fromDate.plusDays(1)) {
				calculateBalance(Date.valueOf(fromDate));
			}
		} else {
			calculateBalance(from);
		}

	}

}
