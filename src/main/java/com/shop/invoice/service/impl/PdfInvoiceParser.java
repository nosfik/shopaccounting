package com.shop.invoice.service.impl;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfReaderContentParser;
import com.itextpdf.text.pdf.parser.SimpleTextExtractionStrategy;
import com.itextpdf.text.pdf.parser.TextExtractionStrategy;
import com.shop.supplier.product.Cigarette;
import com.shop.supplier.product.CigaretteDTO;
import com.shop.supplier.product.ProductDao;

@Service
public class PdfInvoiceParser {
	
	@Autowired
	private ProductDao productDao;
	private final Logger log = LoggerFactory.getLogger(PdfInvoiceParser.class);
	
	private Pattern pattern = Pattern
			.compile("^(\\d{1,4})\\s{1}\\w+\\s{1}\\S+\\s{1}(.+?)\\s{1}(4[0-9]+)\\s{1}([0-9,]+)\\s{1}\\S+\\s{1}"
					+ "([0-9,]+)\\s{1}([0-9,]+)\\s{1}([0-9,]+)\\s{1}([0-9,]+)\\s{1}([0-9,]+)\\s{1}([0-9,]+)\\s{1}[A-Z0-9\\.-]+",  Pattern.DOTALL | Pattern.MULTILINE);
	
	private BigDecimal regionTax = new BigDecimal("1.05");

	public List<CigaretteDTO> parsePDF(byte[] bytes) throws IOException {
		
		PdfReader reader = new PdfReader(bytes);
		
		StringBuilder sb = new StringBuilder();
		PdfReaderContentParser parser = new PdfReaderContentParser(reader);
		TextExtractionStrategy strategy;
		for (int i = 1; i <= reader.getNumberOfPages(); i++) {
			strategy = parser.processContent(i, new SimpleTextExtractionStrategy());
			sb.append(strategy.getResultantText());
		}
		
		String text = sb.toString();
		
		text = text.replaceAll("(\\D)(\\xA0)(\\D)", "$1 $3");
		text = text.replace("\u00A0","");
		Matcher mc = pattern.matcher(text);
		
		List<CigaretteDTO> list = new ArrayList<>();
		
		while(mc.find()) {
			try {
				Cigarette cig = productDao.getCigaretteByBarCode(mc.group(3));
				CigaretteDTO cigarrete = new CigaretteDTO();
				if(cig != null) {
					cigarrete.setOldMarginPrice(cig.getMarginPrice());
					cigarrete.setOldPrice(cig.getPrice());
					cigarrete.setOldName(cig.getName());
					cigarrete.setId(cig.getId());
					cigarrete.setCode(cig.getCode());
					
					if(cig.getExcisePrice() == null) {
						cigarrete.setOldExcisePrice(cig.getPrice());
					} else {
						cigarrete.setOldExcisePrice(cig.getExcisePrice());
					}
					
				}
				cigarrete.setQuantity(new BigDecimal(mc.group(4).replace(',', '.')));
				cigarrete.setName(mc.group(2).replace("\n", "").replace("\r", ""));
				cigarrete.setBarcode(mc.group(3));
				cigarrete.setPrice(new BigDecimal(mc.group(9).replace(',', '.')).divide(cigarrete.getQuantity(), 3, RoundingMode.HALF_UP));
				cigarrete.setRecommendedPrice(new BigDecimal(mc.group(10).replace(',', '.')));
				cigarrete.setMarginPrice(cigarrete.getRecommendedPrice().multiply(regionTax).setScale(2, RoundingMode.DOWN));
				list.add(cigarrete);
			} catch(Exception e) {
				log.error(e.getMessage(),e);
			}
		}
		return list;
		
	}


}
