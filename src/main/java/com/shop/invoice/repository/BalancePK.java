package com.shop.invoice.repository;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Embeddable;

@Embeddable
public class BalancePK implements Serializable {
	protected Date date;
	protected Boolean calculated;

    public BalancePK() {}

    public BalancePK(Date date, boolean calculated) {
        this.date = date;
        this.calculated = calculated;
    }

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Boolean getCalculated() {
		return calculated;
	}

	public void setCalculated(Boolean calculated) {
		this.calculated = calculated;
	}
    
}