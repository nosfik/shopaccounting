package com.shop.invoice.repository;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface InvoiceDAO extends CrudRepository<Invoice, Long>, JpaSpecificationExecutor<Invoice> {
	
	
	@Query("SELECT DISTINCT i FROM Invoice i LEFT JOIN FETCH i.products products LEFT JOIN FETCH i.supplier supplier LEFT JOIN FETCH supplier.productCategory WHERE i.date =:date")
	List<Invoice> findByDateOrderByIdAsc(@Param("date")Date date);

	@Query("SELECT DISTINCT i FROM Invoice i LEFT JOIN FETCH i.products products LEFT JOIN FETCH i.supplier supplier LEFT JOIN FETCH supplier.productCategory WHERE i.credit = true")
	List<Invoice> findCredits();

	@Query("SELECT COUNT(inv) FROM Invoice inv WHERE inv.credit = true")
	Integer countCredit();

	@Transactional
	@Modifying
	@Query("update Invoice i set i.credit = false where i.id =:id")
	Integer fixCredit(@Param("id") Long id);
	
	@Query(value="SELECT SUM(quantity * margin_price) FROM invoice i JOIN invoice_products ips ON ips.invoice = i.id LEFT JOIN invoice_product ip ON ip.id = ips.products WHERE i.date = :date",nativeQuery=true)
	BigDecimal getDayRevenue(@Param("date") Date date);
	
	
	@Query(value="SELECT i.date, SUM(quantity * margin_price) FROM invoice i JOIN invoice_products ips ON ips.invoice = i.id LEFT JOIN invoice_product ip ON ip.id = ips.products WHERE i.date between :from and :to group by i.date",nativeQuery=true)
	List<Object[]> getDateRevenue(@Param("from") Date from, @Param("to") Date to);
	
	@Query(value="SELECT SUM(quantity * price) FROM invoice i JOIN invoice_products ips ON ips.invoice = i.id LEFT JOIN invoice_product ip ON ip.id = ips.products WHERE i.date = :date and i.excise=false",nativeQuery=true)
	BigDecimal getDaySumm(@Param("date") Date date);
	
	@Query(value="SELECT SUM(quantity * excise_price) FROM invoice i JOIN invoice_products ips ON ips.invoice = i.id LEFT JOIN invoice_product ip ON ip.id = ips.products WHERE i.date = :date and i.excise=true",nativeQuery=true)
	BigDecimal getExciseDaySumm(@Param("date") Date date);
	
	@Query(value="select name, sum, supplier  from (select sum(margin_price * quantity) sum , supplier from invoice i left join invoice_products ips on ips.invoice=i.id left join invoice_product ip on ip.id=ips.products where i.date between :from and :to group by supplier) as tableee join supplier s on s.id = tableee.supplier order by sum desc limit 25", nativeQuery=true)
	List<Object[]> getTopSuppliersForPeriod(@Param("from") Date from, @Param("to") Date to);
	
	@Query(value="select date, sum(margin_price * quantity) sum from invoice i left join invoice_products ips on ips.invoice=i.id left join invoice_product ip on ip.id=ips.products where i.date between :from and :to and supplier=:id group by date", nativeQuery=true)
	List<Object[]> getSupplierInvoiceRevenueForPeriod(@Param("id") long sId, @Param("from") Date from, @Param("to") Date to);
	
	@Query("SELECT SUM(expenses) FROM Invoice i WHERE i.date = :date")
	BigDecimal getDayExpenses(@Param("date") Date date);
	
	@Query("SELECT p.name, exciseType.name, exciseType.id, p.quantity, prod.volume, (prod.volume * p.quantity ), i.date "
			+ "FROM Invoice i join  i.products as p join  p.product as prod left join prod.exciseType exciseType WHERE i.date between :from and :to and i.excise = true order by i.date asc")
	List<Object[]> getExciseProducts(@Param("from") Date from, @Param("to") Date to);
	
}