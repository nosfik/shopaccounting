package com.shop.invoice.repository;

import org.springframework.data.jpa.domain.Specification;

public class InvoiceSpecifications {
	
	
	public static Specification<Invoice> hasSupplier(long supplierId) {
		return (root, cq, cb) ->  cb.equal(root.get("supplier").get("id"), supplierId);
	}
	
	public static Specification<Invoice> hasProduct(long p) {
		
		return (root, cq, cb) ->   {
			
			return cb.equal(root.join("products").join("product").get("id"), p);
			 
		};
	}

}
