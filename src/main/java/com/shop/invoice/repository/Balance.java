package com.shop.invoice.repository;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

@Entity
public class Balance {
	
	@EmbeddedId
	private BalancePK balancePk;
	@Column(nullable=false, precision = 15, scale = 3 )
	private BigDecimal balance;
	
	public Balance(){}
	
	public Balance(BalancePK balancePk, BigDecimal balance) {
		this.balancePk = balancePk;
		this.balance = balance;
	}


	public BalancePK getBalancePk() {
		return balancePk;
	}


	public void setBalancePk(BalancePK balancePk) {
		this.balancePk = balancePk;
	}


	public BigDecimal getBalance() {
		return balance;
	}


	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
	
	
}
