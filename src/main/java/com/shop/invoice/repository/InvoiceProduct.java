package com.shop.invoice.repository;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.shop.supplier.product.Product;

@Entity
public class InvoiceProduct {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(nullable = false)
	private Product product;
	@Column(nullable = false, precision = 8, scale = 3)
	private BigDecimal price;
	@Column(nullable = true, precision = 8, scale = 3)
	private BigDecimal excisePrice;
	@Column(nullable = false)
	private BigDecimal marginPrice;
	@Column(precision = 8, scale = 3)
	private BigDecimal quantity;
	@Column(nullable = false)
	private String name;

	@Column(precision = 8, scale = 3)
	private BigDecimal oldPrice;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}

	public BigDecimal getMarginPrice() {
		return marginPrice;
	}

	public void setMarginPrice(BigDecimal marginPrice) {
		this.marginPrice = marginPrice;
	}

	public BigDecimal getQuantity() {
		return quantity;
	}

	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getOldPrice() {
		return oldPrice;
	}

	public void setOldPrice(BigDecimal oldPrice) {
		this.oldPrice = oldPrice;
	}

	public BigDecimal getExcisePrice() {
		return excisePrice;
	}

	public void setExcisePrice(BigDecimal excisePrice) {
		this.excisePrice = excisePrice;
	}

}
