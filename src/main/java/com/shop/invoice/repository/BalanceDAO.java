package com.shop.invoice.repository;

import java.math.BigDecimal;
import java.sql.Date;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;


public interface BalanceDAO extends CrudRepository<Balance, BalancePK> {
	
	@Query("FROM Balance b WHERE b.balancePk.date=?1 and calculated = true")
	Balance getCalculatedByDate(Date date);
	
	@Query("FROM Balance b WHERE b.balancePk.date=?1 and calculated = false")
	Balance getBalancedByDate(Date date);
	
	@Query("Select MAX(balancePk.date) FROM Balance WHERE calculated = true")
	Date getLastCalculatedBalance();
	
}