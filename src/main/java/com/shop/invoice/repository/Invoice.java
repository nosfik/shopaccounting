package com.shop.invoice.repository;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.shop.supplier.repository.Supplier;

@Entity
public class Invoice {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(nullable = false)
	@Fetch(FetchMode.JOIN)
	private Supplier supplier;

	private BigDecimal expenses;

	@Column(nullable = false)
	private java.sql.Date date;

	@Column(nullable = false)
	private Date createdDate;
	@JsonIgnore
	private Date updatedDate;
	private boolean credit;
	private boolean excise;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@Fetch(FetchMode.JOIN)
	@OrderBy("id ASC")
	private Set<InvoiceProduct> products;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Supplier getSupplier() {
		return supplier;
	}

	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}

	public BigDecimal getExpenses() {
		return expenses;
	}

	public void setExpenses(BigDecimal expenses) {
		this.expenses = expenses;
	}

	public java.sql.Date getDate() {
		return date;
	}

	public void setDate(java.sql.Date date) {
		this.date = date;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Set<InvoiceProduct> getProducts() {
		return products;
	}

	public void setProducts(Set<InvoiceProduct> products) {
		this.products = products;
	}

	public boolean isCredit() {
		return credit;
	}

	public void setCredit(boolean credit) {
		this.credit = credit;
	}
	
	public boolean isExcise() {
		return excise;
	}

	public void setExcise(boolean excise) {
		this.excise = excise;
	}

	public void mergeInvoiceProducts(Set<InvoiceProduct> newProducts) {

		Map<Long, InvoiceProduct> map = new HashMap<>();
		for (InvoiceProduct ip : newProducts) {
			map.put(ip.getId(), ip);
		}

		Iterator<InvoiceProduct> it = this.products.iterator();
		while (it.hasNext()) {
			InvoiceProduct ip = it.next();
			InvoiceProduct product = map.get(ip.getId());
			if (product == null) {
				it.remove();
			} else {
				newProducts.remove(product);
				ip.setMarginPrice(product.getMarginPrice());
				ip.setPrice(product.getPrice());
				ip.setExcisePrice(product.getExcisePrice());
				ip.setQuantity(product.getQuantity());
			}
		}

		for (InvoiceProduct ip : newProducts) {
			this.products.add(ip);
		}

	}
}
