package com.shop.util;

import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class DateUtil {
	
	public static SimpleDateFormat getUaDateFormat() {
		
		Locale ukrainian = new Locale("ua");
		String[] newMonths = {
		  "січня", "лютого", "березня", "квітня", "травня", "червня", 
		  "липня", "серпня", "вересня", "жовтня", "листопада", "грудня"
		  };
		DateFormatSymbols dateFormatSymbols = DateFormatSymbols.getInstance(ukrainian);
		dateFormatSymbols.setMonths(newMonths);
		SimpleDateFormat sdf = new SimpleDateFormat("d MMMM yyyy");
		sdf.setDateFormatSymbols(dateFormatSymbols);
		return  sdf;
	}

}
