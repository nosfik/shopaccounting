package com.shop.util;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class ArithmeticUtil {
	
	public static final BigDecimal ONE_HUNDRED = new BigDecimal(100);
	public static final BigDecimal ZERO = new BigDecimal("0.00");
	public static final BigDecimal TWO = new BigDecimal("2");
	
	public static BigDecimal getMargin(BigDecimal invoiceprice, BigDecimal price) {
		
		
		if(price == null || price.intValue() == ZERO.intValue()) {
			return ZERO;
		}
		
		return invoiceprice.divide(price, 2, RoundingMode.HALF_UP).multiply(ONE_HUNDRED).subtract(ONE_HUNDRED);
	}
	
	public static BigDecimal getPercentOfNumber(BigDecimal number, BigDecimal percent){
		return number.multiply(percent).divide(ONE_HUNDRED, 2, RoundingMode.HALF_UP);
	}

}
