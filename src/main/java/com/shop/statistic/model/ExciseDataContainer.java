package com.shop.statistic.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.shop.util.ArithmeticUtil;

public class ExciseDataContainer {
	
	
	private String name;
	private long id;
	private BigDecimal sum = ArithmeticUtil.ZERO;
	private List<ExciseData> products = new ArrayList<>(); 
	
	
	public ExciseDataContainer (String name, long id) {
		this.name = name;
		this.id = id;
	}
	
	
	public void addProduct(ExciseData prod) {
		products.add(prod);
		if(prod.getVolumeSumm() != null) {
			sum = sum.add(prod.getVolumeSumm());
		}
	}


	public String getName() {
		return name;
	}


	public long getId() {
		return id;
	}


	public BigDecimal getSum() {
		return sum;
	}


	public List<ExciseData> getProducts() {
		return products;
	}
	

}
