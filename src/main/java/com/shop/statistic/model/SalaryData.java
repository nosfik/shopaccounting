package com.shop.statistic.model;

import java.math.BigDecimal;
import java.util.Map;

public class SalaryData {
	
	private BigDecimal proceed;
	private BigDecimal salary;
	private int totalShifts;
	private int shiftsInMonth;
	private BigDecimal shiftSalary;
	private Map<String, BigDecimal> sellerSalary;
	
	public BigDecimal getProceed() {
		return proceed;
	}

	public void setProceed(BigDecimal proceed) {
		this.proceed = proceed;
	}

	public BigDecimal getSalary() {
		return salary;
	}

	public void setSalary(BigDecimal salary) {
		this.salary = salary;
	}

	public int getTotalShifts() {
		return totalShifts;
	}

	public void setTotalShifts(int totalShifts) {
		this.totalShifts = totalShifts;
	}

	public int getShiftsInMonth() {
		return shiftsInMonth;
	}

	public void setShiftsInMonth(int shiftsInMonth) {
		this.shiftsInMonth = shiftsInMonth;
	}

	public BigDecimal getShiftSalary() {
		return shiftSalary;
	}

	public void setShiftSalary(BigDecimal shiftSalary) {
		this.shiftSalary = shiftSalary;
	}

	public Map<String, BigDecimal> getSellerSalary() {
		return sellerSalary;
	}

	public void setSellerSalary(Map<String, BigDecimal> sellerSalary) {
		this.sellerSalary = sellerSalary;
	}

}
