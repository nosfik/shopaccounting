package com.shop.statistic.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class StatisticDataContainer {
	
	private Collection<Object> data1 = new ArrayList<>();
	private List<Entry> data2 = new ArrayList<Entry>();
	
	public Collection<Object> getData1() {
		return data1;
	}
	public void setData1(Collection<Object> data1) {
		this.data1 = data1;
	}
	
	public List<Entry> getData2() {
		return data2;
	}
	public void setData2(List<Entry> data2) {
		this.data2 = data2;
	}
	
	public void addEntryValue(String name, Object data){
		data2.add(new Entry(name, data));
	}
	
	public void addValue(String name, int entryIndex, Object data) {
		data2.get(entryIndex).data.add(data);
	}
	
	public void addValues(String name, List<Object> data) {
		data2.add(new Entry(name, data));
	}


	class Entry {
		
		public String name;
		public List<Object> data;
		
		public Entry(String name, List<Object> data) {
			this.name = name;
			this.data = data;
		}
		public Entry(String name, Object data) {
			this.name = name;
			this.data = new ArrayList<Object>();
			this.data.add(data);
		}
	}

}
