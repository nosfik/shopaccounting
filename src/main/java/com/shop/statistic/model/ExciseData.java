package com.shop.statistic.model;

import java.math.BigDecimal;
import java.sql.Date;

public class ExciseData {
	
	
	private String name;
	private String type;
	private long typeId;
	private BigDecimal quantity;
	private BigDecimal volume;
	private BigDecimal volumeSumm;
	private Date date;
	
	
	public ExciseData (Object[] row) {
		
		this.name = (String)row[0];
		this.type = row[1] == null ?  "NO CATEGORY"  :(String)row[1];
		this.typeId = row[2] == null ?  0  :(Long)row[2];
		this.quantity = (BigDecimal)row[3];
		this.volume = (BigDecimal)row[4];
		this.volumeSumm = (BigDecimal)row[5];
		this.date = (Date)row[6];
		
	}


	public String getName() {
		return name;
	}

	public String getType() {
		return type;
	}

	public long getTypeId() {
		return typeId;
	}

	public BigDecimal getQuantity() {
		return quantity;
	}

	public BigDecimal getVolume() {
		return volume;
	}

	public BigDecimal getVolumeSumm() {
		return volumeSumm;
	}
	
	public Date getDate() {
		return date;
	}


	@Override
	public String toString() {
		return "ExciseData [name=" + name + ", type=" + type + ", typeId=" + typeId + ", quantity=" + quantity
				+ ", volume=" + volume + ", volumeSumm=" + volumeSumm + "]";
	}
	
}
