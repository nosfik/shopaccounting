package com.shop.statistic.model;

import java.util.HashMap;
import java.util.Map;

public class ExciseDataDecorator {
	
	
	private Map<String, ExciseDataContainer> data = new HashMap<>();
	private boolean full = true;
	
	
	public void addExciseDate(ExciseData ed) {
		
		ExciseDataContainer ec = data.get(ed.getType());
		if(ec == null) {
			ec = new ExciseDataContainer(ed.getType(), ed.getTypeId());
			data.put(ed.getType(), ec);
		}
		
		if(ed.getVolumeSumm() == null) {
			full = false;
		}
		
		ec.addProduct(ed);
		
	}

	public Map<String, ExciseDataContainer> getData() {
		return data;
	}

	public boolean isFull() {
		return full;
	}

	public void setFull(boolean full) {
		this.full = full;
	}
	
	

}
