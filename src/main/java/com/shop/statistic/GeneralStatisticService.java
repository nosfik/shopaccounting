package com.shop.statistic;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shop.invoice.repository.InvoiceDAO;
import com.shop.revenue.repository.RevenueExpensesDao;
import com.shop.revenue.repository.model.RevenueExpensesSumByType;
import com.shop.revenue.service.RevenueExpensesService;
import com.shop.statistic.model.ExciseData;
import com.shop.statistic.model.ExciseDataDecorator;
import com.shop.statistic.model.StatisticDataContainer;

@Service
public class GeneralStatisticService {

	@Autowired
	private RevenueExpensesDao revenueDao;
	
	@Autowired
	private InvoiceDAO invoiceDao;

	@Autowired
	private RevenueExpensesService revenueExpensesService;
	
	
	public StatisticDataContainer getInvoiceRevenues(Date from, Date to) {
		StatisticDataContainer container = new StatisticDataContainer();
		List<Object[]> invRevenues =invoiceDao.getDateRevenue(from, to);
		List<Object> value = new ArrayList<>();
		for (Object[] re : invRevenues) {
			container.getData1().add(re[0]);
			value.add(re[1]);
		}
		container.addValues("Період", value);
		return container;
	}
	
	
	public StatisticDataContainer getTopSuppliers(Date from, Date to) {
		StatisticDataContainer container = new StatisticDataContainer();
		List<Object[]> invRevenues = invoiceDao.getTopSuppliersForPeriod(from, to);
		for (Object[] re : invRevenues) {
			container.getData1().add(re[0]);
			
			Map<String, Object> map = new HashMap<>();
			map.put("y", re[1]);
			map.put("id", re[2]);
			
			container.addEntryValue((String)re[0], map);
		}
		return container;
	}
	
	public StatisticDataContainer getSupplierInvoiceRevenue(Date from, Date to, long supplierId) {
		StatisticDataContainer container = new StatisticDataContainer();
		List<Object[]> invRevenues = invoiceDao.getSupplierInvoiceRevenueForPeriod(supplierId, from, to);
		List<Object> value = new ArrayList<>();
		for (Object[] re : invRevenues) {
			container.getData1().add(re[0]);
			value.add(re[1]);
		}
		container.addValues("Період", value);
		return container;
	}

	public StatisticDataContainer getProceeds(Date from, Date to) {
		StatisticDataContainer container = new StatisticDataContainer();
		List<Object[]> proceeds = revenueDao.getProceeds(from, to);
		List<Object> value = new ArrayList<>();
		for (Object[] re : proceeds) {
			container.getData1().add(re[0]);
			value.add(re[1]);
		}
		container.addValues("Період", value);
		return container;
	}

	public StatisticDataContainer getOtherRevenueExpenses(Date from, Date to) {
		StatisticDataContainer container = new StatisticDataContainer();
		List<RevenueExpensesSumByType> list = revenueDao.getOtherRevenueExpenses(from, to);

		List<Object> revenues = new ArrayList<>();
		List<Object> expenses = new ArrayList<>();

		for (RevenueExpensesSumByType reSum : list) {
			container.getData1().add(revenueExpensesService.getRevenueType().get(reSum.getType().toString()));
			revenues.add(reSum.getRevenue());
			expenses.add(reSum.getExpenses());
		}
		container.addValues("Приход", revenues);
		container.addValues("Витрати", expenses);
		return container;
	}
	
	
	/*
	 * {'PIVO' : {'Sum' : 120, products : []}, 'Vodka'... }
	 * 
	 */
	public ExciseDataDecorator getInvoiceExciseProducts(Date from, Date to) {
		
		ExciseDataDecorator edd = new ExciseDataDecorator();
		List<Object[]> list = invoiceDao.getExciseProducts(from,to);
		for(Object[] row : list) {
			edd.addExciseDate(new ExciseData(row));
		}
		return edd;
		
	
	}
	
	
	
}
