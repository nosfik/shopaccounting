package com.shop.statistic;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shop.invoice.repository.BalanceDAO;
import com.shop.revenue.repository.RevenueExpensesDao;
import com.shop.schedule.repository.Seller;
import com.shop.schedule.repository.SellerSchedule;
import com.shop.schedule.service.SellerScheduleService;
import com.shop.statistic.model.SalaryData;
import com.shop.statistic.model.StatisticDataContainer;
import com.shop.util.ArithmeticUtil;

@Service
public class SalaryService {
	
	private static final int SHIFT_IN_DAY = 4;
	
	private final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
	
	@Autowired
	private SellerScheduleService sellerScheduleService;
	
	@Autowired
	private RevenueExpensesDao revenueExpensesDao;
	
	public SalaryData getSalaryData(int year, int month, String percent) {
		SalaryData salaryData = new SalaryData();
		
		LocalDate from = LocalDate.of(year, month, 1);
		int numberOfDaysInMonth = from.getMonth().length(from.isLeapYear());
		LocalDate to = LocalDate.of(year, month, numberOfDaysInMonth);
		Map<String, Integer> map = getSellerShiftMap(year, month);
		
		final BigDecimal totalProceed = revenueExpensesDao.getProceedSum(Date.valueOf(from), Date.valueOf(to));
		final BigDecimal totalSalary = ArithmeticUtil.getPercentOfNumber(totalProceed, new BigDecimal(percent));
		final int totalShiftsInMonth = numberOfDaysInMonth * SHIFT_IN_DAY;
		int shiftInMonth = 0;
		for(Integer shft : map.values()){
			shiftInMonth += shft;
		}
		final BigDecimal shiftSalary = totalSalary.divide(new BigDecimal(shiftInMonth), 5, RoundingMode.HALF_UP);
		
		Map<String, BigDecimal> salaryMap = new HashMap<>();
		
		for(Map.Entry<String, Integer> entry : map.entrySet()) {
			salaryMap.put(entry.getKey(), shiftSalary.multiply(new BigDecimal(entry.getValue())).setScale(2, RoundingMode.HALF_UP));
		}
		
		salaryData.setProceed(totalProceed);
		salaryData.setSalary(totalSalary);
		salaryData.setShiftsInMonth(totalShiftsInMonth);
		salaryData.setTotalShifts(shiftInMonth);
		salaryData.setShiftSalary(shiftSalary);
		salaryData.setSellerSalary(salaryMap);
		
		return salaryData;
		
	}
	
	
	public JSONObject getSellerRevenue(Date from, Date to) {
		
		JSONObject result = new JSONObject();
		
		Map<String, BigDecimal> proceeds = getProceeds(from, to);
		
		JSONArray firstShitSellers = new JSONArray();
		JSONArray secondShitSellers = new JSONArray();
		Iterable<Seller> sellers = sellerScheduleService.getAllSellers();
		
		for(Seller seller : sellers) {
			JSONObject seller1Json = new JSONObject();
			JSONArray seller1Procceds = new JSONArray();
			seller1Json.put("name", seller.getName());
			seller1Json.put("data", seller1Procceds);
			
			JSONObject seller2Json = new JSONObject();
			JSONArray seller2Procceds = new JSONArray();
			seller2Json.put("name", seller.getName());
			seller2Json.put("data", seller2Procceds);
			
			
			List<Date> fDates = sellerScheduleService.getAllFirstShiftsBySeller(from, to, seller.getId());
			
			for(Date date :fDates) {
				BigDecimal proceed = proceeds.get(DATE_FORMAT.format(date) + "_" + 1);
				if(proceed == null)
					continue;
				JSONObject data = new JSONObject();
				seller1Procceds.put(data);
				data.put("date", date);
				data.put("proceed", proceed.divide(ArithmeticUtil.TWO, 2, RoundingMode.HALF_UP));
			}
			
			List<Date> sDates = sellerScheduleService.getAllSecondShiftsBySeller(from, to, seller.getId());
			
			for(Date date :sDates) {
				BigDecimal proceed = proceeds.get(DATE_FORMAT.format(date) + "_" + 5);
				if(proceed == null)
					continue;
				JSONObject data = new JSONObject();
				seller2Procceds.put(data);
				data.put("date", date);
				data.put("proceed", proceed.divide(ArithmeticUtil.TWO, 2, RoundingMode.HALF_UP));
			}
			firstShitSellers.put(seller1Json);
			secondShitSellers.put(seller2Json);
		}
		
		result.put("1", firstShitSellers);
		result.put("2", secondShitSellers);
		
		return result;
	}
	
	
	
	public StatisticDataContainer getSellerWorkDays(int year, int month) {
		Map<String, Integer> map = getSellerShiftMap(year, month);

		StatisticDataContainer container = new StatisticDataContainer();
		List<Object> values = new ArrayList<>();
		for(Map.Entry<String, Integer> entry : map.entrySet()) {
			container.getData1().add(entry.getKey());
			values.add(entry.getValue());
		}
		container.addValues("Продавці", values);
		return container;
	}
	
	
	private Map<String, BigDecimal> getProceeds(Date from, Date to) {
		
		List<Object[]> proceeds = revenueExpensesDao.getProceedsApart(from, to);
		
		Map<String, BigDecimal> map = new HashMap<>();
		for(Object[] row : proceeds) {
			String key = (String)row[0];
			BigDecimal value = (BigDecimal)row[1];
			map.put(key, value);
		}
		
		return map;
	}
	

	
	
	private Map<String, Integer> getSellerShiftMap(int year, int month) {
		Map<String, Integer> map = new HashMap<>();
		
		Iterable<SellerSchedule> schedules = sellerScheduleService.getAllSchedules(year, month);
		for(SellerSchedule sellerSchedule : schedules) {
			addSellerToMap(sellerSchedule.getFirstSeller(), map);
			addSellerToMap(sellerSchedule.getSecondSeller(), map);
			addSellerToMap(sellerSchedule.getThirdSeller(), map);
			addSellerToMap(sellerSchedule.getFourthSeller(), map);
		}
		
		return map;
	}
	
	private void addSellerToMap(Seller seller, Map<String, Integer> map) {
		if(seller == null)
			return;
		
		Integer value = map.get(seller.getName());
		if(value == null) {
			map.put(seller.getName(), 1);
		} else {
			map.put(seller.getName(), ++value);
		}
	}

}
