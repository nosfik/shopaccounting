package com.shop.schedule.service;

import java.sql.Date;
import java.util.List;

import com.shop.schedule.repository.Seller;
import com.shop.schedule.repository.SellerSchedule;

public interface SellerScheduleService {

	Iterable<Seller> getAllSellers();

	Seller saveSeller(Seller seller);

	void deleteSeller(long id);

	Iterable<SellerSchedule> getAllSchedules(int year, int month);

	SellerSchedule saveSchedule(SellerSchedule schedule);

	SellerSchedule get(Date date);

	Iterable<SellerSchedule> getAllSchedules(Date from, Date to);

	List<Date> getAllFirstShiftsBySeller(Date from, Date to, long id);

	List<Date> getAllSecondShiftsBySeller(Date from, Date to, long id);

}
