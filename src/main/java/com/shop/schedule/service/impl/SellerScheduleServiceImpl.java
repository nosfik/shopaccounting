package com.shop.schedule.service.impl;

import java.sql.Date;
import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shop.revenue.repository.RevenueExpensesDao;
import com.shop.schedule.repository.Seller;
import com.shop.schedule.repository.SellerDao;
import com.shop.schedule.repository.SellerSchedule;
import com.shop.schedule.repository.SellerScheduleDao;
import com.shop.schedule.service.SellerScheduleService;

@Service
public class SellerScheduleServiceImpl implements SellerScheduleService {

	@Autowired
	private SellerScheduleDao sellerScheduleDao;

	@Autowired
	private SellerDao sellerDao;
	
	@Override
	public Iterable<Seller> getAllSellers() {
		return sellerDao.findAll();
	}

	@Override
	public Seller saveSeller(Seller seller) {
		if(seller.getId() == 0) {
			Seller exist = sellerDao.findByName(seller.getName());
			if(exist != null && exist.getDeleted()) {
				exist.setDeleted(false);
				return sellerDao.save(exist);
			}
		}
		return sellerDao.save(seller);
	}

	@Override
	public void deleteSeller(long id) {
		sellerDao.delete(id);
	}

	@Override
	public Iterable<SellerSchedule> getAllSchedules(int year, int month) {
		LocalDate from = LocalDate.of(year, month, 1);
		LocalDate to = LocalDate.of(year, month, from.getMonth().length(from.isLeapYear()));
		return getAllSchedules(Date.valueOf(from), Date.valueOf(to));
	}
	
	@Override
	public Iterable<SellerSchedule> getAllSchedules(Date from, Date to) {
		return sellerScheduleDao.getAllByDates(from, to);
	}

	@Override
	public SellerSchedule saveSchedule(SellerSchedule schedule) {
		return sellerScheduleDao.save(schedule);
	}

	@Override
	public SellerSchedule get(Date date) {
		return sellerScheduleDao.findOne(date);
	}

	@Override
	public List<Date> getAllFirstShiftsBySeller(Date from, Date to, long id) {
		return sellerScheduleDao.getFirstShiftsBySeller(from, to, id);
	}

	@Override
	public List<Date> getAllSecondShiftsBySeller(Date from, Date to, long id) {
		return sellerScheduleDao.getSecondShiftsBySeller(from, to, id);
	}


}
