package com.shop.schedule.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface SellerDao extends CrudRepository<Seller, Long> {
	
	@Query("From Seller where is_deleted <> 1")
	Iterable<Seller> findAll();
	
	Seller findByName(String name);
}
