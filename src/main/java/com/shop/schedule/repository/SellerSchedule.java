package com.shop.schedule.repository;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Entity
public class SellerSchedule {
	
	@Id
	@Column
	private Date date;
	
	@ManyToOne
	@Fetch(FetchMode.JOIN)
	private Seller firstSeller;
	@ManyToOne
	@Fetch(FetchMode.JOIN)
	private Seller secondSeller;
	@ManyToOne
	@Fetch(FetchMode.JOIN)
	private Seller thirdSeller;
	@ManyToOne
	@Fetch(FetchMode.JOIN)
	private Seller fourthSeller;
	
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public Seller getFirstSeller() {
		return firstSeller;
	}
	public void setFirstSeller(Seller firstSeller) {
		this.firstSeller = firstSeller;
	}
	public Seller getSecondSeller() {
		return secondSeller;
	}
	public void setSecondSeller(Seller secondSeller) {
		this.secondSeller = secondSeller;
	}
	public Seller getThirdSeller() {
		return thirdSeller;
	}
	public void setThirdSeller(Seller thirdSeller) {
		this.thirdSeller = thirdSeller;
	}
	public Seller getFourthSeller() {
		return fourthSeller;
	}
	public void setFourthSeller(Seller fourthSeller) {
		this.fourthSeller = fourthSeller;
	}
	
	
	
}
