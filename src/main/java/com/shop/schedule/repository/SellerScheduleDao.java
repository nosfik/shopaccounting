package com.shop.schedule.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface SellerScheduleDao extends CrudRepository<SellerSchedule, Date> {
	
	@Query("FROM SellerSchedule where date between :from and :to")
	List<SellerSchedule> getAllByDates(@Param("from")Date from, @Param("to")Date to);
	
	
	@Query("Select date FROM SellerSchedule where date between :from and :to and (firstSeller.id = :id or secondSeller.id=:id)")
	List<Date> getFirstShiftsBySeller(@Param("from")Date from, @Param("to")Date to, @Param("id")long id);
	
	@Query("Select date FROM SellerSchedule where date between :from and :to and (thirdSeller.id = :id or fourthSeller.id=:id)")
	List<Date> getSecondShiftsBySeller(@Param("from")Date from, @Param("to")Date to, @Param("id")long id);
	
	
}
