package com.shop.supplier.service;


import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Date;
import java.util.Set;

import com.shop.supplier.product.Product;
import com.shop.supplier.repository.Supplier;

public interface SupplierService {

	Iterable<Supplier> getSuppliers();

	boolean deleteSupplier(long supllierId);

	Supplier save(Supplier supplier);

	Supplier getSupplier(long supplierId);

	Iterable<Supplier> getSuppliersByCategory(long categoryId);
	
	Set<BigInteger> getUnusedProductIds(Date from);
	
	boolean deleteProduct(long productId);

	Product updateProductCode(String code, long id);
	
	Product addProduct(Product product);
	
	Product updateProduct(Product product, long id);

	Product getProductByCode(String code);
	
	boolean updateProductVolume(long id, BigDecimal volume);

	boolean updateProductExciseType(long id, long exciseTypeId);

	Product moveProduct(long productId, long supplierId, boolean copy);

}
