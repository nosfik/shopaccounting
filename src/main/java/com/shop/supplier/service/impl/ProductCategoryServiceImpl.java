package com.shop.supplier.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shop.supplier.product.ProductCategory;
import com.shop.supplier.product.ProductCategoryDao;
import com.shop.supplier.service.ProductCategoryService;

@Service
public class ProductCategoryServiceImpl implements ProductCategoryService {
	
	@Autowired
	private ProductCategoryDao productCategoryDao;

	@Override
	public Iterable<ProductCategory> getAll() {
		return productCategoryDao.findAll();
	}

	@Override
	public ProductCategory get(Long pk) {
		return productCategoryDao.findOne(pk);
	}

	@Override
	public boolean delete(Long pk) {
		productCategoryDao.delete(pk);
		return true;
	}

	@Override
	public ProductCategory save(ProductCategory prodCat) {
		return productCategoryDao.save(prodCat);
	}

	

}
