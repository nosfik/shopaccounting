package com.shop.supplier.service.impl;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Date;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shop.excise.repository.ExciseType;
import com.shop.supplier.product.Product;
import com.shop.supplier.product.ProductDao;
import com.shop.supplier.repository.Supplier;
import com.shop.supplier.repository.SupplierDao;
import com.shop.supplier.service.SupplierService;

@Service
public class SupplierServiceImpl implements SupplierService {
	
	@Autowired
	private SupplierDao supplierDao;
	
	@Autowired
	private ProductDao productDao;
	
	@Override
	public Iterable<Supplier> getSuppliers() {
		return supplierDao.findAllByOrderByNameAsc();
	}
	@Override
	public Iterable<Supplier> getSuppliersByCategory(long categoryId) {
		return supplierDao.findByCategory(categoryId);
	}
	@Override
	public Supplier getSupplier(long supplierId) {
		return supplierDao.findOne(supplierId);
	}
	@Transactional
	@Override
	public boolean deleteSupplier(long supllierId) {
		Supplier supplier = supplierDao.findOne(supllierId);
		supplier.getProducts().clear();
		supplier.setDeleted(true);
		return true;
	}
	@Override
	public Supplier save(Supplier supplier) {
		for(Product product : supplier.getProducts()) {
			product.setSupplier(supplier);
		}
		
		if(supplier.getId() > 0) {
			
		}
		
		return supplierDao.save(supplier);
	}
	
	@Override
	public Set<BigInteger> getUnusedProductIds(Date from) {
		return productDao.getNotUsedProductsFromDate(from);
	}
	
	@Override
	public boolean deleteProduct(long productId) {
		productDao.delete(productId);
		return true;
	}
	
	@Override
	public Product getProductByCode(String code) {
		return productDao.getProductByCode(code);
	}
	
	@Override
	@Transactional
	public Product updateProductCode(String code, long id) {
		Product product = getProductByCode(code);
		if(product == null) {
			productDao.updateProductCode(code, id);
		}
		return product;
	}
	
	@Transactional
	@Override
	public boolean updateProductVolume(long id, BigDecimal volume) {
		 productDao.findOne(id).setVolume(volume);
		 return true;
	}
	
	@Transactional
	@Override
	public boolean updateProductExciseType(long id, long exciseTypeId) {
		ExciseType et = new ExciseType();
		et.setId(exciseTypeId);
		productDao.findOne(id).setExciseType(et);
		 return true;
	}

	@Transactional
	@Override
	public Product moveProduct(long productId, long supplierId, boolean copy) {
		Supplier sp = new Supplier();
		sp.setId(supplierId);

		if(copy) {
			Product p = productDao.findOne(productId);
			Product newProduct = new Product();
			newProduct.setCode(p.getCode());
			newProduct.setExcisePrice(p.getExcisePrice());
			newProduct.setMarginPrice(p.getMarginPrice());
			newProduct.setName(p.getName());
			newProduct.setPrice(p.getPrice());
			newProduct.setVolume(p.getVolume());
			newProduct.setSupplier(sp);
			newProduct.setExciseType(p.getExciseType());
			productDao.save(newProduct);
			return newProduct;
		} else {
			Product p = productDao.findOne(productId);
			p.setSupplier(sp);
			return p;
		}
	}

	@Transactional
	@Override
	public Product updateProduct(Product product, long id) {
		Product p = productDao.findOne(id);
		p.setCode(product.getCode());
		p.setExcisePrice(product.getExcisePrice());
		p.setMarginPrice(product.getMarginPrice());
		p.setName(product.getName());
		p.setPrice(product.getPrice());
		p.setVolume(product.getVolume());
		return p;
		
	}
	
	@Transactional
	@Override
	public Product addProduct(Product product) {
		return productDao.save(product);
	}
	

	
}
