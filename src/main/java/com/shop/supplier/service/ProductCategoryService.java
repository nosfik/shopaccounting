package com.shop.supplier.service;

import com.shop.base.service.GenericRestService;
import com.shop.supplier.product.ProductCategory;

public interface ProductCategoryService extends GenericRestService<ProductCategory, Long> {


}
