package com.shop.supplier.product;

import java.math.BigDecimal;


public class CigaretteDTO {
	
	private String code;
	private String barcode;
	protected long id;
	protected String name;
	protected String oldName;
	protected BigDecimal price;
	protected BigDecimal excisePrice;
	protected BigDecimal oldExcisePrice;
	protected BigDecimal marginPrice;
	protected BigDecimal recommendedPrice;
	protected BigDecimal oldPrice;
	protected BigDecimal oldMarginPrice;
	protected BigDecimal quantity;

	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getBarcode() {
		return barcode;
	}
	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public BigDecimal getMarginPrice() {
		return marginPrice;
	}
	public void setMarginPrice(BigDecimal marginPrice) {
		this.marginPrice = marginPrice;
	}
	public BigDecimal getOldPrice() {
		return oldPrice;
	}
	public void setOldPrice(BigDecimal oldPrice) {
		this.oldPrice = oldPrice;
	}
	public BigDecimal getOldMarginPrice() {
		return oldMarginPrice;
	}
	public void setOldMarginPrice(BigDecimal oldMarginPrice) {
		this.oldMarginPrice = oldMarginPrice;
	}
	public BigDecimal getQuantity() {
		return quantity;
	}
	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}
	public String getOldName() {
		return oldName;
	}
	public void setOldName(String oldName) {
		this.oldName = oldName;
	}
	public BigDecimal getRecommendedPrice() {
		return recommendedPrice;
	}
	public void setRecommendedPrice(BigDecimal recommendedPrice) {
		this.recommendedPrice = recommendedPrice;
	}
	public BigDecimal getExcisePrice() {
		return excisePrice;
	}
	public void setExcisePrice(BigDecimal excisePrice) {
		this.excisePrice = excisePrice;
	}
	public BigDecimal getOldExcisePrice() {
		return oldExcisePrice;
	}
	public void setOldExcisePrice(BigDecimal oldExcisePrice) {
		this.oldExcisePrice = oldExcisePrice;
	}
	
}
