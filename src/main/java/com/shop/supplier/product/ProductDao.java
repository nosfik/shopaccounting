package com.shop.supplier.product;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Date;
import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface ProductDao extends CrudRepository<Product, Long> {
	
	@Modifying
	@Query("Update Product p set p.price =:price, p.marginPrice =:marginPrice, p.excisePrice=:excisePrice where p.id=:pid")
	Integer updateProductPrices(@Param("pid") Long productId, @Param("price")BigDecimal price, @Param("marginPrice")BigDecimal marginPrice, @Param("excisePrice")BigDecimal excisePrice);
	
	@Query(value="select p.id from product as p LEFT JOIN (Select i.id as id, ip.product as product from invoice as i LEFT JOIN invoice_products as ips ON ips.invoice=i.id LEFT JOIN invoice_product as ip ON ip.id = ips.products WHERE i.date >= :date) as t ON p.id = t.product Where t.id is null;", nativeQuery=true)
	Set<BigInteger> getNotUsedProductsFromDate(@Param("date") Date date);
	
	@Query("from Cigarette where barcode =:barcode")
	Cigarette getCigaretteByBarCode(@Param("barcode") String barCode);
	
	@Modifying
	@Query(value="update product p set p.code=:code where p.id =:id",  nativeQuery=true)
	Integer updateProductCode(@Param("code") String code, @Param("id") long id);
	
	@Query("from Product where code =:code")
	Product getProductByCode(@Param("code") String code);
	
	@Query("Select new com.shop.supplier.product.ProductIdWithCode(id, code) from Product where code is not null")
	List<ProductIdWithCode> findAllProductWithCode();
	
}