package com.shop.supplier.product;

import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

@Entity
@SQLDelete(sql = "UPDATE product SET is_deleted = 1, code = NULL, barcode = NULL WHERE id = ?")
@Where(clause = "is_deleted <> 1")
@PrimaryKeyJoinColumn(name = "id", referencedColumnName = "id")
@Table(name = "cigarette", indexes = { 
		@Index(name = "barcode_index", columnList = "barcode", unique = true),
		@Index(name = "barcode__code_unique_index", columnList = "barcode,code", unique = true) 
		})
public class Cigarette extends Product {

	private String barcode;

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	@Override
	public String toString() {
		return "Cigarette [code=" + code + ", barcode=" + barcode + ", id=" + id + ", name=" + name + ", price=" + price
				+ ", marginPrice=" + marginPrice + ", deleted=" + deleted + ", supplier=" + supplier + "]";
	}

}
