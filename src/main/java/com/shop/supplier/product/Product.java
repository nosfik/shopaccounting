package com.shop.supplier.product;

import java.math.BigDecimal;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.shop.excise.repository.ExciseType;
import com.shop.supplier.repository.Supplier;

@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@SQLDelete(sql="UPDATE product SET is_deleted = 1, code = NULL WHERE id = ?")
@Where(clause="is_deleted <> 1")
@DiscriminatorColumn(
	    name = "type",
	    discriminatorType = DiscriminatorType.STRING
	)
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, defaultImpl = Product.class, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({@JsonSubTypes.Type(value = Cigarette.class, name = "Cigarette")})
public class Product {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Access(AccessType.PROPERTY)
	protected long id;
	
	@Column(nullable=false)
	protected String name;
	
	@Column(nullable=false, precision = 10, scale = 3 )
	protected BigDecimal price;
	@Column(nullable=true, precision = 10, scale = 3 )
	protected BigDecimal excisePrice;
	protected BigDecimal marginPrice;
	
	@Column(nullable=false, name="is_deleted")
	protected Boolean deleted = false;
	
	@ManyToOne
	@JoinColumn(nullable=false, name="supplier_id")
	@JsonProperty(access =com.fasterxml.jackson.annotation.JsonProperty.Access.WRITE_ONLY)
	protected Supplier supplier;
	
	@Column(length=15, unique=true)
	protected String code;
	@Column(nullable=true, precision = 10, scale = 3 )
	protected BigDecimal volume;
	
	@ManyToOne
	protected ExciseType exciseType;
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public BigDecimal getExcisePrice() {
		return excisePrice;
	}
	public void setExcisePrice(BigDecimal excisePrice) {
		this.excisePrice = excisePrice;
	}
	public BigDecimal getMarginPrice() {
		return marginPrice;
	}
	public void setMarginPrice(BigDecimal marginPrice) {
		this.marginPrice = marginPrice;
	}
	public Supplier getSupplier() {
		return supplier;
	}
	public void setSupplier(Supplier supplier) {
		this.supplier = supplier;
	}
	
	public BigDecimal getVolume() {
		return volume;
	}
	public void setVolume(BigDecimal volume) {
		this.volume = volume;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	
	public ExciseType getExciseType() {
		return exciseType;
	}
	public void setExciseType(ExciseType exciseType) {
		this.exciseType = exciseType;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((deleted == null) ? 0 : deleted.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((price == null) ? 0 : price.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Product other = (Product) obj;
		if (deleted == null) {
			if (other.deleted != null)
				return false;
		} else if (!deleted.equals(other.deleted))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (price == null) {
			if (other.price != null)
				return false;
		} else if (!price.equals(other.price))
			return false;
		return true;
	}
	
	
}
