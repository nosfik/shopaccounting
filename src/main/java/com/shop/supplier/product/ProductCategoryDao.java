package com.shop.supplier.product;

import org.springframework.data.repository.CrudRepository;

public interface ProductCategoryDao extends CrudRepository<ProductCategory, Long> {

}
