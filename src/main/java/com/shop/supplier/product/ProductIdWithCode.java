package com.shop.supplier.product;

public class ProductIdWithCode {
	
	private long id;
	private String code;
	
	public ProductIdWithCode(){
		
	}
	
	public ProductIdWithCode(long id, String code) {
		super();
		this.id = id;
		this.code = code;
	}

	public long getId() {
		return id;
	}

	public String getCode() {
		return code;
	}

}
