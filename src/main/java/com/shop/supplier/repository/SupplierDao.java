package com.shop.supplier.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface SupplierDao extends CrudRepository<Supplier, Long> {
	
	
	@Query("SELECT DISTINCT s FROM Supplier s LEFT JOIN FETCH s.products p LEFT JOIN FETCH s.productCategory ProductCategory where s.deleted=false order by s.name asc")
	Iterable<Supplier> findAllByOrderByNameAsc();
	
	@Query("SELECT DISTINCT s FROM Supplier s JOIN FETCH s.products where s.productCategory.id = :id and s.deleted=false")
	Iterable<Supplier> findByCategory(@Param("id") Long id);
	
	@Query("FROM Supplier s LEFT JOIN FETCH s.products where s.id = :id")
	Supplier findOne(@Param("id") Long id);

}
