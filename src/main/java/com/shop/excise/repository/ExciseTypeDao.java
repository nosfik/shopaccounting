package com.shop.excise.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface ExciseTypeDao extends CrudRepository<ExciseType, Long> {
	
	ExciseType findByName(String name);
	
	@Query("FROM ExciseType where deleted=false")
	Iterable<ExciseType> findAll();
	
	

}
