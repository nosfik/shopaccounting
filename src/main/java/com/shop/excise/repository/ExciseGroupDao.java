package com.shop.excise.repository;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface ExciseGroupDao extends CrudRepository<ExciseGroup, Long> {
	
	@Transactional
	@Modifying
	@Query(value="INSERT INTO excise_product (excise, product) VALUES (:gId,:pId)", nativeQuery=true)
	void addProductToCategory(@Param("pId") long pId, @Param("gId") long gId);

}
