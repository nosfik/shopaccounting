package com.shop.excise.repository;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.shop.supplier.product.Product;

@Entity
@Table(indexes = { @Index(name = "excise_name", columnList = "name", unique = true) })
public class ExciseGroup {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	protected long id;

	@Column(nullable = false)
	private String name;

	@OneToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "excise_product", joinColumns = @JoinColumn(name = "excise", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "product"))
	private List<Product> products;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}

}
