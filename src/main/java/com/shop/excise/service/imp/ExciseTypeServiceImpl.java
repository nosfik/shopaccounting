package com.shop.excise.service.imp;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shop.excise.repository.ExciseType;
import com.shop.excise.repository.ExciseTypeDao;
import com.shop.excise.service.ExciseTypeService;

@Service
public class ExciseTypeServiceImpl implements ExciseTypeService {
	
	@Autowired
	private ExciseTypeDao repository;

	@Override
	public Iterable<ExciseType> findAll() {
		return repository.findAll();
	}
	
	

	@Override
	public ExciseType findOne(long id) {
		return repository.findOne(id);
	}

	@Override
	public boolean delete(long id) {
		repository.delete(id);
		return true;
	}

	@Override
	public ExciseType update(ExciseType eg) {
		return repository.save(eg);
	}

	@Override
	public ExciseType save(ExciseType type) {
		if(type.getId() == 0) {
			ExciseType exist = repository.findByName(type.getName());
			if(exist != null && exist.getDeleted()) {
				exist.setDeleted(false);
				return repository.save(exist);
			}
		}
		return repository.save(type);
	}

}
