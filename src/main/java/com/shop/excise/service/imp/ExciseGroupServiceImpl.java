package com.shop.excise.service.imp;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shop.excise.repository.ExciseGroup;
import com.shop.excise.repository.ExciseGroupDao;
import com.shop.excise.service.ExciseGroupService;

@Service
public class ExciseGroupServiceImpl implements ExciseGroupService {

	@Autowired
	private ExciseGroupDao repository;

	@Override
	public Iterable<ExciseGroup> findAll() {
		return repository.findAll();
	}
	
	@Override
	public Iterable<ExciseGroup> findAllWithoutProducts() {
		Iterable<ExciseGroup> result = repository.findAll();
		for(ExciseGroup e : repository.findAll()) {
			e.setProducts(null);
		}
		return result;
	}

	@Override
	public ExciseGroup findOne(long id) {
		return repository.findOne(id);
	}

	@Override
	public boolean delete(long id) {
		repository.delete(id);
		return true;
	}

	@Override
	public ExciseGroup update(ExciseGroup eg) {
		return repository.save(eg);
	}

	@Override
	public ExciseGroup save(ExciseGroup eg) {
		return repository.save(eg);
	}
	
	@Override
	public boolean addProductToCategory(long pId, long gId) {
		repository.addProductToCategory(pId, gId);
		return true;
	}

}
