package com.shop.excise.service;

import com.shop.excise.repository.ExciseType;

public interface ExciseTypeService {
	
	public Iterable<ExciseType> findAll();
	public ExciseType findOne(long id);
	public boolean delete(long id);
	public ExciseType update(ExciseType eg);
	public ExciseType save(ExciseType eg);

}
