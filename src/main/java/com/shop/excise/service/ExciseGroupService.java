package com.shop.excise.service;

import com.shop.excise.repository.ExciseGroup;

public interface ExciseGroupService {

	public Iterable<ExciseGroup> findAll();

	public ExciseGroup findOne(long id);

	public boolean delete(long id);

	public ExciseGroup update(ExciseGroup eg);

	public ExciseGroup save(ExciseGroup eg);

	Iterable<ExciseGroup> findAllWithoutProducts();

	boolean addProductToCategory(long pId, long gId);

}
