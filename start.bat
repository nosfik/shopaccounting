@echo off

tasklist /FI "IMAGENAME eq javaw.exe" 2>NUL | find /I /N "javaw.exe">NUL
if "%ERRORLEVEL%"=="0" (
  start chrome http://localhost:8080
  exit
) else (
mysqldump -uroot -proot shopdb > dumps/%DATE%.sql
Start  "" "launcher.bat"
TIMEOUT 40 > NUL
start chrome http://localhost:8080
exit
)
